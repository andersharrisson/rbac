/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.aps.jca.CAException;
import gov.aps.jca.Channel;
import gov.aps.jca.Monitor;
import gov.aps.jca.dbr.DBRType;
import gov.aps.jca.event.MonitorEvent;
import gov.aps.jca.event.MonitorListener;

/**
 *
 * <code>RefCountedMonitor</code> is a monitor reference, which uses a single channel access monitor subscription for
 * many different listeners. This object should be created for the first requested subscription for specific channel and
 * of specific type. Or later creation should reference the same cached object. When all channels unsubscribe this
 * reference should be disposed of as well.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class RefCountedMonitor implements MonitorListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefCountedMonitor.class);

    private Monitor subscription;
    private final String name;
    private final AtomicInteger referenceCount = new AtomicInteger(1);

    private MonitorEvent lastEvent;

    private final List<MonitorListener> listeners = new ArrayList<>();

    /**
     * Creates a new reference monitor for the given channel of the given type.
     *
     * @param channel the channel to subscribe to
     * @param type the data type for the subscription
     * @param mask the monitor mask
     * @param name the name of this reference used for caching
     *
     * @throws IllegalStateException if creation of subscription failed
     * @throws CAException if creation of subscription failed
     */
    public RefCountedMonitor(Channel channel, DBRType type, int mask, String name) throws CAException {
        this.name = name;
        subscription = channel.addMonitor(type, channel.getElementCount(), mask, this);
    }

    /**
     * Adds a listener to this monitor. If this reference already received a value from the CA, the value is forwarded
     * to the listener.
     *
     * @param listener the listener to add
     */
    public void addMonitorListener(MonitorListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
            if (lastEvent != null) {
                listener.monitorChanged(lastEvent);
            }
        }
    }

    /**
     * Removes the listener from this reference.
     *
     * @param listener the listener to remove
     */
    public void removeMonitorListener(MonitorListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    /**
     * Dispose of this reference.
     *
     * @throws CAException if the monitor is still referenced by a PV
     */
    public void dispose() throws CAException {
        if (referenceCount.get() != 0) {
            throw new CAException("Channel destroyed while referenced " + referenceCount + " times");
        }
        try {
            subscription.clear();
        } catch (CAException ex) {
            LOGGER.warn("Channel unsubscribe failed", ex);
        }
        subscription = null;
    }

    /** Increment reference count. */
    public synchronized void incRefs() {
        referenceCount.incrementAndGet();
    }

    /**
     * Decrement reference count.
     *
     * @return Remaining references.
     */
    public synchronized int decRefs() {
        return referenceCount.decrementAndGet();
    }

    /*
     * (non-Javadoc)
     *
     * @see gov.aps.jca.event.MonitorListener#monitorChanged(gov.aps.jca.event.MonitorEvent)
     */
    @Override
    public void monitorChanged(MonitorEvent ev) {
        MonitorListener[] list = null;
        synchronized (this.listeners) {
            lastEvent = ev;
            list = this.listeners.toArray(new MonitorListener[this.listeners.size()]);
        }
        for (MonitorListener l : list) {
            l.monitorChanged(ev);
        }
    }

    /**
     * @return the name of this channel
     */
    public String getName() {
        return name;
    }
}
