/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.aps.jca.event.ContextExceptionEvent;
import gov.aps.jca.event.ContextExceptionListener;
import gov.aps.jca.event.ContextMessageEvent;
import gov.aps.jca.event.ContextMessageListener;
import gov.aps.jca.event.ContextVirtualCircuitExceptionEvent;

/**
 * Handler for JCA Context errors and messages; places them in log.
 * 
 * @author Kay Kasemir
 */
@SuppressWarnings("nls")
public class ContextErrorHandler implements ContextExceptionListener, ContextMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContextErrorHandler.class);

    /*
     * (non-Javadoc)
     * 
     * @see gov.aps.jca.event.ContextExceptionListener#contextException(gov.aps.jca.event. ContextExceptionEvent)
     */
    @Override
    public void contextException(final ContextExceptionEvent ev) {
        LOGGER.error("Exception from " + ev.getSource() + " " + ev.getMessage());
    }

    /*
     * (non-Javadoc)
     * 
     * @see gov.aps.jca.event.ContextExceptionListener#contextVirtualCircuitException(gov.aps.jca.event
     * .ContextVirtualCircuitExceptionEvent)
     */
    @Override
    public void contextVirtualCircuitException(ContextVirtualCircuitExceptionEvent ev) {
        LOGGER.info("Virtual circuit exception from " + ev.getSource() + " at " + ev.getVirtualCircuit());
    }

    /*
     * (non-Javadoc)
     * 
     * @see gov.aps.jca.event.ContextMessageListener#contextMessage(gov.aps.jca.event.ContextMessageEvent )
     */
    @Override
    public void contextMessage(final ContextMessageEvent ev) {
        LOGGER.info("Message from " + ev.getSource() + ": " + ev.getMessage());
    }
}
