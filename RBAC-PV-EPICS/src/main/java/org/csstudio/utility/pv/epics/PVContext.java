/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import gov.aps.jca.CAException;
import gov.aps.jca.Channel;
import gov.aps.jca.Context;
import gov.aps.jca.JCALibrary;
import gov.aps.jca.dbr.DBRType;
import gov.aps.jca.event.ConnectionListener;
import gov.aps.jca.event.ContextExceptionListener;
import gov.aps.jca.event.ContextMessageListener;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.csstudio.utility.pv.PVException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handle PV context, pool PVs by name.
 * 
 * When using the pure java CA client implementation, it returns the same 'channel' when trying to access the same PV
 * name multiple times. That's good, but I don't know how to determine if the channel for this EPICS_V3_PV is actually
 * shared. Calling destroy() on such a shared channel creates problems.<br>
 * The PVContext adds its own hash map of channels and keeps a reference count.
 * 
 * @author Kay Kasemir
 */
public final class PVContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(PVContext.class);
    /**
     * In principle, we like to close the context when it is no longer needed.
     * 
     * This is in fact required for CAJ to close all threads.
     * 
     * For JCA, however, there are problems: With R3.14.8.2 on Linux there were errors
     * "pthread_create error Invalid argument". With R3.14.11 on OS X 10.5.8 the call to jca_context.destroy() caused an
     * "Invalid memory access ..." crash.
     * 
     * -> We keep the context open.
     */
    private static final boolean CLEANUP = true;

    /**
     * Set to <code>true</code> if the pure Java CA context should be used.
     * <p>
     * Changes only have an effect before the very first channel is created.
     */
    private static final boolean USE_PURE_JAVA = Boolean.parseBoolean(System.getProperty(
            "org.csstudio.utility.pv.epics.usePureJava", "true"));

    /** Mask used for CA monitors */
    public static final MonitorMask MONITOR_MASK = MonitorMask.VALUE;

    /** The Java CA Library instance. */
    private static JCALibrary jca;

    /** The JCA context. */
    private static volatile Context jcaContext;

    /** The JCA context reference count. */
    private static AtomicInteger jcaRefs = new AtomicInteger(0);

    /** map of channels. */
    private static Map<String, RefCountedChannel> channels = new HashMap<>();

    /** map of monitors. */
    private static Map<String, RefCountedMonitor> monitors = new HashMap<>();

    private static JCACommandThread commandThread;

    private PVContext() {
    }

    /** Initialise the JCA library, start the command thread. */
    private static synchronized void initJCA() throws CAException {
        if (jcaRefs.get() == 0) {
            if (jca == null) {
                jca = JCALibrary.getInstance();
                final String type = USE_PURE_JAVA ? JCALibrary.CHANNEL_ACCESS_JAVA : JCALibrary.JNI_THREAD_SAFE;
                jcaContext = jca.createContext(type);

                ContextExceptionListener[] extListeners = jcaContext.getContextExceptionListeners();
                for (ContextExceptionListener exl : extListeners) {
                    jcaContext.removeContextExceptionListener(exl);
                }
                ContextMessageListener[] msgListeners = jcaContext.getContextMessageListeners();
                for (ContextMessageListener cml : msgListeners) {
                    jcaContext.removeContextMessageListener(cml);
                }

                ContextErrorHandler logHandler = new ContextErrorHandler();
                jcaContext.addContextExceptionListener(logHandler);
                jcaContext.addContextMessageListener(logHandler);
            }
            commandThread = new JCACommandThread(jcaContext);
        }
        jcaRefs.incrementAndGet();
    }

    /**
     * Disconnect from the JA library.
     * <p>
     * Without this step, JCA threads can stay around and prevent the application from quitting.
     */
    private static void exitJCA() {
        if (jcaRefs.decrementAndGet() > 0) {
            return;
        }
        commandThread.shutdown();
        commandThread = null;
        if (!CLEANUP) {
            LOGGER.debug("JCA not longer used, but kept open.");
            return;
        }
        try {
            jcaContext.destroy();
            jcaContext = null;
            jca = null;
            LOGGER.debug("Finalized JCA");
        } catch (CAException | IllegalStateException ex) {
            LOGGER.warn("exitJCA", ex);
        }
    }

    /**
     * Returns a reference to the channel subscription and adds a listener to it. Method will look for the subscription
     * in the reference map. If found, the reference to an existing subscription will be returned (reference counter is
     * incremented).
     * 
     * @param channel the channel for which we want to create a subscription.
     * @param type the type of subscription
     * @param mask the mask
     * @return the subscription
     * @throws CAException if there was an error creating the subscription
     */
    static RefCountedMonitor getMonitor(Channel channel, DBRType type, int mask) throws CAException {
        synchronized (monitors) {
            String name = channel.getName() + "?" + type.getName() + "&" + mask;
            RefCountedMonitor monitorRef = monitors.get(name);
            if (monitorRef == null) {
                LOGGER.debug("Creating CA monitor " + name);
                monitorRef = new RefCountedMonitor(channel, type, mask, name);
                monitors.put(name, monitorRef);
            } else {
                monitorRef.incRefs();
                LOGGER.debug("Re-using CA monitor " + name);
            }
            return monitorRef;
        }
    }

    /**
     * Releases the subscription. If the reference count is 0 the subscription is terminated.
     * 
     * @param monitor the reference to release
     * @throws CAException if there was an error in terminating the subscription
     */
    static void releaseMonitor(RefCountedMonitor monitor) throws CAException {
        synchronized (monitors) {
            final String name = monitor.getName();
            if (monitor.decRefs() <= 0) {
                LOGGER.debug("Deleting CA monitor " + name);
                monitors.remove(name);
                monitor.dispose();
            } else {
                LOGGER.debug("CA monitor " + name + " still ref'ed");
            }
        }
    }

    /**
     * Get a new channel, or a reference to an existing one.
     * 
     * @param name Channel name
     * @param connCallback the connection callback to add to the channel
     * @return reference to channel
     * @throws PVException if null channel was created
     * @throws CAException on error
     * @see #releaseChannel(RefCountedChannel, ConnectionListener)
     */
    static RefCountedChannel getChannel(final String name, final ConnectionListener connCallback) throws CAException,
            PVException {
        synchronized (channels) {
            initJCA();
            RefCountedChannel channelRef = channels.get(name);
            if (channelRef == null) {
                LOGGER.debug("Creating CA channel " + name);
                final Channel channel = jcaContext.createChannel(name, connCallback);
                if (channel == null) {
                    throw new PVException(name, "Cannot create channel '" + name + "'");
                }
                channelRef = new RefCountedChannel(channel);
                channels.put(name, channelRef);
                // Start the command thread after the first channel is created.
                // This starts it in any case, but follow-up calls are NOPs.
                commandThread.start();
            } else {
                channelRef.incRefs();
                // Saw null pointer exception here.
                // Must have been getChannel() == null, but how is that possible?
                if (channelRef.getChannel() == null) {
                    LOGGER.error("Channel " + name + " is null.");
                    return channelRef;
                }
                channelRef.getChannel().addConnectionListener(connCallback);
                LOGGER.debug("Re-using CA channel " + name);
            }
            return channelRef;
        }
    }

    /**
     * Release a channel.
     * 
     * @param channelRef Channel to release
     * @param connCallback the connection callback that is removed from the channel prior to release
     * @see #getChannel(String, ConnectionListener)
     * @throws CAException if the channel could not be released
     */
    static void releaseChannel(final RefCountedChannel channelRef, final ConnectionListener connCallback)
            throws CAException {
        synchronized (channels) {
            final String name = channelRef.getChannel().getName();
            try {
                channelRef.getChannel().removeConnectionListener(connCallback);
            } catch (CAException | IllegalStateException ex) {
                LOGGER.warn("Remove connection listener", ex);
            }
            if (channelRef.decRefs() <= 0) {
                LOGGER.debug("Deleting CA channel " + name);
                channels.remove(name);
                channelRef.dispose();
            } else {
                LOGGER.debug("CA channel " + name + " still ref'ed");
            }
            exitJCA();
        }
    }

    /**
     * Add a command to the JCACommandThread.
     * <p>
     * 
     * @param command Command to schedule.
     */
    static void scheduleCommand(final Runnable command) {
        commandThread.addCommand(command);
    }

    /**
     * Helper for unit test.
     * 
     * @return <code>true</code> if all has been release.
     */
    static boolean allReleased() {
        return jcaRefs.get() == 0;
    }
}
