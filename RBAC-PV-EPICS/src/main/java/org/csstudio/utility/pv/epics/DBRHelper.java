/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import org.csstudio.data.values.IEnumeratedMetaData;
import org.csstudio.data.values.IMetaData;
import org.csstudio.data.values.INumericMetaData;
import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.ITimestamp;
import org.csstudio.data.values.IValue;
import org.csstudio.data.values.TimestampFactory;
import org.csstudio.data.values.ValueFactory;

import gov.aps.jca.dbr.DBR;
import gov.aps.jca.dbr.DBRType;
import gov.aps.jca.dbr.DBR_CTRL_Double;
import gov.aps.jca.dbr.DBR_CTRL_Enum;
import gov.aps.jca.dbr.DBR_CTRL_Int;
import gov.aps.jca.dbr.DBR_CTRL_Short;
import gov.aps.jca.dbr.DBR_LABELS_Enum;
import gov.aps.jca.dbr.DBR_TIME_Byte;
import gov.aps.jca.dbr.DBR_TIME_Double;
import gov.aps.jca.dbr.DBR_TIME_Enum;
import gov.aps.jca.dbr.DBR_TIME_Float;
import gov.aps.jca.dbr.DBR_TIME_Int;
import gov.aps.jca.dbr.DBR_TIME_Short;
import gov.aps.jca.dbr.DBR_TIME_String;
import gov.aps.jca.dbr.Status;
import gov.aps.jca.dbr.TimeStamp;

/**
 * 
 * <code>DBRHelper</code> provides utility methods to extract data from JCA types and pack them into PV types.
 * 
 */
public final class DBRHelper {

    private static final String OK = "OK";

    private DBRHelper() {
    }

    /**
     * Convert the EPICS time stamp (based on 1990) into the usual 1970 epoch.
     * <p>
     * In case this is called with data from a CTRL_... request, the null timestamp is replaced with the current host
     * time.
     * <p>
     * The EPICS start of epoch as returned by IOCs for records that have never processed is mapped to 0 in the 1970
     * epoch so that ITimestamp.isValid() can be used to catch it. This "works", bad time stamps are identified. But
     * it's not ideal because the original time stamp is lost. Better would be an implementation that passes 1990+0secs
     * on, yet makes isValid() return false by using an EPICS-specific ITimestamp override of isValid(). Not implemented
     * at this time.
     */
    private static ITimestamp createTimeFromEPICS(TimeStamp t) {
        if (t == null) {
            return TimestampFactory.now();
        } else if (t.secPastEpoch() == 0 && t.nsec() == 0) {
            return TimestampFactory.createTimestamp(0, 0);
        } else {
            return TimestampFactory.createTimestamp(t.secPastEpoch() + 631152000L, t.nsec());
        }
    }

    /**
     * The CTRL or normal type of the given DBRType.
     * 
     * @param type the source from which the new type is generated
     * @return the CTRL or normal type that matches the type of the given dbr type
     */
    public static DBRType getCtrlType(DBRType type) {
        if (type.isDOUBLE()) {
            return DBRType.CTRL_DOUBLE;
        } else if (type.isFLOAT()) {
            return DBRType.CTRL_DOUBLE;
        } else if (type.isINT()) {
            return DBRType.CTRL_INT;
        } else if (type.isSHORT()) {
            return DBRType.CTRL_INT;
        } else if (type.isBYTE()) {
            return DBRType.CTRL_BYTE;
        } else if (type.isENUM()) {
            return DBRType.CTRL_ENUM;
        } else {
            return DBRType.CTRL_STRING;
        }
    }

    /**
     * Extracts the meta data from the dbr.
     * 
     * @param dbr the source of data
     * @return Meta data extracted from dbr
     */
    public static IMetaData decodeMetaData(DBR dbr) {
        if (dbr.isLABELS()) {
            DBR_LABELS_Enum labels = (DBR_LABELS_Enum) dbr;
            return ValueFactory.createEnumeratedMetaData(labels.getLabels());
        } else if (dbr instanceof DBR_CTRL_Double) {
            DBR_CTRL_Double ctrl = (DBR_CTRL_Double) dbr;
            return ValueFactory.createNumericMetaData(ctrl.getLowerDispLimit().doubleValue(), ctrl.getUpperDispLimit()
                    .doubleValue(), ctrl.getLowerWarningLimit().doubleValue(), ctrl.getUpperWarningLimit()
                    .doubleValue(), ctrl.getLowerAlarmLimit().doubleValue(), ctrl.getUpperAlarmLimit().doubleValue(),
                    ctrl.getPrecision(), ctrl.getUnits());
        } else if (dbr instanceof DBR_CTRL_Int) {
            DBR_CTRL_Int ctrl = (DBR_CTRL_Int) dbr;
            return ValueFactory.createNumericMetaData(ctrl.getLowerDispLimit().doubleValue(), ctrl.getUpperDispLimit()
                    .doubleValue(), ctrl.getLowerWarningLimit().doubleValue(), ctrl.getUpperWarningLimit()
                    .doubleValue(), ctrl.getLowerAlarmLimit().doubleValue(), ctrl.getUpperAlarmLimit().doubleValue(),
                    0, // no
                       // precision
                    ctrl.getUnits());
        } else if (dbr instanceof DBR_CTRL_Short) {
            DBR_CTRL_Short ctrl = (DBR_CTRL_Short) dbr;
            return ValueFactory.createNumericMetaData(ctrl.getLowerDispLimit().doubleValue(), ctrl.getUpperDispLimit()
                    .doubleValue(), ctrl.getLowerWarningLimit().doubleValue(), ctrl.getUpperWarningLimit()
                    .doubleValue(), ctrl.getLowerAlarmLimit().doubleValue(), ctrl.getUpperAlarmLimit().doubleValue(),
                    0, // no
                       // precision
                    ctrl.getUnits());
        } else {
            return null;
        }
    }

    /**
     * Returns the plain or TIME type for the given dbr type.
     * 
     * @param type the source
     * @return plain or TIME type
     */
    public static DBRType getTimeType(DBRType type) {
        if (type.isDOUBLE()) {
            return DBRType.TIME_DOUBLE;
        } else if (type.isFLOAT()) {
            return DBRType.TIME_FLOAT;
        } else if (type.isINT()) {
            return DBRType.TIME_INT;
        } else if (type.isSHORT()) {
            return DBRType.TIME_SHORT;
        } else if (type.isENUM()) {
            return DBRType.TIME_ENUM;
        } else if (type.isBYTE()) {
            return DBRType.TIME_BYTE;
        } else {
            return DBRType.TIME_STRING;
        }
    }

    /** Convert short array to int array. */
    private static int[] short2int(short[] v) {
        int[] result = new int[v.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = v[i];
        }
        return result;
    }

    /** Convert short array to long array. */
    private static long[] short2long(short[] v) {
        long[] result = new long[v.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = v[i];
        }
        return result;
    }

    /** Convert int array to long array. */
    private static long[] int2long(int[] v) {
        long[] result = new long[v.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = v[i];
        }
        return result;
    }

    /** Convert byte array to long array. */
    private static long[] byte2long(byte[] v) {
        long[] result = new long[v.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = v[i];
        }
        return result;
    }

    /** Convert float array to a double array. */
    private static double[] float2double(float[] v) {
        double[] result = new double[v.length];
        for (int i = 0; i < result.length; i++) {
            // 0.1f is not equals to 0.1d
            // transform to double and then parse
            result[i] = Double.parseDouble(String.valueOf(v[i]));
        }
        return result;
    }

    /**
     * Decodes the data.
     * 
     * @param meta the meta information
     * @param dbr the source of data
     * @return decoded data as {@link IValue} implementation that matches the dbr type
     */
    public static IValue decodeValue(IMetaData meta, DBR dbr) {
        if (dbr.isDOUBLE()) {
            DBR_TIME_Double dt = (DBR_TIME_Double) dbr;
            return ValueFactory.createDoubleValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity()
                    .getValue()), decodeStatus(dt.getStatus()), (INumericMetaData) meta, dt.getDoubleValue());
        } else if (dbr.isFLOAT()) {
            DBR_TIME_Float dt = (DBR_TIME_Float) dbr;
            return ValueFactory.createDoubleValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity()
                    .getValue()), decodeStatus(dt.getStatus()), (INumericMetaData) meta, float2double(dt
                    .getFloatValue()));
        } else if (dbr.isINT()) {
            DBR_TIME_Int dt = (DBR_TIME_Int) dbr;
            return ValueFactory.createLongValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity()
                    .getValue()), decodeStatus(dt.getStatus()), (INumericMetaData) meta, int2long(dt.getIntValue()));
        } else if (dbr.isSHORT()) {
            DBR_TIME_Short dt = (DBR_TIME_Short) dbr;
            return ValueFactory
                    .createLongValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity().getValue()),
                            decodeStatus(dt.getStatus()), (INumericMetaData) meta, short2long(dt.getShortValue()));
        } else if (dbr.isSTRING()) {
            DBR_TIME_String dt = (DBR_TIME_String) dbr;
            return ValueFactory.createStringValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity()
                    .getValue()), decodeStatus(dt.getStatus()), dt.getStringValue());
        } else if (dbr.isENUM()) {
            if (dbr instanceof DBR_TIME_Enum) {
                DBR_TIME_Enum dt = (DBR_TIME_Enum) dbr;
                return ValueFactory.createEnumeratedValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt
                        .getSeverity().getValue()), decodeStatus(dt.getStatus()), (IEnumeratedMetaData) meta,
                        short2int(dt.getEnumValue()));
            } else if (dbr instanceof DBR_CTRL_Enum) {
                DBR_CTRL_Enum dt = (DBR_CTRL_Enum) dbr;
                return ValueFactory.createEnumeratedValue(createTimeFromEPICS(null), forCode(dt.getSeverity()
                        .getValue()), decodeStatus(dt.getStatus()), (IEnumeratedMetaData) meta, short2int(dt
                        .getEnumValue()));
            }
        } else if (dbr.isBYTE()) {
            DBR_TIME_Byte dt = (DBR_TIME_Byte) dbr;
            return ValueFactory.createLongValue(createTimeFromEPICS(dt.getTimeStamp()), forCode(dt.getSeverity()
                    .getValue()), decodeStatus(dt.getStatus()), (INumericMetaData) meta, byte2long(dt.getByteValue()));
        }
        throw new IllegalArgumentException("Cannot decode " + dbr);
    }

    /** @return String for Status */
    private static String decodeStatus(Status status) {
        if (status.getValue() == 0) {
            return OK;
        }
        return status.getName();
    }

    /**
     * Transforms the EPICS int code to a Severity instance.
     * 
     * @param code
     * @return the severity matching the given code
     */
    private static ISeverity forCode(int code) {
        switch (code) {
            case 0:
                return ValueFactory.createOKSeverity();
            case 1:
                return ValueFactory.createMinorSeverity();
            case 2:
                return ValueFactory.createMajorSeverity();
            default:
                return ValueFactory.createInvalidSeverity();
        }
    }
}
