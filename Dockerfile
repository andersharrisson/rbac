FROM jboss/wildfly:8.2.1.Final
LABEL maintainer="anders.harrisson@esss.se"

# PostgreSQL jdbc driver module
RUN mkdir -p /opt/jboss/wildfly/modules/org/postgresql/main
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/42.2.0/postgresql-42.2.0.jar /opt/jboss/wildfly/modules/org/postgresql/main/
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/wildfly-modules/org.postgresql/postgresql/postgresql-42.2.0.xml /opt/jboss/wildfly/modules/org/postgresql/main/module.xml
COPY --chown=jboss:jboss standalone.xml /opt/jboss/wildfly/standalone/configuration/

# GELF logging module
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/biz/paluch/logging/logstash-gelf/1.12.0/logstash-gelf-1.12.0-logging-module.zip /tmp/gelf-logging-module.zip
RUN unzip -q /tmp/gelf-logging-module.zip -d /tmp/ && mv /tmp/logstash-gelf-*/* /opt/jboss/wildfly/modules/ && rmdir /tmp/logstash-gelf-* && rm /tmp/gelf-logging-module.zip

# Set Environment variables used in JAVA_OPTS
ENV JBOSS_MODULES_SYSTEM_PKGS=org.jboss.byteman \
    JAVA_XMS=1024m \
    JAVA_XMX=2048m \
    JAVA_METASPACE=96M \
    JAVA_MAX_METASPACE=256m
ENV JAVA_OPTS="-server -Xms${JAVA_XMS} -Xmx${JAVA_XMX} -XX:MetaspaceSize=${JAVA_METASPACE} -XX:MaxMetaspaceSize=${JAVA_MAX_METASPACE} -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -XX:+UseStringDeduplication -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=${JBOSS_MODULES_SYSTEM_PKGS} -Djava.awt.headless=true"

COPY RBAC-Enterprise/target/rbac-ear-*.ear /opt/jboss/wildfly/standalone/deployments/
