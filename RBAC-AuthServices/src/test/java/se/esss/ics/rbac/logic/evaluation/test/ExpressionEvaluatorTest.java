/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.csstudio.data.values.ValueFactory;
import org.csstudio.utility.pv.PV;
import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.logic.evaluation.CachedPVProvider;
import se.esss.ics.rbac.logic.evaluation.EvaluationException;
import se.esss.ics.rbac.logic.evaluation.ExpressionEvaluator;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Token;

/**
 * 
 * <code>ExpressionEvaluatorTest</code> tests the methods of the {@link ExpressionEvaluator}.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class ExpressionEvaluatorTest {

    private static final String PV_DOUBLE_1 = "pvd1";
    private static final String PV_DOUBLE_2 = "pvd2";
    private static final String PV_LONG_1 = "pvl1";
    private static final String PV_LONG_2 = "pvl2";
    private static final String PV_STRING_1 = "pvs1";
    private static final String PV_STRING_2 = "pvs2";
    private static final String PV_STRING_1C = "PVS1";

    private static final String TEST_IP = "1.2.3.4";

    private ExpressionEvaluator evaluator;
    private Token token;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        evaluator = new ExpressionEvaluator();

        CachedPVProvider pvSource = mock(CachedPVProvider.class);
        PV pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createDoubleValue(null,
                null, null, null, new double[] { 1d }));
        when(pvSource.getPV(PV_DOUBLE_1)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createDoubleValue(null,
                null, null, null, new double[] { 2d }));
        when(pvSource.getPV(PV_DOUBLE_2)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createLongValue(null, null, null, null, new long[] { 1L }));
        when(pvSource.getPV(PV_LONG_1)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createLongValue(null, null, null, null, new long[] { 2L }));
        when(pvSource.getPV(PV_LONG_2)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createStringValue(null, null, null, new String[] { "test1" }));
        when(pvSource.getPV(PV_STRING_1)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createStringValue(null, null, null, new String[] { "test2" }));
        when(pvSource.getPV(PV_STRING_2)).thenReturn(pv);
        pv = mock(PV.class);
        when(pv.getValue()).thenReturn(ValueFactory.createStringValue(null, null, null, new String[] { "TEST1" }));
        when(pvSource.getPV(PV_STRING_1C)).thenReturn(pv);
        
        EntityManager entityManager = mock(EntityManager.class);
        TypedQuery<IPGroup> query = mock(TypedQuery.class);
        when(query.setParameter("name", "myGroup")).thenReturn(query);
        IPGroup group = new IPGroup();
        group.setName("myGroup");
        Set<String> ips = new HashSet<>();
        ips.add("1.2.3.4");
        group.setIp(ips);
        when(query.getSingleResult()).thenReturn(group);
        when(entityManager.createNamedQuery("IPGroup.findByName", IPGroup.class)).thenReturn(query);
        
        Field f = ExpressionEvaluator.class.getDeclaredField("pvSource");
        f.setAccessible(true);
        f.set(evaluator, pvSource);
        
        f = ExpressionEvaluator.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(evaluator, entityManager);
        token = new Token();
        token.setIp(TEST_IP);
    }

    /**
     * Test a few invalid expressions.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testInvaidPatterns() throws EvaluationException {
        Expression expression = new Expression();
        expression.setDefinition("PV " + PV_DOUBLE_1 + " = ");
        try {
            evaluator.evaluate(expression, token);
            fail("Invalid pattern should result in an exception.");
        } catch (EvaluationException e) {
            assertEquals(Messages.getString(Messages.INVALID_EXPRESSION, expression.getDefinition()), e.getMessage());
        }
        expression.setDefinition("P " + PV_DOUBLE_1 + " = 1");
        try {
            evaluator.evaluate(expression, token);
            fail("Invalid pattern should result in an exception.");
        } catch (EvaluationException e) {
            assertEquals(Messages.getString(Messages.INVALID_EXPRESSION, expression.getDefinition()), e.getMessage());
        }
        expression.setDefinition("PV " + PV_DOUBLE_1 + " ! 1");
        try {
            evaluator.evaluate(expression, token);
            fail("Invalid pattern should result in an exception.");
        } catch (EvaluationException e) {
            assertEquals(Messages.getString(Messages.INVALID_EXPRESSION, expression.getDefinition()), e.getMessage());
        }
        expression.setDefinition("IP 1.2.3.4");
        try {
            evaluator.evaluate(expression, token);
            fail("Invalid pattern should result in an exception.");
        } catch (EvaluationException e) {
            assertEquals(Messages.getString(Messages.INVALID_EXPRESSION, expression.getDefinition()), e.getMessage());
        }
        expression.setDefinition("IP < 1.2.3.4");
        try {
            evaluator.evaluate(expression, token);
            fail("Invalid pattern should result in an exception.");
        } catch (EvaluationException e) {
            assertEquals(Messages.getString(Messages.INVALID_EXPRESSION, expression.getDefinition()), e.getMessage());
        }
        expression.setDefinition("IP = 2.3.4");
        assertFalse("Invalid IP is not resolvable", evaluator.evaluate(expression, token));
    }

    /**
     * Test the expressions related to numeric type PVs.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testNumberPVs() throws EvaluationException {
        Expression expression = new Expression();

        // Test PV and a scalar.
        // Test =
        expression.setDefinition("PV " + PV_DOUBLE_1 + " = 1");
        assertTrue(evaluator.evaluate(expression, token));
        // Test !=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " != 2");
        assertTrue(evaluator.evaluate(expression, token));
        // Test <
        expression.setDefinition("PV " + PV_DOUBLE_1 + " < 2");
        assertTrue(evaluator.evaluate(expression, token));
        // Test <=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " <= 2");
        assertTrue(evaluator.evaluate(expression, token));
        // Test >
        expression.setDefinition("PV " + PV_DOUBLE_1 + " > 0");
        assertTrue(evaluator.evaluate(expression, token));
        // Test >=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " >= 0");
        assertTrue(evaluator.evaluate(expression, token));

        // Test PVs on both sides.
        // Test =
        expression.setDefinition("PV " + PV_DOUBLE_1 + " = " + PV_DOUBLE_2);
        assertFalse(evaluator.evaluate(expression, token));
        // Test !=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " != " + PV_DOUBLE_2);
        assertTrue(evaluator.evaluate(expression, token));
        // Test <
        expression.setDefinition("PV " + PV_DOUBLE_1 + " < " + PV_DOUBLE_2);
        assertTrue(evaluator.evaluate(expression, token));
        // Test <=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " <= " + PV_DOUBLE_2);
        assertTrue(evaluator.evaluate(expression, token));
        // Test >
        expression.setDefinition("PV " + PV_DOUBLE_1 + " > " + PV_DOUBLE_2);
        assertFalse(evaluator.evaluate(expression, token));
        // Test >=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " >= " + PV_DOUBLE_2);
        assertFalse(evaluator.evaluate(expression, token));

        // Test interchangeable numerical type PVs.
        // Test =
        expression.setDefinition("PV " + PV_DOUBLE_1 + " = " + PV_LONG_1);
        assertTrue(evaluator.evaluate(expression, token));
        // Test !=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " != " + PV_LONG_1);
        assertFalse(evaluator.evaluate(expression, token));
        // Test <
        expression.setDefinition("PV " + PV_DOUBLE_1 + " < " + PV_LONG_1);
        assertFalse(evaluator.evaluate(expression, token));
        // Test <=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " <= " + PV_LONG_1);
        assertTrue(evaluator.evaluate(expression, token));
        // Test >
        expression.setDefinition("PV " + PV_DOUBLE_1 + " > " + PV_LONG_1);
        assertFalse(evaluator.evaluate(expression, token));
        // Test >=
        expression.setDefinition("PV " + PV_DOUBLE_1 + " >= " + PV_LONG_1);
        assertTrue(evaluator.evaluate(expression, token));
    }

    /**
     * Test the expressions related to the string type PVs.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testStrings() throws EvaluationException {
        Expression expression = new Expression();

        // Test =
        expression.setDefinition("PV " + PV_STRING_1 + " = " + PV_STRING_2);
        assertFalse(evaluator.evaluate(expression, token));

        // Test !=
        expression.setDefinition("PV " + PV_STRING_1 + " != " + PV_STRING_2);
        assertTrue(evaluator.evaluate(expression, token));

        // Test <
        expression.setDefinition("PV " + PV_STRING_1 + " < " + PV_STRING_2);
        assertTrue(evaluator.evaluate(expression, token));

        // Test <=
        expression.setDefinition("PV " + PV_STRING_1 + " <= " + PV_STRING_2);
        assertTrue(evaluator.evaluate(expression, token));

        // Test >
        expression.setDefinition("PV " + PV_STRING_1 + " > " + PV_STRING_2);
        assertFalse(evaluator.evaluate(expression, token));

        // Test >=
        expression.setDefinition("PV " + PV_STRING_1 + " >= " + PV_STRING_2);
        assertFalse(evaluator.evaluate(expression, token));

        // Test upper case.
        expression.setDefinition("PV " + PV_STRING_1 + " != " + PV_STRING_1C);
        assertTrue(evaluator.evaluate(expression, token));

        expression.setDefinition("PV " + PV_STRING_1 + " > " + PV_STRING_1C);
        assertTrue(evaluator.evaluate(expression, token));
    }
    
    /**
     * Test the expressions related to IP.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testIPEquals() throws EvaluationException {
        Expression expression = new Expression();
        
        expression.setDefinition("IP = 1.2.3.4");
        assertTrue(evaluator.evaluate(expression, token));
        
        expression.setDefinition("IP != 1.1.1.1");
        assertTrue(evaluator.evaluate(expression, token));
        
        expression.setDefinition("IP @ myGroup");
        assertTrue(evaluator.evaluate(expression, token));
        expression.setDefinition("IP !@ myGroup");
        assertFalse(evaluator.evaluate(expression, token));
        
        token.setIp("1.1.1.1");
        expression.setDefinition("IP @ myGroup");
        assertFalse(evaluator.evaluate(expression, token));
        expression.setDefinition("IP !@ myGroup");
        assertTrue(evaluator.evaluate(expression, token));
    }

}
