/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.logic.AuthorizationEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.evaluation.RuleEvaluator;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>AuthorizationEJBTest</code> tests the methods of the {@link AuthorizationEJB}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class AuthorizationEJBTest {

    private AuthorizationEJB ejb;
    private EntityManager em;
    private Token token;
    private TypedQuery<Token> tokenQuery;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        ejb = new AuthorizationEJB();

        token = new Token();
        token.setCreationDate(new Timestamp(System.currentTimeMillis() - 10000));
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));
        token.setIp("192.168.1.1");
        token.setTokenId("id123");
        token.setUserId("johndoe");
        Role r = new Role();
        r.setName("Role");
        Field f = Role.class.getDeclaredField("id");
        f.setAccessible(true);
        f.set(r, 1);
        UserRole ur = new UserRole();
        ur.setUserId("johndoe");
        ur.setRole(r);
        token.setRole(new HashSet<>(Arrays.asList(new TokenRole(token, ur))));

        em = mock(EntityManager.class);
        tokenQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Token.findByTokenId", Token.class)).thenReturn(tokenQuery);
        when(tokenQuery.setParameter("tokenId", "id123")).thenReturn(tokenQuery);
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token));

        Resource resource = new Resource();
        resource.setName("resource");
        TypedQuery<Resource> resourceQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Resource.findByName", Resource.class)).thenReturn(resourceQuery);
        when(resourceQuery.setParameter("name", "resource")).thenReturn(resourceQuery);
        when(resourceQuery.getResultList()).thenReturn(Arrays.asList(resource));

        Role role2 = new Role();
        role2.setName("Role2");
        f.set(role2, 3);
        TypedQuery<Role> roleQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Role.findValidByUserId", Role.class)).thenReturn(roleQuery);
        when(roleQuery.setParameter("userId", "johndoe")).thenReturn(roleQuery);
        when(roleQuery.getResultList()).thenReturn(Arrays.asList(r));

        UserRole ur2 = new UserRole();
        ur2.setRole(r);
        ur2.setUserId("johndoe");
        TypedQuery<UserRole> roleQuery2 = mock(TypedQuery.class);
        when(em.createNamedQuery("UserRole.findValidByUserId", UserRole.class)).thenReturn(roleQuery2);
        when(roleQuery2.setParameter("userId", "johndoe")).thenReturn(roleQuery2);
        when(roleQuery2.getResultList()).thenReturn(Arrays.asList(ur2));

        Permission permission1 = new Permission();
        permission1.setName("p1");
        permission1.setResource(resource);
        permission1.setRole(new HashSet<>(Arrays.asList(role2)));
        permission1.setExclusiveAccessAllowed(Boolean.FALSE);
        Permission permission2 = new Permission();
        permission2.setName("p2");
        permission2.setResource(resource);
        permission2.setRole(new HashSet<>(Arrays.asList(r)));
        permission2.setExclusiveAccessAllowed(Boolean.TRUE);

        TypedQuery<Permission> permissionQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Permission.findByNames", Permission.class)).thenReturn(permissionQuery);
        when(permissionQuery.setParameter(any(String.class), any(String[].class))).thenReturn(permissionQuery);
        when(permissionQuery.setParameter("resource", "resource")).thenReturn(permissionQuery);
        when(permissionQuery.getResultList()).thenReturn(Arrays.asList(permission1, permission2));

        TypedQuery<Permission> permissionQuery2 = mock(TypedQuery.class);
        when(em.createNamedQuery("Permission.findByName", Permission.class)).thenReturn(permissionQuery2);
        when(permissionQuery2.setParameter("name", "p1")).thenReturn(permissionQuery2);
        when(permissionQuery2.setParameter("resource", "resource")).thenReturn(permissionQuery2);
        when(permissionQuery2.getResultList()).thenReturn(Arrays.asList(permission1));

        RuleEvaluator re = mock(RuleEvaluator.class);
        when(re.evaluate(any(Rule.class), any(Token.class))).thenReturn(true);
        
        f = AuthorizationEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(ejb, em);

        GeneralEJB general = new GeneralEJB();
        f = GeneralEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(general, em);
        
        f = AuthorizationEJB.class.getDeclaredField("generalEJB");
        f.setAccessible(true);
        f.set(ejb, general);
        
        f = AuthorizationEJB.class.getDeclaredField("ruleEvaluatorEJB");
        f.setAccessible(true);
        f.set(ejb, re);
    }

    /**
     * Tests {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} when invalid parameters are
     * provided.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithInvalidParameters() throws RBACException {
        // test null parameters
        try {
            ejb.hasPermissions(null, "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.hasPermissions("", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.hasPermissions("id123", null, new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.hasPermissions("id123", "", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.hasPermissions("id123", "resource", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
        try {
            ejb.hasPermissions("id123", "resource", new String[0]);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} when there is no exclusive access
     * set.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testNormal() throws RBACException {
        // test normal
        PermissionsInfo info = ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" }).getInfo();
        assertEquals("Resource name should match", "resource", info.getResourceName());
        assertFalse("p1 should be denied", info.isPermissionGranted("p1"));
        assertTrue("p2 should be granted", info.isPermissionGranted("p2"));
    }

    /**
     * Test that proper exceptions are thrown if the permission list is invalid.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithInvalidPermissions() throws RBACException {
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2", "p3" });
            fail("Exception should occur");
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected", "All requested permissions for resource 'resource' do not exist.",
                    e.getMessage());
        }

        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1" });
            fail("Exception should occur");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple permissions named '[p1]' exist for the resource 'resource'.",
                    e.getMessage());
        }

    }

    /**
     * Test {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} if the resource does not exist.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithoutResource() throws RBACException {
        // test if resource does not exist
        TypedQuery<Resource> resourceQuery = em.createNamedQuery("Resource.findByName", Resource.class);
        when(resourceQuery.getResultList()).thenReturn(new ArrayList<Resource>());
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (InvalidResourceException e) {
            assertEquals("Exception message should match", "Resource with name 'resource' could not be found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} if the token has expired.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithExpiredToken() throws RBACException {
        // test with expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception message should match", "Token owned by '" + token.getUserId()
                    + "' has already expired.", e.getMessage());
        }
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 100000));

        // test if token contains an expired role - token has expired
        Set<TokenRole> roles = token.getRole();
        roles.iterator().next().getRole().setEndTime(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception message should match", "Token owned by '" + token.getUserId()
                    + "' has already expired.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} if there is no token or there are
     * multiple.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithoutToken() throws RBACException {
        // test if the token does not exist
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        }

        // test if multiple tokens are returned
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" });
            fail("Exception should occur");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }
    }

    /**
     * Test the {@link AuthorizationEJB#hasPermissions(String, String, String, String[])} when exclusive access is set
     * on the checked permissions.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testWithExclusiveAccess() throws RBACException {
        TypedQuery<Permission> query = em.createNamedQuery("Permission.findByNames", Permission.class);
        Permission p = query.getResultList().get(1);
        ExclusiveAccess access = new ExclusiveAccess();
        access.setEndTime(new Timestamp(System.currentTimeMillis() + 10000));
        access.setPermission(p);
        access.setStartTime(new Timestamp(System.currentTimeMillis() - 10000));
        access.setUserId("johndoe");
        p.setExclusiveAccess(access);

        // test with a valid exclusive access, that johndoe owns
        PermissionsInfo info = ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" }).getInfo();
        assertFalse("p1 should be denied", info.isPermissionGranted("p1"));
        assertTrue("p2 should be granted", info.isPermissionGranted("p2"));

        // test with a valid exclusive access, that johndoe does not own
        access.setUserId("janedoe");
        info = ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" }).getInfo();
        assertFalse("p1 should be denied", info.isPermissionGranted("p1"));
        assertFalse("p2 should be denied, Jane has exclusive access", info.isPermissionGranted("p2"));

        // test with an invalid exclusive access, that johndoe does not own
        access.setEndTime(new Timestamp(System.currentTimeMillis() - 1000));
        info = ejb.hasPermissions("id123", "resource", new String[] { "p1", "p2" }).getInfo();
        assertFalse("p1 should be denied", info.isPermissionGranted("p1"));
        assertTrue("p2 should be granted, Jane's exclusive access has expired", info.isPermissionGranted("p2"));
    }
}
