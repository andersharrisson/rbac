/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Field;

import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;
import org.csstudio.utility.pv.PVFactory;
import org.junit.Test;

import se.esss.ics.rbac.logic.evaluation.CachedPVProvider;

/**
 * <code>PVProviderTest</code> tests the methods the the {@link CachedPVProvider}.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class PVProviderTest {
    
    public static class MyPVFactory implements PVFactory {
        /* @see org.csstudio.utility.pv.PVFactory#createPV(java.lang.String) */
        @Override
        public PV createPV(String name) throws PVException {
            if ("existingPV".equals(name)) {
                PV pv = mock(PV.class);
                return pv;
            } else if ("existingPV2".equals(name)) {
                PV pv = mock(PV.class);
                return pv;
            } 
            return null;
        }
    }

    /**
     * Test that the PVs can be created and that cached values are returned when possible.
     * 
     * @throws Exception
     */
    @Test
    public void testGetPV() throws Exception {        
        CachedPVProvider provider = new CachedPVProvider();
        Field f = CachedPVProvider.class.getDeclaredField("pvFactory");
        f.setAccessible(true);
        f.set(provider, new MyPVFactory());
        
        PV pv = provider.getPV("existingPV",false);
        assertNotNull("The PV should not be null",pv);
        verify(pv, only()).start();        
        
        try {
            pv = provider.getPV("nonExistingPV",false);
            fail("Exception should occur.");
        } catch (PVException e) {
            assertEquals("Could not connect to PV 'nonExistingPV'.",e.getMessage());
            assertEquals("nonExistingPV", e.getPVName());
        }
        
        pv = provider.getPV("existingPV2",true);
        assertNotNull("The PV should not be null",pv);
        verify(pv, times(1)).start();
        verify(pv, times(1)).blockUntilFirstUpdate(2000);

        //additional request should return the cached value
        PV pv2 = provider.getPV("existingPV2",true);
        assertEquals("The cached PV should be returned",pv, pv2);
        verify(pv2, times(1)).start();
        verify(pv2, times(2)).blockUntilFirstUpdate(2000);
        
        PV pv3 = provider.getPV("existingPV2");
        assertEquals("The cached PV should be returned",pv, pv3);
        verify(pv3, times(1)).start();
        verify(pv3, times(3)).blockUntilFirstUpdate(2000);
    }
    
}
