/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.DatatypeConverter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.ResourcesInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;
import se.esss.ics.rbac.logic.AuthenticationEJB;
import se.esss.ics.rbac.logic.AuthorizationEJB;
import se.esss.ics.rbac.logic.ExclusiveAccessEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.ManifestEJB;
import se.esss.ics.rbac.logic.PVAccessEJB;
import se.esss.ics.rbac.logic.RBACLogger;
import se.esss.ics.rbac.logic.datatypes.ExclusiveInfoWrapper;
import se.esss.ics.rbac.logic.datatypes.PermissionsInfoWrapper;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.RBACRuntimeException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.restservices.RBACResources;
import se.esss.ics.rbac.restservices.RequestUtilities;
import se.esss.ics.rbac.RBACLog.Action;

/**
 * 
 * <code>AuthResourcesTest</code> tests the methods of the {@link RBACResources}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class AuthResourcesTest {

    private RBACResources resources;

    @Before
    public void setUp() throws Exception {
        resources = new RBACResources();
        setEJB(mock(RBACLogger.class));
    }

    private void setEJB(Object ejb) throws Exception {
        Field f = null;
        if (ejb instanceof AuthenticationEJB) {
            f = RBACResources.class.getDeclaredField("authenticationEJB");
        } else if (ejb instanceof AuthorizationEJB) {
            f = RBACResources.class.getDeclaredField("authorizationEJB");
        } else if (ejb instanceof GeneralEJB) {
            f = RBACResources.class.getDeclaredField("generalEJB");
        } else if (ejb instanceof ExclusiveAccessEJB) {
            f = RBACResources.class.getDeclaredField("exclusiveAccessEJB");
        } else if (ejb instanceof PVAccessEJB) {
            f = RBACResources.class.getDeclaredField("pvAccessEJB");
        } else if (ejb instanceof ManifestEJB) {
            f = RBACResources.class.getDeclaredField("manifestEJB");
        } else if (ejb instanceof RBACLogger) {
            f = RBACResources.class.getDeclaredField("rbacLogger");
        }
        if (f == null) {
            throw new IllegalArgumentException(ejb.getClass() + " is not recognized.");
        }
        f.setAccessible(true);
        f.set(resources, ejb);
    }

    /**
     * Test the {@link RBACResources#getVersion()} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetVersion() throws Exception {
        ManifestEJB manifest = mock(ManifestEJB.class);
        when(manifest.getVersion()).thenReturn("Ver 1.0");
        setEJB(manifest);

        Response response = resources.getVersion();

        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Version number should be", "Ver 1.0", response.getEntity());

        when(manifest.getVersion()).thenThrow(new IOException());
        response = resources.getVersion();
        assertEquals("Status should be Server Error", Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                response.getStatus());

        verify(manifest, times(2)).getVersion();
    }

    /**
     * Test the {@link RBACResources#getPublicKey()} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetPublicKey() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        when(ejb.getPublicKeyData()).thenReturn(new byte[] { 1, 2, 3, 4 });
        setEJB(ejb);

        Response response = resources.getPublicKey();

        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        assertArrayEquals("Data should match", new byte[] { 1, 2, 3, 4 }, (byte[]) response.getEntity());

        when(ejb.getPublicKeyData()).thenThrow(new RBACException(null, "Error"));
        response = resources.getPublicKey();
        assertEquals("Status should be Server Error", Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                response.getStatus());

        verify(ejb, times(2)).getPublicKeyData();
    }

    /**
     * Test the {@link RBACResources#getRoles(String, HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetRolesForUser() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        ArrayList<String> roles = new ArrayList<>();
        roles.add("Role 1");
        roles.add("Role 2");
        when(ejb.getUserRoles("username")).thenReturn(new RolesInfo("username", roles));
        when(ejb.getUserRoles("someone")).thenReturn(new RolesInfo("someone", new ArrayList<String>()));
        when(ejb.getUserRoles("blabla")).thenThrow(new IllegalRBACArgumentException("Error"));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getRoles("username", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        RolesInfo info = (RolesInfo) response.getEntity();
        assertEquals("The username of the roles info should be \"username\"", "username", info.getUserID());
        assertArrayEquals("The roles of the roles info should be Role 1 and Role 2",
                new String[] { "Role 1", "Role 2" }, info.getRoleNames().toArray(new String[0]));

        response = resources.getRoles("someone", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        info = (RolesInfo) response.getEntity();
        assertEquals("The username of the roles info should be \"someone\"", "someone", info.getUserID());
        assertArrayEquals("There should be ne roles", new String[0], info.getRoleNames().toArray(new String[0]));

        response = resources.getRoles("foobar", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        info = (RolesInfo) response.getEntity();
        assertNull("There should be no return object", info);

        response = resources.getRoles("blabla", request);
        assertEquals("Status should be Bad Request", Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        String error = (String) response.getEntity();
        assertEquals("The returned value should be exception message", "Error", error);

        verify(ejb, times(1)).getUserRoles("username");
        verify(ejb, times(1)).getUserRoles("someone");
        verify(ejb, times(1)).getUserRoles("blabla");
        verify(ejb, times(1)).getUserRoles("foobar");
    }
    
    /**
     * Test the {@link RBACResources#getRoles(HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetRoles() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        ArrayList<String> roles = new ArrayList<>();
        roles.add("Role 1");
        roles.add("Role 2");
        when(ejb.getRoles()).thenReturn(new RolesInfo(null, roles));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getRoles(request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        RolesInfo info = (RolesInfo) response.getEntity();
        assertNull("The username of the roles info should be null", info.getUserID());
        assertArrayEquals("The roles of the roles info should be Role 1 and Role 2",
                new String[] { "Role 1", "Role 2" }, info.getRoleNames().toArray(new String[0]));

        verify(ejb, Mockito.only()).getRoles();
    }
    
    /**
     * Test the {@link RBACResources#getUsersWithRole(String, HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetUsersWithRole() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        ArrayList<String> users = new ArrayList<>();
        users.add("johndoe");
        users.add("janedoe");
        when(ejb.getUsersWithRole("role")).thenReturn(new UsersInfo("role",users));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getUsersWithRole("role",request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        UsersInfo info = (UsersInfo) response.getEntity();
        assertArrayEquals("The usernames of the users info should be johndoe and janedoe.",
                new String[] { "johndoe", "janedoe" }, info.getUsers().toArray(new String[0]));

        verify(ejb, Mockito.only()).getUsersWithRole("role");
    }
    
    /**
     * Test the {@link RBACResources#getUsersWithPermission(String, String, HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetUsersWithPermission() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        ArrayList<String> users = new ArrayList<>();
        users.add("johndoe");
        users.add("janedoe");
        when(ejb.getUsersWithPermission("resource","permission")).thenReturn(new UsersInfo("resource",
                "permission",users));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getUsersWithPermission("resource", "permission", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        UsersInfo info = (UsersInfo) response.getEntity();
        assertArrayEquals("The usernames of the users info should be johndoe and janedoe.",
                new String[] { "johndoe", "janedoe" }, info.getUsers().toArray(new String[0]));

        verify(ejb, Mockito.only()).getUsersWithPermission("resource","permission");
    }

    /**
     * Test the {@link RBACResources#getResources(HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetResources() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        ArrayList<String> roles = new ArrayList<>();
        roles.add("Resource 1");
        roles.add("Resource 2");
        when(ejb.getResources()).thenReturn(new ResourcesInfo(roles));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getResources(request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        ResourcesInfo info = (ResourcesInfo) response.getEntity();
        assertArrayEquals("The resources of the resource info should be Resource 1 and Resource 2",
                new String[] { "Resource 1", "Resource 2" }, info.getResourceNames().toArray(new String[0]));

        verify(ejb, Mockito.only()).getResources();
    }

    /**
     * Test the {@link RBACResources#getPermissions(String, HttpServletRequest)} method.
     *  
     * @throws Exception on error
     */
    @Test
    public void testGetPermissions() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        Map<String,Boolean> perms = new LinkedHashMap<>(2);
        perms.put("permission 1", Boolean.TRUE);
        perms.put("permission 2", Boolean.TRUE);
        when(ejb.getPermissions("Resource 1")).thenReturn(new PermissionsInfo("Resource 1",perms));
        when(ejb.getPermissions("Resource 2")).thenReturn(new PermissionsInfo("Resource 2",
                new HashMap<String,Boolean>(0)));
        when(ejb.getPermissions("Resource 3")).thenThrow(new InvalidResourceException(null, "Resource 3", null));
        when(ejb.getPermissions(null)).thenThrow(new IllegalRBACArgumentException(null));
        setEJB(ejb);

        HttpServletRequest request = mock(HttpServletRequest.class);

        Response response = resources.getPermissions("Resource 1",request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        PermissionsInfo info = (PermissionsInfo) response.getEntity();
        assertEquals("Resource name should be Resource 1", "Resource 1", info.getResourceName());
        assertEquals("2 permissions should be returned", 2, info.getPermissions().size());
        assertTrue("Permission 'permission 1' should be included", info.getPermissions().containsKey("permission 1"));
        assertTrue("Permission 'permission 2' should be included", info.getPermissions().containsKey("permission 2"));
        
        response = resources.getPermissions("Resource 2",request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        info = (PermissionsInfo) response.getEntity();
        assertEquals("Resource name should be Resource 2", "Resource 2", info.getResourceName());
        assertTrue("0 permissions should be returned", info.getPermissions().isEmpty());
        
        response = resources.getPermissions("Resource 3",request);
        assertEquals("Status should be Not Found", Status.NOT_FOUND.getStatusCode(), response.getStatus());
        info = (PermissionsInfo) response.getEntity();
        assertNull("There should be no return object", info);
        
        response = resources.getPermissions(null,request);
        assertEquals("Status should be Bad Request", Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        info = (PermissionsInfo) response.getEntity();
        assertNull("There should be no return object", info);      
        
        verify(ejb, times(1)).getPermissions("Resource 1");
        verify(ejb, times(1)).getPermissions("Resource 2");
        verify(ejb, times(1)).getPermissions("Resource 3");
        verify(ejb, times(1)).getPermissions(null);
    }
    
    /**
     * Test {@link RBACResources#login(HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testLogin() throws Exception {
        AuthenticationEJB auth = mock(AuthenticationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        when(request.getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION)).thenReturn(
                HttpServletRequest.BASIC_AUTH + " " + getUsernameAndPassword("johndoe"));
        TokenInfo token = new TokenInfo();
        token.setCreationTime(System.currentTimeMillis());
        token.setExpirationTime(System.currentTimeMillis() + 3600000);
        token.setFirstName("John");
        token.setLastName("Doe");
        token.setIp("192.168.1.1");
        ArrayList<String> roles = new ArrayList<>();
        roles.add("Role 1");
        roles.add("Role 2");
        token.setRoleNames(roles);
        token.setSignature(new byte[64]);
        token.setTokenID("tokenID");
        token.setUserID("johndoe");
        when(auth.login("johndoe".toCharArray(), "password".toCharArray(), "192.168.1.1", new String[0])).thenReturn(
                token);
        when(auth.login("blabla".toCharArray(), "password".toCharArray(), "192.168.1.1", new String[0])).thenThrow(
                new RBACException(null, "exception"));
        setEJB(auth);

        Response response = resources.login(request);
        assertEquals("Status should be CREATED", Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals("The entity should be the token", token, response.getEntity());

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        when(request.getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION)).thenReturn(
                HttpServletRequest.BASIC_AUTH + " " + getUsernameAndPassword("blabla"));
        response = resources.login(request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                response.getStatus());
        assertEquals("The entity should match", "exception", response.getEntity());
    }

    /**
     * Test {@link RBACResources#logout(String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testLogout() throws Exception {
        AuthenticationEJB auth = mock(AuthenticationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        when(auth.logout("tokenID")).thenReturn(new TokenInfo());
        setEJB(auth);

        Response response = resources.logout("tokenID", request);
        assertEquals("Status should be NO CONTENT", Status.NO_CONTENT.getStatusCode(), response.getStatus());

        when(auth.logout("tokenID")).thenReturn(null);
        response = resources.logout("tokenID", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());

        when(auth.logout("tokenID")).thenThrow(new RBACException(null, "Error"));
        response = resources.logout("tokenID", request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                response.getStatus());
        assertEquals("The entity should match", "Error", response.getEntity());
    }

    /**
     * Test {@link RBACResources#hasPermissions(String, String, String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testHasPermissions() throws Exception {
        AuthorizationEJB ejb = mock(AuthorizationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        HashMap<String, Boolean> permissions = new HashMap<>();
        permissions.put("P1", Boolean.TRUE);
        permissions.put("P2", Boolean.FALSE);
        PermissionsInfoWrapper info = new PermissionsInfoWrapper(new PermissionsInfo("resource", permissions),
                "johndoe");
        when(ejb.hasPermissions("tokenID", "resource", new String[] { "P1", "P2" })).thenReturn(info);
        setEJB(ejb);

        Response response = resources.hasPermissions("tokenID", "resource", "P1,P2", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Entity should match", info.getInfo(), response.getEntity());

        when(ejb.hasPermissions("tokenID", "resource", new String[] { "bla" })).thenThrow(
                new InvalidResourceException(null, null, "Error"));

        response = resources.hasPermissions("tokenID", "resource", "bla", request);
        assertEquals("Status should be NOT FOUND", Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertEquals("Entity should match", "Error", response.getEntity());
    }

    /**
     * Test {@link RBACResources#getToken(String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetToken() throws Exception {
        AuthenticationEJB auth = mock(AuthenticationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        TokenInfo token = new TokenInfo();
        token.setCreationTime(System.currentTimeMillis());
        token.setExpirationTime(System.currentTimeMillis() + 1000);
        token.setFirstName("John");
        token.setLastName("Doe");
        token.setIp("192.168.1.1");
        token.setSignature(new byte[64]);
        token.setTokenID("tokenID");
        token.setUserID("johndoe");
        when(auth.getToken("tokenID")).thenReturn(token);
        when(auth.getToken("tokenID1")).thenThrow(new RBACException("user", "Error"));
        when(auth.getToken("tokenID2")).thenThrow(new AuthAndAuthException(null, null, "Error"));
        when(auth.getToken("tokenID12")).thenThrow(new TokenInvalidException(null, null, "Error"));
        when(auth.getToken("tokenID3")).thenThrow(new DataConsistencyException(null, "Error"));
        when(auth.getToken("tokenID4")).thenThrow(new InvalidResourceException(null, null, "Error"));
        when(auth.getToken("tokenID5")).thenThrow(new IllegalRBACArgumentException("Error"));
        setEJB(auth);

        Response r = resources.getToken("tokenID", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", token, r.getEntity());

        r = resources.getToken("tokenID1", request);
        assertEquals("Status should be SERVER_ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.getToken("tokenID2", request);
        assertEquals("Status should be FORBIDDEN", Status.FORBIDDEN.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.getToken("tokenID12", request);
        assertEquals("Status should be GONE", Status.GONE.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.getToken("tokenID3", request);
        assertEquals("Status should be SERVER_ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.getToken("tokenID4", request);
        assertEquals("Status should be NOT FOUND", Status.NOT_FOUND.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.getToken("tokenID5", request);
        assertEquals("Status should be BAD REQUEST", Status.BAD_REQUEST.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());
    }

    /**
     * Test {@link RBACResources#isTokenValid(String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testIsTokenValid() throws Exception {
        AuthenticationEJB auth = mock(AuthenticationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        TokenInfo token = new TokenInfo();
        token.setCreationTime(System.currentTimeMillis());
        token.setExpirationTime(System.currentTimeMillis() + 1000);
        token.setFirstName("John");
        token.setLastName("Doe");
        token.setIp("192.168.1.1");
        token.setSignature(new byte[64]);
        token.setTokenID("tokenID");
        token.setUserID("johndoe");
        when(auth.isTokenValid("tokenID")).thenReturn(token);
        when(auth.isTokenValid("tokenID12")).thenThrow(new TokenInvalidException(null, null, "Error"));
        when(auth.isTokenValid("tokenID3")).thenThrow(new DataConsistencyException(null, "Error"));
        when(auth.isTokenValid("tokenID5")).thenThrow(new IllegalRBACArgumentException("Error"));
        setEJB(auth);

        Response r = resources.isTokenValid("tokenID", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());

        r = resources.isTokenValid("tokenID12", request);
        assertEquals("Status should be GONE", Status.GONE.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.isTokenValid("tokenID3", request);
        assertEquals("Status should be SERVER_ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.isTokenValid("tokenID5", request);
        assertEquals("Status should be BAD REQUEST", Status.BAD_REQUEST.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());
    }

    /**
     * Test {@link RBACResources#renewToken(String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRenewToken() throws Exception {
        AuthenticationEJB auth = mock(AuthenticationEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        TokenInfo token = new TokenInfo();
        token.setCreationTime(System.currentTimeMillis());
        token.setExpirationTime(System.currentTimeMillis() + 1000);
        token.setFirstName("John");
        token.setLastName("Doe");
        token.setIp("192.168.1.1");
        token.setSignature(new byte[64]);
        token.setTokenID("tokenID");
        token.setUserID("johndoe");
        when(auth.renewToken("tokenID")).thenReturn(token);
        when(auth.renewToken("tokenID1")).thenThrow(new RBACException(null, "Error"));
        when(auth.renewToken("tokenID2")).thenThrow(new AuthAndAuthException(null, null, "Error"));
        when(auth.renewToken("tokenID12")).thenThrow(new TokenInvalidException(null, null, "Error"));
        when(auth.renewToken("tokenID3")).thenThrow(new DataConsistencyException(null, "Error"));
        when(auth.renewToken("tokenID4")).thenThrow(new InvalidResourceException(null, null, "Error"));
        when(auth.renewToken("tokenID5")).thenThrow(new IllegalRBACArgumentException("Error"));
        setEJB(auth);

        Response r = resources.renewToken("tokenID", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", token, r.getEntity());

        r = resources.renewToken("tokenID1", request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.renewToken("tokenID2", request);
        assertEquals("Status should be FORBIDDEN", Status.FORBIDDEN.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.renewToken("tokenID12", request);
        assertEquals("Status should be GONE", Status.GONE.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.renewToken("tokenID3", request);
        assertEquals("Status should be SERVER_ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.renewToken("tokenID4", request);
        assertEquals("Status should be NOT FOUND", Status.NOT_FOUND.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.renewToken("tokenID5", request);
        assertEquals("Status should be BAD REQUEST", Status.BAD_REQUEST.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());
    }

    /**
     * Test {@link RBACResources#requestExclusiveAccess(String, String, String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRequestExclusiveAccess() throws Exception {
        ExclusiveAccessEJB ejb = mock(ExclusiveAccessEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        when(request.getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION)).thenReturn("6000000");

        ExclusiveInfoWrapper info = new ExclusiveInfoWrapper(new ExclusiveInfo("permission", "resource",
                System.currentTimeMillis() + 10000), "johndoe", false);
        when(ejb.requestExclusiveAccess("tokenID", "resource", "permission", 6000000)).thenReturn(info);
        when(ejb.requestExclusiveAccess(null, "resource", "permission", 6000000)).thenThrow(
                new IllegalRBACArgumentException("null"));
        when(ejb.requestExclusiveAccess("tokenID1", "resource", "permission", 6000000)).thenThrow(
                new AuthAndAuthException(null, null, "Error"));
        when(ejb.requestExclusiveAccess("tokenID2", "resource", "permission", 6000000)).thenThrow(
                new DataConsistencyException(null, "Error"));
        when(ejb.requestExclusiveAccess("tokenID3", "resource", "permission", 6000000)).thenThrow(
                new InvalidResourceException(null, null, "Error"));
        when(ejb.requestExclusiveAccess("tokenID12", "resource", "permission", 6000000)).thenThrow(
                new TokenInvalidException(null, null, "Error"));
        setEJB(ejb);

        Response r = resources.requestExclusiveAccess("tokenID", "resource", "permission", request);
        assertEquals("Status should be CREATED", Status.CREATED.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", info.getInfo(), r.getEntity());

        r = resources.requestExclusiveAccess(null, "resource", "permission", request);
        assertEquals("Status should be BAD REQUEST", Status.BAD_REQUEST.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "null", r.getEntity());

        r = resources.requestExclusiveAccess("tokenID1", "resource", "permission", request);
        assertEquals("Status should be FORBIDDEN", Status.FORBIDDEN.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.requestExclusiveAccess("tokenID2", "resource", "permission", request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.requestExclusiveAccess("tokenID3", "resource", "permission", request);
        assertEquals("Status should be NOT FOUND", Status.NOT_FOUND.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.requestExclusiveAccess("tokenID12", "resource", "permission", request);
        assertEquals("Status should be GONE", Status.GONE.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());
    }

    /**
     * Test {@link RBACResources#releaseExclusiveAccess(String, String, String, HttpServletRequest)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testReleaseExclusiveAccess() throws Exception {
        ExclusiveAccessEJB ejb = mock(ExclusiveAccessEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");

        when(ejb.releaseExclusiveAccess("tokenID", "resource", "permission")).thenReturn(
                new ExclusiveInfoWrapper(new ExclusiveInfo(), "", true));
        when(ejb.releaseExclusiveAccess(null, "resource", "permission")).thenThrow(
                new IllegalRBACArgumentException("null"));
        when(ejb.releaseExclusiveAccess("tokenID1", "resource", "permission")).thenThrow(
                new AuthAndAuthException(null, null, "Error"));
        when(ejb.releaseExclusiveAccess("tokenID12", "resource", "permission")).thenThrow(
                new TokenInvalidException(null, null, "Error"));
        when(ejb.releaseExclusiveAccess("tokenID", "resource1", "permission")).thenReturn(
                new ExclusiveInfoWrapper(new ExclusiveInfo(), "", false));
        when(ejb.releaseExclusiveAccess("tokenID2", "resource", "permission")).thenThrow(
                new DataConsistencyException(null, "Error"));
        when(ejb.releaseExclusiveAccess("tokenID3", "resource", "permission")).thenThrow(
                new InvalidResourceException(null, null, "Error"));
        setEJB(ejb);

        Response r = resources.releaseExclusiveAccess("tokenID", "resource", "permission", request);
        assertEquals("Status should be NO CONTENT", Status.NO_CONTENT.getStatusCode(), r.getStatus());
        assertNull("Entity should be null", r.getEntity());

        r = resources.releaseExclusiveAccess(null, "resource", "permission", request);
        assertEquals("Status should be BAD REQUEST", Status.BAD_REQUEST.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "null", r.getEntity());

        r = resources.releaseExclusiveAccess("tokenID1", "resource", "permission", request);
        assertEquals("Status should be FORBIDDEN", Status.FORBIDDEN.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.releaseExclusiveAccess("tokenID12", "resource", "permission", request);
        assertEquals("Status should be GONE", Status.GONE.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.releaseExclusiveAccess("tokenID", "resource1", "permission", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertNull("Entity should be null", r.getEntity());

        r = resources.releaseExclusiveAccess("tokenID2", "resource", "permission", request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

        r = resources.releaseExclusiveAccess("tokenID3", "resource", "permission", request);
        assertEquals("Status should be NOT FOUND", Status.NOT_FOUND.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());
    }

    /**
     * Test {@link RBACResources#getAccessSecurityGroups(String)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetAccessSecurityGroups() throws Exception {
        PVAccessEJB ejb = mock(PVAccessEJB.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(ejb.generateACFContent(null, new String[] { "g1", "g2" })).thenReturn("Test");
        when(ejb.generateACFContent("d", new String[] { "g1", "g2" })).thenReturn("Test1");
        when(ejb.generateACFContent("d", null)).thenReturn("Test2");
        when(ejb.generateACFContent(null, new String[] { "g2" })).thenThrow(new RBACException(null, "Error"));

        setEJB(ejb);

        Response r = resources.getAccessSecurityGroups("g1,g2", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Test", r.getEntity());

        r = resources.getAccessSecurityGroups("d|g1,g2", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Test1", r.getEntity());

        r = resources.getAccessSecurityGroups("d|", request);
        assertEquals("Status should be OK", Status.OK.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Test2", r.getEntity());

        r = resources.getAccessSecurityGroups("g2", request);
        assertEquals("Status should be SERVER ERROR", Status.INTERNAL_SERVER_ERROR.getStatusCode(), r.getStatus());
        assertEquals("Entity should match", "Error", r.getEntity());

    }

    /**
     * Test the exception handling method in the resources.
     * 
     * @throws Exception on error
     */
    @Test
    public void testEJBExceptionHandling() throws Exception {
        GeneralEJB ejb = mock(GeneralEJB.class);
        when(ejb.getUserRoles(any(String.class))).thenThrow(new EJBException("EJB Exception"));
        setEJB(ejb);
        RBACLogger logger = mock(RBACLogger.class);
        setEJB(logger);
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRemoteAddr()).thenReturn("192.168.1.2");

        try {
            resources.getRoles("johndoe", request);
            fail("Exception should occur");
        } catch (EJBException e) {
            assertEquals("Exception expected", "EJB Exception", e.getMessage());
        }
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "EJB Exception");

        Mockito.reset(ejb);
        when(ejb.getUserRoles(any(String.class))).thenThrow(
                new EJBException("EJB Exception", new IllegalRBACArgumentException("ILLEGAL")));
        resources.getRoles("johndoe", request);
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "EJB Exception");
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "ILLEGAL");

        Mockito.reset(ejb);
        when(ejb.getUserRoles(any(String.class))).thenThrow(new IllegalArgumentException("Illegal Argument Exception"));
        try {
            resources.getRoles("johndoe", request);
            fail("Exception should occur");
        } catch (RBACRuntimeException e) {
            assertEquals("Exception expected", "Illegal Argument Exception", e.getCause().getMessage());
        }
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "EJB Exception");
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "ILLEGAL");
        verify(logger, Mockito.times(1)).error(null, "192.168.1.2", Action.ROLES, "Illegal Argument Exception");
    }

    /**
     * Generate username and password and encode them as they are encoded when sent by the client to the service. The
     * method uses the given username and a constant password.
     * 
     * @param username the username to use
     * @return the encoded username and password to be used in the auth header of the HTTP request
     * @throws CharacterCodingException
     */
    private static String getUsernameAndPassword(String username) throws CharacterCodingException {
        Charset utf8Charset = Charset.forName("UTF-8");
        byte[] usernameData = username.getBytes(utf8Charset);
        ByteBuffer buffer = utf8Charset.newEncoder().encode(CharBuffer.wrap("password"));
        byte[] passwordData = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
        byte[] rawAuthData = new byte[usernameData.length + passwordData.length + 1];
        System.arraycopy(usernameData, 0, rawAuthData, 0, usernameData.length);
        rawAuthData[usernameData.length] = (byte) ':';
        System.arraycopy(passwordData, 0, rawAuthData, usernameData.length + 1, passwordData.length);
        return DatatypeConverter.printBase64Binary(rawAuthData);
    }
}
