/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.logic.evaluation.EvaluationException;
import se.esss.ics.rbac.logic.evaluation.ExpressionEvaluator;
import se.esss.ics.rbac.logic.evaluation.RuleEvaluator;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.datamodel.Token;

/**
 * 
 * <code>RuleEvaluatorTest</code> test evaluation of rules using the {@link RuleEvaluator}.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class RuleEvaluatorTest {

    private static final String EXP1 = "Exp_1";
    private static final String EXP2 = "Exp_2";
    private static final String EXP3 = "Exp_3";
    private static final String EXP4 = "Exp_4";

    private RuleEvaluator evaluator;
    private Permission permission;
    private Rule rule;

    @Before
    public void setUp() throws Exception {
        permission = new Permission();
        rule = new Rule();
        permission.setRule(rule);
        Set<Expression> expressions = new LinkedHashSet<>();
        Expression exp1 = new Expression();
        exp1.setName(EXP1);
        Expression exp2 = new Expression();
        exp2.setName(EXP2);
        expressions.add(exp2);
        Expression exp3 = new Expression();
        exp3.setName(EXP3);
        expressions.add(exp3);
        Expression exp4 = new Expression();
        exp4.setName(EXP4);
        expressions.add(exp4);
        expressions.add(exp1);
        rule.setExpression(expressions);
        
        evaluator = new RuleEvaluator();
        ExpressionEvaluator expressionEvaluator = mock(ExpressionEvaluator.class);
        when(expressionEvaluator.evaluate(eq(exp1), any(Token.class))).thenReturn(Boolean.TRUE);
        when(expressionEvaluator.evaluate(eq(exp2), any(Token.class))).thenReturn(Boolean.TRUE);
        when(expressionEvaluator.evaluate(eq(exp3), any(Token.class))).thenReturn(Boolean.FALSE);
        when(expressionEvaluator.evaluate(eq(exp4), any(Token.class))).thenReturn(Boolean.FALSE);
        Field f = RuleEvaluator.class.getDeclaredField("expressionEvaluator");
        f.setAccessible(true);
        f.set(evaluator, expressionEvaluator);
        
    }

    /**
     * Tests evaluation of rules that contain ADN.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testANDPatterns() throws EvaluationException {
        // Test AND.
        rule.setDefinition(EXP1 + " &" + EXP2);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "& " + EXP2);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "&" + EXP2);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "&" + EXP3);
        assertFalse(evaluator.evaluate(rule, null));
    }
    
    /**
     * Tests evaluation of rules that contains brackets.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testBracketedPatterns() throws EvaluationException {
        // Test brackets.
        rule.setDefinition("( " + EXP1 + ")");
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "& " + EXP2);
        rule.setDefinition("(" + EXP1 + " )");
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition("( " + EXP1 + " )");
        assertTrue(evaluator.evaluate(rule, null));
    }
     
    /**
     * Test evaluation of rules that contain negation.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testNegatedPatterns() throws EvaluationException {
        // Test negation.
        rule.setDefinition("!" + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition("! " + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition("! " + EXP1);
        assertFalse(evaluator.evaluate(rule, null));

    }
    
    /**
     * Test evaluation of rules that contain OR.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testORPatterns() throws EvaluationException {
        // Test OR.
        rule.setDefinition(EXP1 + " |" + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "| " + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "|" + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP1 + "|" + EXP2);
        assertTrue(evaluator.evaluate(rule, null));
        rule.setDefinition(EXP3 + "|" + EXP4);
        assertFalse(evaluator.evaluate(rule, null));
    }
    
    /**
     * Test evaluation of rules composed of several expressions.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testCompoundRules() throws EvaluationException {
        // (E1 | E2) & E3 = false
        rule.setDefinition("(" + EXP1 + " | " + EXP2 + ") & " + EXP3);
        assertFalse(evaluator.evaluate(rule, null));
        // E1 | (E2 & E3) = true
        rule.setDefinition(EXP1 + " | (" + EXP2 + " & " + EXP3 + ")");
        assertTrue(evaluator.evaluate(rule, null));
        // (E1 | E2) & !E3 = true
        rule.setDefinition("(" + EXP1 + " | " + EXP2 + ") & !" + EXP3);
        assertTrue(evaluator.evaluate(rule, null));
        // E1 | !(E2 & E3) = true
        rule.setDefinition(EXP1 + " | !(" + EXP2 + " & " + EXP3 + ")");
        assertTrue(evaluator.evaluate(rule, null));
        // (E1 | E2) & (E3 | E4) = false
        rule.setDefinition("(" + EXP1 + " | " + EXP2 + ") & (" + EXP3 + " | " + EXP4 + ")");
        assertFalse(evaluator.evaluate(rule, null));
        // !(E1 & E2) | (E3 | E4) = false
        rule.setDefinition("!(" + EXP1 + " & " + EXP2 + ") | (" + EXP3 + " | " + EXP4 + ")");
        assertFalse(evaluator.evaluate(rule, null));
    }
    
    /**
     * Test evaluation with null rule.
     * 
     * @throws EvaluationException
     */
    @Test
    public void testOtherRules() throws EvaluationException {
        assertTrue(evaluator.evaluate(null,null));
        
        rule.setDefinition("something");
        try {
            evaluator.evaluate(rule,null);
            fail("Exception should occur");
        } catch (EvaluationException e) {
            assertEquals("Invalid rule definition: something.",e.getMessage());
        }
    }    
}
