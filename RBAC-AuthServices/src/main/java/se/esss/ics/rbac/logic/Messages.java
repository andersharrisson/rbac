/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <code>Messages</code> provides externalisation facilities for all strings related to RBAC services.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class Messages {

    private static final Logger LOGGER = LoggerFactory.getLogger(Messages.class);
    
    public static final String AUTHENTICATION_FAILED = "authenticationFailed";
    public static final String AUTHENTICATION_INCORRECT_USERNAME = "authenticationIncorrectUsername";
    public static final String AUTHORISED = "authorised";
    public static final String NOT_AUTHORISED = "notAuthorised";
    public static final String CLEANUP = "cleanup";
    public static final String EMPTY_EXPRESSION = "emptyExpression";
    public static final String EXCLUSIVE_ACCESS_RELEASE = "exclusiveAccessRelease";
    public static final String EXCLUSIVE_ACCESS_RELEASE_NOT_EXIST = "exclusiveAccessReleaseNotExist";
    public static final String EXCLUSIVE_ACCESS_REQUEST = "exclusiveAccessRequest";
    public static final String EXCLUSIVE_ACCESS_REQUEST_ALREADY_TAKEN = "exclusiveAccessRequestAlreadyTaken";
    public static final String EXCLUSIVE_ACCESS_REQUEST_NOT_ALLOWED = "exclusiveAccessRequestNotAllowed";
    public static final String EXCLUSIVE_ACCESS_REQUEST_NOT_PERMISSION_OWNER = 
            "exclusiveAccessRequestNotPermissionOwner";
    public static final String EXCLUSIVE_ACCESS_USER_ID_DONT_MATCH = "exclusiveAccessUserIdDontMatch";
    public static final String DS_LOGOUT_FAILED = "directoryServiceLogoutFailed";
    public static final String GROUP_NOT_FOUND = "groupNotFound";
    public static final String IP_ADDRESS_DONT_MATCH = "ipAddressDontMatch";
    public static final String INVALID_AUTHENTICATION = "invalidAuthentication";
    public static final String INVALID_EXPRESSION = "invalidExpression";
    public static final String INVALID_RULE = "invalidRule";
    public static final String LOGGED_OUT = "loggedOut";
    public static final String LOGGED_IN = "loggedIn";
    public static final String MISMATCHING_PV_VALUE_TYPES = "mismatchingPVValueTypes";
    public static final String MULTIPLE_ASG = "multipleASGs";
    public static final String MULTIPLE_TOKENS = "multipleTokens";
    public static final String MULTIPLE_PERMISSIONS = "multiplePermissions";
    public static final String NO_ASG = "noASG";
    public static final String NO_PERMISSION = "noPermission";
    public static final String NO_RESOURCE = "noResource";
    public static final String NO_ROLE = "noRole";
    public static final String NO_TOKEN = "noToken";
    public static final String PARAMETER_NOT_PROVIDED = "parameterNotProvided";
    public static final String PERMISSIONS_NOT_FOUND = "permissionsNotFound";
    public static final String PV_AUTO_CONNECT_ERROR = "pvAutoConnectError";
    public static final String PV_CONNECT_ERROR = "pvConnectError";
    public static final String PV_VALUE_ERROR = "pvValueError";
    public static final String ROLES_LOADED = "rolesLoaded";
    public static final String TOKEN_EXPIRED = "tokenExpired";
    public static final String TOKEN_RENEWED = "tokenRenewed";
    public static final String TOKEN_VALIDATED = "tokenValidated";
    public static final String TOKEN_IP_UNREACHABLE = "tokenIPUnreachable";
    public static final String TOO_MANY_ASGS = "tooManyASGs";
    public static final String TOO_LITTLE_ASGS = "tooLittleASGs";
    public static final String UNSUPPORTED_PV_VALUE_TYPE = "unsupportedPVValueType";
    public static final String USER_NOT_FOUND = "userNotFound";
    public static final String USER_INFO_RETRIEVAL_FAILED = "userInfoRetrievalFailed";

    private static final Properties MESSAGES = new Properties();
    static {
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("messages.properties")) {
            MESSAGES.load(stream);
        } catch (IOException e) {
            //should never happen
            LOGGER.error("Error reading the message.properties",e);
        }
    }

    /**
     * Returns the text which belongs to the given key. The text is then formatted with the given attributes. Each tag
     * {n} is replaced by the attribute under the specified index.
     * 
     * @param key the key
     * @param attributes the attributes
     * @return the text (if not defined, the bracketed key is returned)
     */
    public static String getString(String key, Object... attributes) {
        String text = getString(key);
        try {
            return MessageFormat.format(text, attributes);
        } catch (IllegalArgumentException e) {
            return '<' + key + '>';
        }
    }

    /**
     * Returns the text which belongs to the given key.
     * 
     * @param key the key
     * @return the text loaded from the properties file
     */
    public static String getString(String key) {
        String s = MESSAGES.getProperty(key);
        return (s == null) ? '<' + key + '>' : s;
    }

    /**
     * Constructor.
     */
    private Messages() {
    }
}
