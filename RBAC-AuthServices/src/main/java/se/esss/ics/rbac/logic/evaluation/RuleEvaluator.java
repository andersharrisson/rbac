/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.datamodel.Token;

/**
 * {@link RuleEvaluator} is a singleton bean used for evaluating permission rules.
 * 
 * Rule is evaluated by evaluating expressions in its definition and then recursively evaluating the remaining
 * mathematical expression of logical values.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Singleton(description = "This bean evaluates a single rule to true or false based on its definition and expressions.")
public class RuleEvaluator implements Serializable {

    private static final long serialVersionUID = -6394177593186491500L;

    private static final Pattern CAPTURE_EXPRESSION = Pattern.compile("\\b([\\d\\w]+)\\b");
    private static final Pattern CAPTURE_AND = Pattern.compile("([\\w]+)&([\\w]+)");
    private static final Pattern CAPTURE_BRACKETS = Pattern.compile("(\\([\\w]+\\))");
    private static final Pattern CAPTURE_NEGATION = Pattern.compile("(![\\w]+)");
    private static final Pattern CAPTURE_OR = Pattern.compile("([\\w]+)\\|([\\w]+)");
    private static final Pattern SPACES = Pattern.compile("\\s+");
    private static final String EMPTY_STRING = "";

    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String NOT_FALSE = "!false";
    private static final String BRACKET_TRUE = "(true)";

    @EJB
    private ExpressionEvaluator expressionEvaluator;
    

    /**
     * Evaluates the rule governing the provided permission and returns the resulting logical value.
     * 
     * If the rule is not defined, the evaluator only checks if the token owner has the provided permission. If the rule
     * is defined, the expression names in rule definition are replaced by their logical evaluation values and then the
     * remaining logical expression is evaluated with accordance to mathematical rules of operation precedence. Result
     * of the logical expression evaluation is returned. Values of some expressions and rule definition parts may depend
     * on the caller's roles or permissions, therefore the token has to be provided.
     * 
     * @param rule the rule to evaluate
     * @param token associated with the evaluation call
     * 
     * @return logical value that is the result of permission's rule evaluation
     * 
     * @throws EvaluationException if there is an error while evaluating expressions or if the permission's rule
     *             contains invalid definition
     */
    public boolean evaluate(Rule rule, Token token) throws EvaluationException {
        if (rule == null) {
            //if there is no rule we evaluate to true for everybody
            return true;
        }
        String booleanDefinition = determineDefinitionValues(rule, token);
        return evaluateBooleanDefinition(SPACES.matcher(booleanDefinition).replaceAll(EMPTY_STRING));
    }

    private String determineDefinitionValues(Rule rule, Token token) throws EvaluationException {
        String definition = rule.getDefinition();
        Matcher matcher = CAPTURE_EXPRESSION.matcher(definition);
        Map<String, Boolean> expressionValues = new HashMap<>();
        while (matcher.find()) {
            String expressionName = matcher.group().trim();
            if (!expressionValues.containsKey(expressionName)) {
                boolean value = false;
                Expression expression = getExpression(expressionName, rule);
                if (expression == null) {
                    if (TRUE.equalsIgnoreCase(expressionName) || FALSE.equalsIgnoreCase(expressionName)) {
                        value = Boolean.parseBoolean(expressionName);
                    } else {
                        throw new EvaluationException(Messages.getString(Messages.INVALID_RULE,definition));
                    }
                } else {
                    value = expressionEvaluator.evaluate(expression, token);
                }
                expressionValues.put(expressionName, Boolean.valueOf(value));
            }
        }
        for (Map.Entry<String, Boolean> entry : expressionValues.entrySet()) {
            definition = definition.replaceAll(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return definition;
    }

    private boolean evaluateBooleanDefinition(String booleanDefinition) throws EvaluationException {
        if (TRUE.equalsIgnoreCase(booleanDefinition) || FALSE.equalsIgnoreCase(booleanDefinition)) {
            return Boolean.parseBoolean(booleanDefinition);
        }

        Matcher matcher = CAPTURE_NEGATION.matcher(booleanDefinition);
        if (matcher.find()) {
            return evaluateBooleanDefinition(matcher.replaceFirst(String.valueOf(NOT_FALSE.equalsIgnoreCase(matcher
                    .group()))));
        }
        matcher = CAPTURE_BRACKETS.matcher(booleanDefinition);
        if (matcher.find()) {
            return evaluateBooleanDefinition(matcher.replaceFirst(String.valueOf(BRACKET_TRUE.equalsIgnoreCase(matcher
                    .group()))));
        }
        matcher = CAPTURE_AND.matcher(booleanDefinition);
        if (matcher.find()) {
            return evaluateBooleanDefinition(matcher.replaceFirst(String.valueOf(evaluateAnd(matcher.group(1),
                    matcher.group(2)))));
        }
        matcher = CAPTURE_OR.matcher(booleanDefinition);
        if (matcher.find()) {
            return evaluateBooleanDefinition(matcher.replaceFirst(String.valueOf(evaluateOr(matcher.group(1),
                    matcher.group(2)))));
        }
        throw new EvaluationException(Messages.getString(Messages.INVALID_RULE, booleanDefinition));
    }

    private static boolean evaluateAnd(String boolean1, String boolean2) {
        return Boolean.parseBoolean(boolean1) && Boolean.parseBoolean(boolean2);
    }

    private static boolean evaluateOr(String boolean1, String boolean2) {
        return Boolean.parseBoolean(boolean1) || Boolean.parseBoolean(boolean2);
    }

    /**
     * Find the given expression name in the list of expressions in the rule. If found, the expression is
     * returned, otherwise null is returned.
     * 
     * @param expressionName the name of the expression
     * @param rule the rule to search
     * @return the expression if found or null otherwise
     */
    private static Expression getExpression(String expressionName, Rule rule) {
        for (Expression expression : rule.getExpression()) {
            if (expression.getName().equals(expressionName)) {
                return expression;
            }
        }
        return null;
    }
}
