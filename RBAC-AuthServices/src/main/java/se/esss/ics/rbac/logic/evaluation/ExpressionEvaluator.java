/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.csstudio.data.values.IDoubleValue;
import org.csstudio.data.values.IEnumeratedValue;
import org.csstudio.data.values.ILongValue;
import org.csstudio.data.values.IStringValue;
import org.csstudio.data.values.IValue;
import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;

import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Token;

/**
 * {@link ExpressionEvaluator} is a singleton bean used for evaluation of rule expressions.
 *
 * Expressions are evaluated by replacing elements in their definitions with their current values
 * and then evaluating the comparison statement. Values are obtained from the RBAC database or from
 * PV values.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Singleton(description = "The bean evaluates a single expression to true or false based on its definition.")
public class ExpressionEvaluator implements Serializable {

    private static final long serialVersionUID = -2325502752897016043L;

    private static final Pattern PV_EQUAL = Pattern.compile("^PV\\s+[\\w-:]+\\s+=\\s+[\\w-:]+$");
    private static final Pattern PV_EQUAL_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+=\\s+[\"].+[\"]$");
    private static final Pattern PV_NOT_EQUAL = Pattern.compile("^PV\\s+[\\w-:]+\\s+!=\\s+[\\w-:]+$");
    private static final Pattern PV_NOT_EQUAL_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+!=\\s+[\"].+[\"]$");
    private static final Pattern PV_GREATER = Pattern.compile("^PV\\s+[\\w-:]+\\s+>\\s+[\\w-:]+$");
    private static final Pattern PV_GREATER_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+>\\s+[\"].+[\"]$");
    private static final Pattern PV_GREATER_OR_EQUAL = Pattern.compile("^PV\\s+[\\w-:]+\\s+>=\\s+[\\w-:]+$");
    private static final Pattern PV_GREATER_OR_EQUAL_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+>=\\s+[\"].+[\"]$");
    private static final Pattern PV_LESS = Pattern.compile("^PV\\s+[\\w-:]+\\s+<\\s+[\\w-:]+$");
    private static final Pattern PV_LESS_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+<\\s+[\"].+[\"]$");
    private static final Pattern PV_LESS_OR_EQUAL = Pattern.compile("^PV\\s+[\\w-:]+\\s+<=\\s+[\\w-:]+$");
    private static final Pattern PV_LESS_OR_EQUAL_Q = Pattern.compile("^PV\\s+[\\w-:]+\\s+<=\\s+[\"].+[\"]$");
    private static final Pattern IP_EQUAL = Pattern.compile("^IP\\s+=\\s+[a-zA-Z0-9-\\.]+$");
    private static final Pattern IP_NOT_EQUAL = Pattern.compile("^IP\\s+!=\\s+[a-zA-Z0-9-\\.]+$");
    private static final Pattern IP_GROUP_CONTAIN = Pattern.compile("^IP\\s+@\\s+[\\w\\s-:]+$");
    private static final Pattern IP_GROUP_NOT_CONTAIN = Pattern.compile("^IP\\s+!@\\s+[\\w\\s-:]+$");
    private static final Pattern IP = Pattern.compile("[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}");

    private static final Pattern SPACES = Pattern.compile("\\s+");

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private CachedPVProvider pvSource;

    /**
     * Evaluates the provided expression and returns the resulting logical value.
     *
     * Type of the expression is first identified by its definition's prefix and comparison
     * operator. If it is recognised, values for both parts of the comparison statement are then
     * retrieved and compared accordingly. Result of the comparison is then returned as the
     * expression's evaluation value. Values of some expression parts may be dependent on the
     * caller's roles or permissions, that's why the caller's token also has to be provided.
     *
     * @param expression to be evaluated
     * @param token associated with the evaluation call
     *
     * @return logical value that is the result of expression evaluation
     *
     * @throws EvaluationException if there is an error while fetching a value or if the expression
     *             contains an unrecognised definition pattern.
     */
    public boolean evaluate(Expression expression, Token token) throws EvaluationException {
        String definition = expression.getDefinition();
        if (definition == null || definition.isEmpty()) {
            throw new EvaluationException(Messages.getString(Messages.EMPTY_EXPRESSION));
        }
        String[] parts = SPACES.split(definition);
        try {
            if (PV_EQUAL.matcher(definition).matches()
                    || PV_EQUAL_Q.matcher(definition).matches()) {
                return pvEqual(parts[1],parts[3]);
            } else if (PV_NOT_EQUAL.matcher(definition).matches()
                    || PV_NOT_EQUAL_Q.matcher(definition).matches()) {
                return pvNotEqual(parts[1],parts[3]);
            } else if (PV_GREATER.matcher(definition).matches()
                    || PV_GREATER_Q.matcher(definition).matches()) {
                return pvGreater(parts[1],parts[3]);
            } else if (PV_GREATER_OR_EQUAL.matcher(definition).matches()
                    || PV_GREATER_OR_EQUAL_Q.matcher(definition).matches()) {
                return pvGreaterOrEqual(parts[1],parts[3]);
            } else if (PV_LESS.matcher(definition).matches()
                    || PV_LESS_Q.matcher(definition).matches()) {
                return pvLess(parts[1],parts[3]);
            } else if (PV_LESS_OR_EQUAL.matcher(definition).matches()
                    || PV_LESS_OR_EQUAL_Q.matcher(definition).matches()) {
                return pvLessOrEqual(parts[1],parts[3]);
            } else if (IP_EQUAL.matcher(definition).matches()) {
                return isIPEqualToTokens(parts[2], token);
            } else if (IP_NOT_EQUAL.matcher(definition).matches()) {
                return !isIPEqualToTokens(parts[2], token);
            } else if (IP_GROUP_CONTAIN.matcher(definition).matches()) {
                IPGroup ipGroup = entityManager.createNamedQuery("IPGroup.findByName", IPGroup.class)
                        .setParameter("name", parts[2]).getSingleResult();
                return isIPInGroup(ipGroup, token);
            } else if (IP_GROUP_NOT_CONTAIN.matcher(definition).matches()) {
                IPGroup ipGroup = entityManager.createNamedQuery("IPGroup.findByName", IPGroup.class)
                        .setParameter("name", parts[2]).getSingleResult();
                return !isIPInGroup(ipGroup, token);
            }
        } catch (PVException e) {
            throw new EvaluationException(Messages.getString(Messages.PV_CONNECT_ERROR, e.getPVName()),e);
        } catch (NoResultException e) {
            throw new EvaluationException(Messages.getString(Messages.GROUP_NOT_FOUND, parts[2]));
        }
        throw new EvaluationException(Messages.getString(Messages.INVALID_EXPRESSION, definition));
    }

    private boolean pvEqual(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        //if the right side is quoted it is most likely a string.
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return pvValueEqualsValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return pvValueEqualsValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return pvValueEqualsPV(pv1, pv2);
        }
    }

    private boolean pvNotEqual(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return !pvValueEqualsValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return !pvValueEqualsValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return !pvValueEqualsPV(pv1, pv2);
        }
    }

    private boolean pvGreater(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return pvValueGreaterThanValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return pvValueGreaterThanValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return pvValueGreaterThanPV(pv1, pv2);
        }
    }

    private boolean pvGreaterOrEqual(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return !pvValueLessThanValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return !pvValueLessThanValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return !pvValueLessThanPV(pv1, pv2);
        }
    }

    private boolean pvLess(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return pvValueLessThanValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return pvValueLessThanValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return pvValueLessThanPV(pv1, pv2);
        }
    }

    private boolean pvLessOrEqual(String leftSide, String rightSide) throws PVException, EvaluationException {
        PV pv1 = pvSource.getPV(leftSide);
        if (rightSide.charAt(0) == '"' && rightSide.charAt(rightSide.length()-1) == '"') {
            return !pvValueGreaterThanValue(pv1,rightSide.substring(1,rightSide.length()-1));
        }
        try {
            Double.parseDouble(rightSide);
            return !pvValueGreaterThanValue(pv1, rightSide);
        } catch (NumberFormatException nfe) {
            PV pv2 = pvSource.getPV(rightSide);
            return !pvValueGreaterThanPV(pv1, pv2);
        }
    }

    /**
     * Compares the value of the given PV to the string value. If the PV is numeric, the
     * string value will be transformed into a number before comparing.
     *
     * @param pv the PV compare
     * @param value the value to compare the PV value to
     * @return true if the value matches
     *
     * @throws EvaluationException if the type of PV is not supported
     * @throws NumberFormatException if the string value could not be transformed into a numeric type
     */
    private static boolean pvValueEqualsValue(PV pv, String value) throws EvaluationException {
        return comparePVvalueToValue(pv, value) == 0;
    }

    /**
     * Compares the value of the PV to the string value and returns true if the PV value
     * is greater than the string value or false otherwise. If the PV is numeric, the string
     * value will be first transformed to a number and than compared.
     *
     * @param pv the PV to compare
     * @param value the value to compare to
     * @return true if the PV value is greater than value
     *
     * @throws EvaluationException if the PV type is not supported
     * @throws NumberFormatException if the string value could not be transformed to a
     *          number.
     */
    private static boolean pvValueGreaterThanValue(PV pv, String value) throws EvaluationException {
        return comparePVvalueToValue(pv, value) > 0;
    }

    /**
    * Compares the value of the PV to the string value and returns true if the PV value
    * is less than the string value or false otherwise. If the PV is numeric, the string
    * value will be first transformed to a number and than compared.
    *
    * @param pv the PV to compare
    * @param value the value to compare to
    * @return true if the PV value is less than value
    * @throws EvaluationException if the PV type is not supported
    * @throws NumberFormatException if the string value could not be transformed to a
    *          number.
    */
    private static boolean pvValueLessThanValue(PV pv, String value) throws EvaluationException {
        return comparePVvalueToValue(pv, value) < 0;
    }

    /**
     * Compares the PV value to the given string value and return a positive integer if the PV value
     * is greater than the string value, 0 if it is equals or negative integer if the PV value is less than
     * the string value. String PV values are compared lexicographically.
     *
     * @param pv the PV, which value to compare
     * @param value the value which the PV value is compared against
     * @return less than, 0, or more than zero if the PV value is less than, equal or greater than the string value
     * @throws EvaluationException if the PV value type is not supported
     */
    private static int comparePVvalueToValue(PV pv, String value) throws EvaluationException {
        IValue pvValue = pv.getValue();
        if (pvValue == null) {
            throw new EvaluationException(Messages.getString(Messages.PV_VALUE_ERROR, pv.getName()));
        }
        double d = Double.NaN;
        if (pvValue instanceof IDoubleValue) {
            d = ((IDoubleValue) pvValue).getValue() - Double.parseDouble(value);
        } else if (pvValue instanceof ILongValue) {
            d = ((ILongValue) pvValue).getValue() - Long.parseLong(value);
        } else if (pvValue instanceof IEnumeratedValue) {
            try {
                return ((IEnumeratedValue) pvValue).getValue() - Integer.parseInt(value);
            } catch (NumberFormatException e) {
                return ((IEnumeratedValue) pvValue).getMetaData()
                        .getState(((IEnumeratedValue) pvValue).getValue()).compareTo(value);
            }
        } else if (pvValue instanceof IStringValue) {
            return ((IStringValue) pvValue).getValue().compareTo(value);
        }
        if (!Double.isNaN(d)) {
            return Double.compare(d,0);
        }
        throw new EvaluationException(Messages.getString(Messages.UNSUPPORTED_PV_VALUE_TYPE,
                pvValue.getClass().getName()));
    }

    /**
     * Compares values of two different PVs and returns true if the values are equal or false
     * if values are different. If the values of both PVs are numeric they will be compared
     * as numbers. If one of them is not numeric, the values need to be of the same type
     * (enumeration or string).
     *
     * @param pv1 the first PV
     * @param pv2 the second PV
     * @return true if values are equal or false otherwise
     * @throws EvaluationException if the value types do not match
     */
    private static boolean pvValueEqualsPV(PV pv1, PV pv2) throws EvaluationException {
        return comparePVValueToPV(pv1, pv2) == 0;
    }

    /**
     * Compares values of two different PVs and returns true if the value of first PV
     * is greater than value of the second PV or false otherwise. If the values of both PVs
     * are numeric they will be compared as numbers. If one of them is not numeric, the values
     * need to be of the same type (enumeration or string).
     *
     * @param pv1 the first PV
     * @param pv2 the second PV
     * @return true if the value of the first PV is greater than value of the second PV
     * @throws EvaluationException if the value types do not match
     */
    private static boolean pvValueGreaterThanPV(PV pv1, PV pv2) throws EvaluationException {
        return comparePVValueToPV(pv1, pv2) > 0;
    }

    /**
     * Compares values of two different PVs and returns true if the value of first PV
     * is less than value of the second PV or false otherwise. If the values of both PVs
     * are numeric they will be compared as numbers. If one of them is not numeric, the values
     * need to be of the same type (enumeration or string).
     *
     * @param pv1 the first PV
     * @param pv2 the second PV
     * @return true if the value of the first PV is less than value of the second PV
     * @throws EvaluationException if the value types do not match
     */
    private static boolean pvValueLessThanPV(PV pv1, PV pv2) throws EvaluationException {
        return comparePVValueToPV(pv1, pv2) < 0;
    }

    private static int comparePVValueToPV(PV pv1, PV pv2) throws EvaluationException {
        IValue pv1Value = pv1.getValue();
        IValue pv2Value = pv2.getValue();
        if (pv1Value == null) {
            throw new EvaluationException(Messages.getString(Messages.PV_VALUE_ERROR, pv1.getName()));
        } else if (pv2Value == null) {
            throw new EvaluationException(Messages.getString(Messages.PV_VALUE_ERROR, pv2.getName()));
        }
        double d = Double.NaN;
        if (pv1Value instanceof IDoubleValue) {
            if (pv2Value instanceof IDoubleValue) {
                d = ((IDoubleValue) pv1Value).getValue() - ((IDoubleValue) pv2Value).getValue();
            } else if (pv2Value instanceof ILongValue) {
                d = ((IDoubleValue) pv1Value).getValue() - ((ILongValue) pv2Value).getValue();
            }
        } else if (pv1Value instanceof ILongValue) {
            if (pv2Value instanceof IDoubleValue) {
                d = ((ILongValue) pv1Value).getValue() - ((IDoubleValue) pv2Value).getValue();
            } else if (pv2Value instanceof ILongValue) {
                d = ((ILongValue) pv1Value).getValue() - ((ILongValue) pv2Value).getValue();
            }
        }
        if (!Double.isNaN(d)) {
            return Double.compare(d,0);
        }

        if (pv1Value.getClass() != pv2Value.getClass()) {
            throw new EvaluationException(Messages.getString(Messages.MISMATCHING_PV_VALUE_TYPES,
                                 pv1Value.getClass().getName(), pv2Value.getClass().getName()));
        }
        if (pv1Value instanceof IEnumeratedValue) {
            return ((IEnumeratedValue) pv1Value).getValue() - ((IEnumeratedValue) pv2Value).getValue();
        } else if (pv1Value instanceof IStringValue) {
            return ((IStringValue) pv1Value).getValue().compareTo(((IStringValue) pv2Value).getValue());
        }
        throw new EvaluationException(Messages.getString(Messages.UNSUPPORTED_PV_VALUE_TYPE,
                pv1Value.getClass().getName()));

    }

    /**
     * Checks if the IP is the same as the IP in the token.
     *
     * @param ip the IP to check against token
     * @param token the token to check its IP
     * @return true if the IPs match or false otherwise
     * @throws EvaluationException if the IP is unavailable
     */
    private static boolean isIPEqualToTokens(String ip, Token token) throws EvaluationException {
        try {
            //TODO what if the caller is outside reach (e.g. through another webapp, which is public)
            if (IP.matcher(token.getIp()).matches()) {
                if (IP.matcher(ip).matches()) {
                    return ip.equals(token.getIp());
                } else {
                    return ip.equals(InetAddress.getByName(token.getIp()).getHostName());
                }
            } else {
                if (IP.matcher(ip).matches()) {
                    return ip.equals(InetAddress.getByName(token.getIp()).getHostAddress());
                } else {
                    return ip.equals(token.getIp());
                }
            }
        } catch (UnknownHostException e) {
            throw new EvaluationException(Messages.getString(Messages.TOKEN_IP_UNREACHABLE, token.getIp(),
                    e.getMessage()), e);
        }
    }

    /**
     * Checks if the IP from the token is in the given IP group. The method checks the
     * host address and host name. If any of the two is found, true is returned, otherwise
     * false is returned.
     *
     * @param ipGroup the group to check the IP against
     * @param token the token that provides the IP
     * @return true if the IP is in the group or false otherwise
     * @throws EvaluationException if the IP is unavailable
     */
    private static boolean isIPInGroup(IPGroup ipGroup, Token token) throws EvaluationException {
        try {
            //TODO what if the caller is outside reach (e.g. through another webapp, which is public)
            if (IP.matcher(token.getIp()).matches()) {
                //the ip in the token is generally always an IP, so this should be sufficient
                if (ipGroup.getIp().contains(token.getIp())) {
                    return true;
                }
                //iterate over all ips in the group. if they are all IPs then don't try to resolve the token ip
                boolean nonIP = false;
                for (String ip : ipGroup.getIp()) {
                    if (!IP.matcher(ip).matches()) {
                        nonIP = true;
                        break;
                    }
                }
                if (nonIP) {
                    return false;
                }
                //token ip should always be visible so this should resolve fast
                return ipGroup.getIp().contains(InetAddress.getByName(token.getIp()).getHostName());
            } else {
                //but just in case if someone is providing the hostname
                if (ipGroup.getIp().contains(token.getIp())) {
                    return true;
                }
                //iterate over all ips in the group. if they are all hostnames then don't try to resolve the token ip
                boolean nonHostname = false;
                for (String ip : ipGroup.getIp()) {
                    if (IP.matcher(ip).matches()) {
                        nonHostname = true;
                        break;
                    }
                }
                if (nonHostname) {
                    return false;
                }
                //token ip should always be visible so this should resolve fast
                return ipGroup.getIp().contains(InetAddress.getByName(token.getIp()).getHostAddress());
            }
        } catch (UnknownHostException e) {
            throw new EvaluationException(Messages.getString(Messages.TOKEN_IP_UNREACHABLE, token.getIp(),
                    e.getMessage()), e);
        }
    }
}
