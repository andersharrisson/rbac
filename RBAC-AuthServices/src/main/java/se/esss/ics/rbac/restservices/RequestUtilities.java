/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.restservices;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import se.esss.ics.rbac.jaxb.util.TokenUtil;
import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;

/**
 * <code>RequestUtilities</code> is a utility class for extracting specific information from a
 * {@link HttpServletRequest}.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public final class RequestUtilities {

    // it has the be spelled with z, to follow the standards, which are American
    /** The name of the authorisation header */
    public static final String HEADER_NAME_AUTHORISATION = "Authorization"; 
    /** The name of the exclusive access duration header */
    public static final String HEADER_NAME_EXCLUSIVE_DURATION = "RBACDuration";
    /** The name of the IP address header */
    public static final String HEADER_NAME_IP = "RBACAddress";
    /** The name of the roles header */
    public static final String HEADER_NAME_ROLES = "RBACRoles";
    
    private static final String CHAR_ENCODING = "UTF-8";
    private static final Charset CHARSET = Charset.forName(CHAR_ENCODING);

    private RequestUtilities() {
    }

    /**
     * Retrieves login information from the provided request. Returned information is stored into an array, where first
     * element is the username and the second one is password. The method only returns login information for basic
     * authentication method. If the authentication method is not recognised or if the header is <code>null</code>, an
     * exception is thrown.
     * 
     * @param request request containing basic authentication information
     * 
     * @return char matrix of length 2, where first array contains the username and the second one password
     * 
     * @throws AuthAndAuthException if expected HTTP header {@value #HEADER_NAME_AUTHORISATION} is missing
     */
    public static char[][] getLoginData(HttpServletRequest request) throws AuthAndAuthException {
        String authHeader = request.getHeader(HEADER_NAME_AUTHORISATION);

        if (authHeader == null) {
            throw new AuthAndAuthException(null, null, Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Authorisation header"));
        } else if (!authHeader.startsWith(HttpServletRequest.BASIC_AUTH)) {
            throw new AuthAndAuthException(null, null, Messages.getString(Messages.INVALID_AUTHENTICATION,
                    HttpServletRequest.BASIC_AUTH, authHeader));
        }
       
        try {
            // Authorisation data is base64 encoded username:password string.
            String authDataEncoded = authHeader.substring(HttpServletRequest.BASIC_AUTH.length() + 1);
            byte[] authDataBytes = DatatypeConverter.parseBase64Binary(authDataEncoded);
            
            CharBuffer buffer = CHARSET.newDecoder().decode(ByteBuffer.wrap(authDataBytes));
            char[] data = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
        
            for (int i = 0; i < data.length; i++) {
                if (data[i] == ':') {
                    char[] usernameChars = new char[i];
                    for (int j = 0; j < usernameChars.length; j++) {
                        usernameChars[j] = (char) data[j];
                    }
    
                    char[] passwordChars = new char[data.length - i - 1];
                    for (int j = 0; j < passwordChars.length; j++) {
                        passwordChars[j] = data[j+i+1];
                    }
    
                    return new char[][] { usernameChars, passwordChars };
                }
            }
            return new char[2][0];
        } catch (CharacterCodingException e) {
            throw new AuthAndAuthException(null, null, Messages.getString(Messages.INVALID_AUTHENTICATION,
                    HttpServletRequest.BASIC_AUTH, authHeader));
        }
    }

    /**
     * Retrieves user's IP info from the provided request. User's IP should be stored in a header named
     * {@value #HEADER_NAME_IP}. If this header does not exist, remote address of the request is used instead.
     * 
     * @param request containing user's IP information.
     * 
     * @return user's IP address.
     */
    public static String getIP(HttpServletRequest request) {
        String ipHeader = request.getHeader(HEADER_NAME_IP);
        if (ipHeader != null) {
            return ipHeader;
        }
        return request.getRemoteAddr();
    }

    /**
     * Retrieves user's preferred roles from the provided request. User's roles should be stored in a header named
     * {@value #HEADER_NAME_ROLES} and delimited by <code>','</code> character. if the header does not exist, an empty
     * array of zero length is returned.
     * 
     * @param request containing user's preferred role information.
     * @return array of user's preferred role names.
     */
    public static String[] getRoleNames(HttpServletRequest request) {
        String rolesHeader = request.getHeader(HEADER_NAME_ROLES);
        if (rolesHeader != null) {
            return rolesHeader.split(String.valueOf(TokenUtil.DELIMITER_ROLES));
        }
        return new String[0];
    }

    /**
     * Retrieves exclusive access duration from the provided request. Duration period of exclusive access for permission
     * should be stored in a header named {@value #HEADER_NAME_EXCLUSIVE_DURATION}. If the header value does not exist,
     * 0 is returned. In case that a value less than 60000 is provided, 0 is returned.
     * 
     * @param request containing exclusive access duration in seconds.
     * 
     * @return exclusive access duration in milliseconds or 0 if the duration could not be extracted or an invalid
     *         parameter is provided
     * 
     */
    public static long getExclusiveDuration(HttpServletRequest request) {
        String durationHeader = request.getHeader(HEADER_NAME_EXCLUSIVE_DURATION);
        if (durationHeader != null) {
            long duration = Long.parseLong(durationHeader);
            return duration < 60000 ? 0 : duration;
        }
        return 0;
    }
}
