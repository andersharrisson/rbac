/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.timer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * 
 * <code>TimerBeanHandler</code> is a singleton that takes care that the cleanup timer is started when the service is
 * running.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Singleton(description = "The bean provides a mechanism to start the cleanup timer.")
@Startup
@DependsOn("DatabaseMigration")
public class TimerBeanHandler implements Serializable {

    private static final long serialVersionUID = 6029329711346998258L;

    @EJB
    private TimerBeanRemote timerBean;

    /**
     * Start this timer bean after constructions, which starts the injected timer.
     */
    @PostConstruct
    public void startUp() {
        timerBean.startTimer();
    }
}
