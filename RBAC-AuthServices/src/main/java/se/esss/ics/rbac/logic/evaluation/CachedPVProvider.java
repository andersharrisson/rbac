/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.evaluation;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;
import org.csstudio.utility.pv.PVFactory;
import org.csstudio.utility.pv.PVFactoryProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.logic.Messages;

/**
 * {@link CachedPVProvider} is a singleton that provides PVs by PV name. PVs retrieved through
 * <code>CachedPVProvider</code> are cached and the connection is kept open, so that there is no need to reconnect them
 * on subsequent uses.
 *
 * <code>CachedPVProvider</code> also offers capability of automatically connecting and caching PVs on startup. PVs that
 * should take advantage of this must be declared in {@value #FILE_AUTOCONNECT} file, which must in turn be located
 * somewhere in the classpath.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Singleton(description = "The bean is responsible for the lifecycle of the EPICS PVs endpoints.")
@Startup
@DependsOn("DatabaseMigration")
public class CachedPVProvider implements Serializable {

    private static final long serialVersionUID = 8868438663422479907L;

    @XmlRootElement(name = "autoconnect")
    private static class AutoconnectPVs {
        /** @return the list of PV names that RBAC should automatically connect to */
        @XmlElement(name = "pv")
        @XmlElementWrapper(name = "pvs")
        public List<String> pvNames;
    }

    /** The name of the file that defines the PVs that RBAC automatically connects to at startup */
    public static final String FILE_AUTOCONNECT = "autoconnect.xml";

    private static final transient Logger LOGGER = LoggerFactory.getLogger(CachedPVProvider.class);

    private final transient PVFactory pvFactory = PVFactoryProvider.getInstance().getDefaultFactory();
    // Map is not serializable, but HashMap is
    private final Map<String, PV> pvMap = new HashMap<>();

    @PostConstruct
    private void loadAutoconnectPVs() {
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FILE_AUTOCONNECT)) {
            AutoconnectPVs pvs = JAXB.unmarshal(stream, AutoconnectPVs.class);
            for (String pvName : pvs.pvNames) {
                getPV(pvName, false);
            }
        } catch (Exception e) {
            LOGGER.error(Messages.getString(Messages.PV_AUTO_CONNECT_ERROR), e);
        }
    }

    @PreDestroy
    private void disconnectAllPVs() {
        for (PV pv : pvMap.values()) {
            pv.stop();
        }
    }

    /**
     * Returns a PV connection by the specified name from cache or creates a new one, if on does not already exist.
     * Returned PV is already connected.
     *
     * @see PV#blockUntilFirstUpdate(int)
     * @param pvName name of the PV
     * @param waitForValue if true the calling thread will be blocked, until the PV receives the first value, but no
     *            longer than 2 seconds
     * @return PV by the specified name
     *
     * @throws PVException if there is a problem connecting the PV or if the parameter is null
     */
    public PV getPV(String pvName, boolean waitForValue) throws PVException {
        if (pvName == null) {
            throw new PVException("Unknown", Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "PV name"));
        }
        PV pv = null;
        synchronized (pvMap) {
            pv = pvMap.get(pvName);
            if (pv == null) {
                pv = pvFactory.createPV(pvName);
                if (pv == null) {
                    throw new PVException(pvName, Messages.getString(Messages.PV_CONNECT_ERROR,pvName));
                }
                pv.start();
                pvMap.put(pvName, pv);
            }
        }
        if (waitForValue) {
            pv.blockUntilFirstUpdate(2000);
        }
        return pv;
    }

    /**
     * Returns a PV connection by the specified name from cache or creates a new one, if on does not already exist.
     * Returned PV is already connected.
     *
     * @param pvName name of the PV
     *
     * @return PV by the specified name
     *
     * @throws PVException if there is a problem connecting the PV or parameter is null
     */
    public PV getPV(String pvName) throws PVException {
        return getPV(pvName, true);
    }
}
