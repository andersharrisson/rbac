/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.security.cert.X509Certificate;

import javax.swing.JPanel;

import org.junit.Test;

import se.esss.ics.rbac.access.swing.SecurityFacade;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.test.SecurityFacadeAutoLogoutTest;

/**
 * 
 * <code>SwingSecurityFacadeAutoLogoutTest</code> test the auto logout functionality using the swing security facade.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SwingSecurityFacadeAutoLogoutTest extends SecurityFacadeAutoLogoutTest {

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.test.AbstractSecurityFacadeTest#createFacade()
     */
    @Override
    protected ISecurityFacade createFacade() {
        ISecurityFacade facade = new SecurityFacade(super.createFacade());
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public boolean acceptCertificate(String host, X509Certificate certificate) {
                return true;
            }
        });
        return facade;
    }

    /**
     * Forcefully set the auto logout features, which cannot be set in a standard way. This is necessary to make the
     * test shorter, so one doesn't need to wait for long timeouts.
     * 
     * @throws Exception on error
     */
    @Override
    protected void setUpAutoLogout() throws Exception {
        // force autoLogout of 1000 milliseconds to avoid excessive waiting during this test
        Field field = se.esss.ics.rbac.access.SecurityFacade.class.getDeclaredField("autoLogout");
        field.setAccessible(true);
        Field delegate = SecurityFacade.class.getDeclaredField("delegate");
        delegate.setAccessible(true);
        AutoLogout logout = (AutoLogout) field.get(delegate.get(facade));
        Field f = AutoLogout.class.getDeclaredField("logoutTimeout");
        f.setAccessible(true);
        f.set(logout, Long.valueOf(1000));
    }

    private static void createAndDispatchMouseEvent(int x, int y, int button, int type) {
        MouseEvent e = new MouseEvent(new JPanel(), type, System.currentTimeMillis(), 0, x, y, x, y, 0, false, button);
        AWTEventListener[] listeners = Toolkit.getDefaultToolkit().getAWTEventListeners();
        for (AWTEventListener l : listeners) {
            l.eventDispatched(e);
        }
    }

    private static void createAndDispatchKeyEvent(int keyType) {
        KeyEvent e = new KeyEvent(new JPanel(), keyType, System.currentTimeMillis(), 0, KeyEvent.VK_F, 'F');
        AWTEventListener[] listeners = Toolkit.getDefaultToolkit().getAWTEventListeners();
        for (AWTEventListener l : listeners) {
            l.eventDispatched(e);
        }
    }

    /**
     * Test if auto logout is cancelled when mouse is moving.
     * 
     * @throws Exception on error
     */
    @Test
    public void testWithMouseMovement() throws Exception {
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            createAndDispatchMouseEvent(100, 100, 0, MouseEvent.MOUSE_MOVED);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_PRESSED);
            cb.wait(250);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_RELEASED);
            for (int i = 0; i < 15; i++) {
                createAndDispatchMouseEvent(100 + i * 5, 100 + i * 5, 0, MouseEvent.MOUSE_MOVED);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when mouse buttons are pressed.
     * 
     * @throws Exception on error
     */
    @Test
    public void testWithMouseClick() throws Exception {
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            createAndDispatchMouseEvent(100, 100, 0, MouseEvent.MOUSE_MOVED);
            for (int i = 0; i < 15; i++) {
                createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_PRESSED);
                // add time between press and release, so the system recognizes that as a click
                cb.wait(250);
                createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_RELEASED);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when mouse wheel is moving.
     * 
     * @throws Exception on error
     */
    @Test
    public void testWithMouseWheelMove() throws Exception {
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            createAndDispatchMouseEvent(100, 100, 0, MouseEvent.MOUSE_MOVED);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_PRESSED);
            // add time between press and release, so the system recognizes that as a click
            cb.wait(250);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_RELEASED);
            for (int i = 0; i < 15; i++) {
                createAndDispatchMouseEvent(100, 100, 0, MouseEvent.MOUSE_WHEEL);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when keys are typed.
     * 
     * @throws Exception on error
     */
    @Test
    public void testWitKeyTyped() throws Exception {
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            createAndDispatchMouseEvent(100, 100, 0, MouseEvent.MOUSE_MOVED);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_PRESSED);
            // add time between press and release, so the system recognizes that as a click
            cb.wait(250);
            createAndDispatchMouseEvent(100, 100, MouseEvent.BUTTON1, MouseEvent.MOUSE_RELEASED);
            for (int i = 0; i < 15; i++) {
                createAndDispatchKeyEvent(KeyEvent.KEY_PRESSED);
                // add time between press and release, so the system recognizes that as a click
                cb.wait(250);
                createAndDispatchKeyEvent(KeyEvent.KEY_RELEASED);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }
}
