/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing;

import java.util.Map;

import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ExclusiveAccess;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;

/**
 *
 * <code>SecurityFacade</code> is an implementation of the {@link ISecurityFacade}, which delegates all the calls to its
 * delegate implementation of the {@link ISecurityFacade}. However, by default this facility uses swing implementation
 * of the {@link SecurityCallback} and also registers for swing input events to properly handle the auto logout.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public final class SecurityFacade implements ISecurityFacade {

    private static final long serialVersionUID = -624579467289843861L;
    private ISecurityFacade delegate;

    /**
     * Default constructor to be able to use this class as a service.
     */
    public SecurityFacade() {
        this(new se.esss.ics.rbac.access.SecurityFacade());
    }

    /**
     * Constructs a new SecurityFacade, using the provided delegate. It sets the swing auto logout and security callback
     * and sets the default inactivity timeout.
     *
     * @param delegate the delegate
     */
    public SecurityFacade(ISecurityFacade delegate) {
        this.delegate = delegate;
        setAutoLogout(new SwingAutoLogout(delegate));
        setAutoLogoutTimeout(RBACProperties.getInstance().getInactivityDefaultTimeout());
        setDefaultSecurityCallback(new SwingSecurityCallback());
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate()
     */
    @Override
    public Token authenticate() throws SecurityFacadeException {
        return delegate.authenticate();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate(se.esss.ics.rbac.access. SecurityCallback)
     */
    @Override
    public void authenticate(SecurityCallback callback) {
        delegate.authenticate(callback);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout(String)
     */
    @Override
    public boolean logout(String ip) throws SecurityFacadeException {
        return delegate.logout(ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout(se.esss.ics.rbac.access.SecurityCallback,
     * String)
     */
    @Override
    public void logout(SecurityCallback callback, String ip) {
        delegate.logout(callback, ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getToken(String)
     */
    @Override
    public Token getToken(String ip) throws SecurityFacadeException {
        return delegate.getToken(ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getLocalToken()
     */
    @Override
    public Token getLocalToken() {
        return delegate.getLocalToken();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#setToken(char[])
     */
    @Override
    public void setToken(char[] tokenID) {
        delegate.setToken(tokenID);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermission(String,
     * java.lang.String, java.lang.String)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean hasPermission(String ip, String resource, String permission)
            throws SecurityFacadeException {
        return delegate.hasPermission(ip, resource, permission);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(String,
     * java.lang.String, java.lang.String[])
     *
     * @throws IllegalArgumentException
     */
    @Override
    public Map<String, Boolean> hasPermissions(String ip, String resource, String... permissions)
            throws SecurityFacadeException {
        return delegate.hasPermissions(ip, resource, permissions);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(se.esss.ics.rbac.access.SecurityCallback,
     * String, java.lang.String, java.lang.String[])
     */
    @Override
    public void hasPermissions(SecurityCallback callback, String ip,
            String resource, String... permissions) {
        delegate.hasPermissions(callback, ip, resource, permissions);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(String, java.lang.String)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public String[] getRolesForUser(String ip, String username) throws SecurityFacadeException {
        return delegate.getRolesForUser(ip, username);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(se.esss.ics.rbac.access. SecurityCallback,
     * java.lang.String)
     */
    @Override
    public void getRolesForUser(SecurityCallback callback, String ip, final String username) {
        delegate.getRolesForUser(callback, ip, username);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(String,
     * java.lang.String, java.lang.String, int)
     */
    @Override
    public ExclusiveAccess requestExclusiveAccess(String ip,
            String resource, String permission, int durationInMinutes) throws SecurityFacadeException {
        return delegate.requestExclusiveAccess(ip, resource, permission, durationInMinutes);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * String, java.lang.String, java.lang.String, int)
     */
    @Override
    public void requestExclusiveAccess(SecurityCallback callback, String ip,
            String resource, String permission, int durationInMinutes) {
        delegate.requestExclusiveAccess(callback, ip, resource, permission, durationInMinutes);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(String,
     * java.lang.String, java.lang.String)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean releaseExclusiveAccess(String ip, String resource, String permission)
            throws SecurityFacadeException {
        return delegate.releaseExclusiveAccess(ip, resource, permission);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * String, java.lang.String, java.lang.String)
     */
    @Override
    public void releaseExclusiveAccess(SecurityCallback callback, String ip,
            String resource, String permission) {
        delegate.releaseExclusiveAccess(callback, ip, resource, permission);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRBACVersion()
     */
    @Override
    public String getRBACVersion() throws SecurityFacadeException {
        return delegate.getRBACVersion();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken(String)
     */
    @Override
    public Token renewToken(String ip) throws SecurityFacadeException {
        return delegate.renewToken(ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken(se.esss.ics.rbac.access.SecurityCallback,
     * String)
     */
    @Override
    public void renewToken(SecurityCallback callback, String ip) {
        delegate.renewToken(callback, ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid(String)
     */
    @Override
    public boolean isTokenValid(String ip) throws SecurityFacadeException {
        return delegate.isTokenValid(ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid(se.esss.ics.rbac.access.SecurityCallback, String)
     */
    @Override
    public void isTokenValid(SecurityCallback callback, String ip) {
        delegate.isTokenValid(callback, ip);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogoutTimeout(int)
     */
    @Override
    public void setAutoLogoutTimeout(int timeoutInMinutes) {
        delegate.setAutoLogoutTimeout(timeoutInMinutes);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogout(se.esss.ics.rbac.access.AutoLogout)
     */
    @Override
    public void setAutoLogout(AutoLogout autoLogout) {
        delegate.setAutoLogout(autoLogout);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getAutoLogoutTimeout()
     */
    @Override
    public int getAutoLogoutTimeout() {
        return delegate.getAutoLogoutTimeout();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#getDefaultSecurityCallback()
     */
    @Override
    public SecurityCallback getDefaultSecurityCallback() {
        return delegate.getDefaultSecurityCallback();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#destroy()
     */
    @Override
    public void destroy() {
        delegate.destroy();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#isDestroyed()
     */
    @Override
    public boolean isDestroyed() {
        return delegate.isDestroyed();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#setDefaultSecurityCallback(se.esss.ics.rbac
     * .access.SecurityCallback)
     */
    @Override
    public void setDefaultSecurityCallback(SecurityCallback callback) {
        if (callback == null) {
            delegate.setDefaultSecurityCallback(new SwingSecurityCallback());
        } else {
            delegate.setDefaultSecurityCallback(callback);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#initLocalServiceUsage()
     */
    @Override
    public void initLocalServiceUsage() {
        delegate.initLocalServiceUsage();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#addSecurityFacadeListener(se.esss.ics.rbac.
     * access.SecurityFacadeListener)
     */
    @Override
    public void addSecurityFacadeListener(SecurityFacadeListener listener) {
        delegate.addSecurityFacadeListener(listener);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#removeSecurityFacadeListener(se.esss.ics.rbac
     * .access.SecurityFacadeListener)
     */
    @Override
    public void removeSecurityFacadeListener(SecurityFacadeListener listener) {
        delegate.removeSecurityFacadeListener(listener);
    }
}
