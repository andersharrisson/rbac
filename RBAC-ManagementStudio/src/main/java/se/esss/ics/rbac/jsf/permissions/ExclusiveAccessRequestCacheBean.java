/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;

/**
 * <code>ExclusiveAccessRequestCacheBean</code> is a request bean used on the exclusive access page. It provides access
 * to DB content by caching the results for the duration of the request. Using this bean eliminates multiple calls to
 * the database, which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "eaRequestCacheBean")
@RequestScoped
public class ExclusiveAccessRequestCacheBean implements Serializable {

    private static final long serialVersionUID = -2905697569646774672L;

    @EJB
    private ExclusiveAccesses exclusiveAccessEJB;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private Permissions permissionsEJB;

    private Resource currentResource;
    private String currentUsername;

    private List<ExclusiveAccess> exclusiveAccesses;
    private List<Resource> resources;
    private List<Permission> permissions;

    /**
     * @param refresh if true retrieves exclusive access from database otherwise return cached exclusive access
     * 
     * @return list of all active exclusive access requests retrieved from database. Exclusive accesses are shown in
     *         active exclusive access request on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public List<ExclusiveAccess> getExclusiveAccesses(boolean refresh) {
        if (refresh || exclusiveAccesses == null) {
            exclusiveAccesses = exclusiveAccessEJB.getExclusiveAccesses();
        }
        return exclusiveAccesses;
    }

    /**
     * @return list of all resources retrieved from database. Resources are shown in drop down menu in request exclusive
     *         access dialog on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public List<Resource> getResources() {
        if (resources == null) {
            resources = resourcesEJB.getResources();
        }
        return resources;
    }

    /**
     * Returns list which contains permissions from source list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param resource resource
     * @param username users username
     * 
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsByResourceAndUser(Resource resource, String username) {
        if (permissions == null || currentResource.getId() != resource.getId() || !currentUsername.equals(username)) {
            currentResource = resource;
            currentUsername = username;
            permissions = permissionsEJB.getPermissionsByResourceAndUser(resource, username);
        }
        return permissions;
    }
}
