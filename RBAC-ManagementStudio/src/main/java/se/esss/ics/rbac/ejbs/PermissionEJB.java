/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>PermissionEJB</code> is a stateless bean containing utility methods for dealing with permissions. Contains
 * methods for retrieving, inserting, deleting and updating permission informations. All methods are defined in
 * <code>Permissions</code> interface.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class PermissionEJB implements Permissions {

    private static final long serialVersionUID = -2727993336072072915L;

    private static final String NAME = "name";
    private static final String RESOURCE = "resource";

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    @EJB
    private Resources resourcesEJB;

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermission(int)
     */
    @Override
    public Permission getPermission(int permissionId) {
        Permission permission = em.find(Permission.class, permissionId);
        // collections are lazy loaded
        if (permission != null) {
            permission.getRole().size();
        }
        return permission;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionByName(java.lang.String,
     * se.esss.ics.rbac.datamodel.Resource)
     */
    @Override
    public Permission getPermissionByName(String name, Resource resource) {
        List<Permission> permissionsList = em.createNamedQuery("Permission.findByName", Permission.class)
                .setParameter(NAME, name)
                .setParameter(RESOURCE, resource).getResultList();
        // collections are lazy loaded
        if (permissionsList != null && !permissionsList.isEmpty()) {
            permissionsList.get(0).getRole().size();
            return permissionsList.get(0);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissions()
     */
    @Override
    public List<Permission> getPermissions() {
        return em.createNamedQuery("Permission.selectAll", Permission.class).getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionNames(se.esss.ics.rbac.datamodel.Resource)
     */
    @Override
    public List<String> getPermissionNames(Resource resource) {
        return em.createNamedQuery("Permission.selectNamesByResource", String.class).setParameter(RESOURCE, resource)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionsByResource(int)
     */
    @Override
    public List<Permission> getPermissionsByResource(int resourceId) {
        return em.createNamedQuery("Permission.findByResource", Permission.class)
                .setParameter("resourceId", resourceId).getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionsByWildcard(java.lang.String)
     */
    @Override
    public List<Permission> getPermissionsByWildcard(String wildcard) {
        return em.createNamedQuery("Permission.findByWildcard", Permission.class).setParameter("wildcard", wildcard)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionsByRule(se.esss.ics.rbac.datamodel.Rule)
     */
    @Override
    public List<Permission> getPermissionsByRule(Rule rule) {
        return em.createNamedQuery("Permission.findByRule", Permission.class).setParameter("rule", rule)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.Permissions#getPermissionsByResourceAndUser(se.esss.ics.rbac.datamodel
     * .Resource, java.lang.String)
     */
    @Override
    public List<Permission> getPermissionsByResourceAndUser(Resource resource, String userId) {
        return em.createNamedQuery("Permission.findByResourceAndUser", Permission.class).setParameter("userId", userId)
                .setParameter(RESOURCE, resource).setParameter("eaAllowed", Boolean.TRUE).getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#getFilteredPermissionsGroupByNames(java.util.List)
     */
    @Override
    public List<Permission> getFilteredPermissionsGroupByNames(List<String> permissionNames) {
        return em.createNamedQuery("Permission.filterByResource", Permission.class)
                .setParameter("permissionNames", permissionNames).getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#createPermission(se.esss.ics.rbac.datamodel.Permission)
     */
    @Override
    public void createPermission(Permission permission) {
        em.persist(permission);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#createPermissions(java.util.Set)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createPermissions(Set<Permission> permissions) {
        int i = 0;
        for (Permission p : permissions) {
            em.persist(p);
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#createPermissionsAndResources(java.util.List)
     */
    @Override
    public void createPermissionsAndResources(List<Permission> permissions) {
        List<String> resourceNames = resourcesEJB.getResourceNames();
        for (Permission permission : permissions) {
            if (permission.getResource() == null) {
                continue;
            }
            if (!resourceNames.contains(permission.getResource().getName())) {
                em.persist(permission.getResource());
                resourceNames.add(permission.getResource().getName());
            }
            Resource resource = resourcesEJB.getResourceByName(permission.getResource().getName());
            permission.setResource(resource);
            if (!getPermissionNames(resource).contains(permission.getName())) {
                em.persist(permission);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#removePermission(se.esss.ics.rbac.datamodel.Permission)
     */
    @Override
    public void removePermission(Permission permission) {
        em.remove(em.find(Permission.class, permission.getId()));
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#updatePermission(se.esss.ics.rbac.datamodel.Permission)
     */
    @Override
    public void updatePermission(Permission permission) {
        em.merge(permission);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#updatePermissions(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updatePermissions(List<Permission> permissions) {
        int i = 0;
        for (Permission p : permissions) {
            em.merge(p);
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.ejbs.interfaces.Permissions#filterPermissions(java.util.List, java.util.List,
     * java.util.List, java.util.List, java.util.List)
     */
    @Override
    public List<Permission> filterPermissions(List<Role> roles, List<Resource> resources, List<String> ips,
            List<IPGroup> ipGroups, List<Expression> expressions) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Permission> criteriaQuery = criteriaBuilder.createQuery(Permission.class);
        Root<Permission> permission = criteriaQuery.from(Permission.class);
        criteriaQuery.select(permission).distinct(true);
        Predicate filterByRoles = criteriaBuilder.conjunction();
        if (roles != null && !roles.isEmpty()) {
            filterByRoles = permission.join("role", JoinType.INNER).in(roles); // param1
        }
        Predicate filterByResources = criteriaBuilder.conjunction();
        if (resources != null && !resources.isEmpty()) {
            filterByResources = permission.get(RESOURCE).in(resources); // param2
        }
        Predicate filterByRules = criteriaBuilder.conjunction();
        if (!expressions.isEmpty() || !ips.isEmpty() || !ipGroups.isEmpty()) {
            Subquery<Rule> subquery = criteriaQuery.subquery(Rule.class);
            Root<Rule> rule = subquery.from(Rule.class);
            Join<Rule, Expression> expressionsJoin = rule.join("expression", JoinType.INNER);
            Predicate filterByExpressions = criteriaBuilder.conjunction();
            if (!expressions.isEmpty()) {
                filterByExpressions = expressionsJoin.in(expressions); // param3
            }
            Predicate filterByIps = ips.isEmpty() ? criteriaBuilder.conjunction() : criteriaBuilder.disjunction();
            for (int i = 0; i < ips.size(); i++) {
                Predicate predicate = criteriaBuilder.like(expressionsJoin.<String> get("definition"), "%" + ips.get(i)
                        + "%");
                filterByIps = criteriaBuilder.or(filterByIps, predicate);
            }
            Predicate filterByIpGroups = (ipGroups.isEmpty()) ? criteriaBuilder.conjunction() : criteriaBuilder
                    .disjunction();
            for (int i = 0; i < ipGroups.size(); i++) {
                Predicate predicate = criteriaBuilder.like(expressionsJoin.<String> get("definition"), "%"
                        + ipGroups.get(i).getName() + "%");
                filterByIpGroups = criteriaBuilder.or(filterByIpGroups, predicate);
            }
            subquery.select(rule).distinct(true);
            subquery.where(criteriaBuilder.and(filterByExpressions, filterByIps, filterByIpGroups));
            filterByRules = permission.get("rule").in(subquery);
        }
        criteriaQuery.where(criteriaBuilder.and(filterByRoles, filterByResources, filterByRules));
        //TODO the order by is not correct
//        criteriaQuery.orderBy(criteriaBuilder.asc(permission.get("resource").get("name")),
//                criteriaBuilder.asc(permission.<String> get("name")));
        return em.createQuery(criteriaQuery).getResultList();
    }
}
