/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * <code>PasswordEncryptionService</code> is a utility class which provides methods for encrypting passwords and
 * comparing encrypted passwords.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public final class PasswordEncryptionService {

    private PasswordEncryptionService() {
    }

    /**
     * Encrypts the clear-text password using the same salt that was used to encrypt the original password. If encrypted
     * password that the user entered is equal to the stored hash, authentication succeeds.
     * 
     * @param attemptedPassword attempted password entered by user
     * @param encryptedPassword encrypted original password
     * @param salt salt that was used to encrypt original password
     * 
     * @return true if encrypted password that the user entered is equal to the stored password (authentication
     *         succeeds), otherwise false.
     * 
     * @throws NoSuchAlgorithmException if algorithm for password encryption was not found
     * @throws InvalidKeySpecException if cryptographic key specifications was invalid
     */
    public static boolean authenticate(char[] attemptedPassword, byte[] encryptedPassword, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword, salt);
        return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
    }

    /**
     * Returns encrypted clear-text password. Password is encrypted with PBKDF2 with SHA-1 hashing algorithm. SHA-1
     * generates 160 bit hashes. Pseudo-random function is called 2000 times to generate a block of keying material.
     * 
     * @param password clear-text password entered by user
     * @param salt salt that is used to encrypt entered clear-text password
     * 
     * @return encrypted clear-text password entered by user.
     * 
     * @throws NoSuchAlgorithmException if algorithm for password encryption was not found
     * @throws InvalidKeySpecException if cryptographic key specifications was invalid
     */
    public static byte[] getEncryptedPassword(char[] password, byte[] salt) throws NoSuchAlgorithmException,
            InvalidKeySpecException {
        KeySpec keySpec = new PBEKeySpec(password, salt, 20000, 160);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return factory.generateSecret(keySpec).getEncoded();
    }

    /**
     * Returns random generated 8 byte (64bit) salt using SHA1PRNG algorithm.
     * 
     * @return random generated salt using SHA1PRNG algorithm.
     * 
     * @throws NoSuchAlgorithmException if algorithm for generating the salt was not found
     */
    public static byte[] generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[8];
        random.nextBytes(salt);
        return salt;
    }
}
