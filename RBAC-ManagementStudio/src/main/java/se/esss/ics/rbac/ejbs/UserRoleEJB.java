/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.ejbs.interfaces.UserRoles;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>UserRoleEJB</code> is a stateless bean containing utility methods for dealing with user roles. Contains methods
 * for retrieving, inserting, deleting and updating user role informations. All methods are defined in
 * <code>UserRoles</code> interface.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Stateless
public class UserRoleEJB implements UserRoles {

    private static final long serialVersionUID = 14093456202342382L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#getUserRoles(java.lang.String)
     */
    @Override
    public List<UserRole> getUserRoles(String userId) {
        List<UserRole> userRoles = em.createNamedQuery("UserRole.findNonExpiredByUserId", UserRole.class)
                .setParameter("userId", userId).getResultList();
        for (UserRole userRole : userRoles) {
            // collections are lazy loaded
            userRole.getRole().getPermissions().size();
        }
        return userRoles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#getUsers(int)
     */
    @Override
    public List<String> getUsernames(int roleId) {
        return em.createNamedQuery("UserRole.selectUserIds", String.class).setParameter("roleId", roleId)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#createUserRole(se.esss.ics.rbac.datamodel.UserRole)
     */
    @Override
    public void createUserRole(UserRole userRole) {
        em.persist(userRole);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#createUserRoles(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createUserRoles(List<UserRole> userRoles) {
        for (int i = 0; i < userRoles.size(); i++) {
            em.persist(userRoles.get(i));
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#editUserRole(se.esss.ics.rbac.datamodel.UserRole)
     */
    @Override
    public void updateUserRole(UserRole userRole) {
        UserRole userRoleEntity = em.find(UserRole.class, userRole.getId());
        userRoleEntity.setAssignment(userRole.getAssignment());
        userRoleEntity.setEndTime(userRole.getEndTime());
        userRoleEntity.setStartTime(userRole.getStartTime());
        em.merge(userRoleEntity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.UserRoles#removeUserRole(se.esss.ics.rbac.datamodel.UserRole)
     */
    @Override
    public void removeUserRole(UserRole userRole) {
        em.remove(em.find(UserRole.class, userRole.getId()));
    }
}
