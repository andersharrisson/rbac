/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Rules;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RuleEJB</code> is a stateless bean containing utility methods for dealing with rules. Contains methods for
 * retrieving, inserting, deleting and updating rule informations. All methods are defined in <code>Rules</code>
 * interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class RuleEJB implements Rules {

    private static final long serialVersionUID = 3722638851072133565L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#getRule(int)
     */
    @Override
    public Rule getRule(int ruleId) {
        Rule rule = em.find(Rule.class, ruleId);
        // collections are lazy loaded
        if (rule != null) {
            rule.getExpression().size();
        }
        return rule;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#getRules()
     */
    @Override
    public List<Rule> getRules() {
        return em.createNamedQuery("Rule.selectAll", Rule.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#getRulesByExpression(se.esss.ics.rbac.datamodel.Expression)
     */
    @Override
    public List<Rule> getRulesByExpression(Expression expression) {
        List<Rule> rules = em.createNamedQuery("Rule.findByExpression", Rule.class)
                .setParameter("expression", expression).getResultList();
        // lazy load collection
        for (Rule rule : rules) {
            rule.getExpression().size();
        }
        return rules;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#getRulesByWildcard(java.lang.String)
     */
    @Override
    public List<Rule> getRulesByWildcard(String wildcard) {
        return em.createNamedQuery("Rule.findByWildcard", Rule.class).setParameter("wildcard", wildcard)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#createRule(se.esss.ics.rbac.datamodel.Rule)
     */
    @Override
    public void createRule(Rule rule) {
        em.persist(rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#updateRule(se.esss.ics.rbac.datamodel.Rule)
     */
    @Override
    public void updateRule(Rule rule) {
        em.merge(rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#removeRule(se.esss.ics.rbac.datamodel.Rule)
     */
    @Override
    public void removeRule(Rule rule) {
        em.remove(em.find(Rule.class, rule.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Rules#updateRules(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateRules(List<Rule> updatedRules) {
        int i = 0;
        for (Rule r : updatedRules) {
            em.merge(r);
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }
}
