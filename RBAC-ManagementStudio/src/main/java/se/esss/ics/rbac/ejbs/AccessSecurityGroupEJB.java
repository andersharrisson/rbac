/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.pvaccess.AccessSecurityGroup;
import se.esss.ics.rbac.pvaccess.AccessSecurityInput;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;

/**
 * <code>AccessSecurityGroupEJB</code> is a stateless bean containing utility methods for dealing with access security
 * groups. Contains methods for retrieving, inserting, deleting and updating access security group informations. All
 * methods are defined in <code>AccessSecurityGroups</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class AccessSecurityGroupEJB implements AccessSecurityGroups {

    private static final long serialVersionUID = -3429883288565958915L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#getAccessSecurityGroup(int)
     */
    @Override
    public AccessSecurityGroup getAccessSecurityGroup(int groupId) {
        AccessSecurityGroup group = em.find(AccessSecurityGroup.class, groupId);
        // collections are lazy loaded
        if (group != null) {
            group.getInputs().size();
            group.getRules().size();
        }
        return group;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#getAccessSecurityGroups()
     */
    @Override
    public List<AccessSecurityGroup> getAccessSecurityGroups() {
        return em.createNamedQuery("AccessSecurityGroup.selectAll", AccessSecurityGroup.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#getAccessSecurityGroupsByWildcard(java.lang.String)
     */
    @Override
    public List<AccessSecurityGroup> getAccessSecurityGroupsByWildcard(String wildcard) {
        return em.createNamedQuery("AccessSecurityGroup.findByWildcard", AccessSecurityGroup.class)
                .setParameter("wildcard", wildcard).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#createAccessSecurityGroup(se.esss.ics.rbac.pvaccess
     * .AccessSecurityGroup)
     */
    @Override
    public void createAccessSecurityGroup(AccessSecurityGroup group) {
        em.persist(group);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#removeAccessSecurityGroup(se.esss.ics.rbac.pvaccess
     * .AccessSecurityGroup)
     */
    @Override
    public void removeAccessSecurityGroup(AccessSecurityGroup group) {
        em.remove(em.find(AccessSecurityGroup.class, group.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#updateAccessSecurityGroup(se.esss.ics.rbac.pvaccess
     * .AccessSecurityGroup)
     */
    @Override
    public void updateAccessSecurityGroup(AccessSecurityGroup group) {
        em.merge(group);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#getAccessSecurityInput(int)
     */
    @Override
    public AccessSecurityInput getAccessSecurityInput(int asInputId) {
        return em.find(AccessSecurityInput.class, asInputId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#createAccessSecurityInput(se.esss.ics.rbac.pvaccess
     * .AccessSecurityInput)
     */
    @Override
    public void createAccessSecurityInput(AccessSecurityInput input) {
        em.persist(input);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#removeAccessSecurityInput(java.lang.Integer)
     */
    @Override
    public void removeAccessSecurityInput(Integer inputId) {
        em.remove(em.find(AccessSecurityInput.class, inputId));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#updateAccessSecurityInputs(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateAccessSecurityInputs(List<AccessSecurityInput> inputsList) {
        for (int i = 0; i < inputsList.size(); i++) {
            AccessSecurityInput input = inputsList.get(i);
            input.setIndex(input.getIndex() + 12);
            em.merge(input);
        }
        em.flush();
        em.clear();
        for (int i = 0; i < inputsList.size(); i++) {
            AccessSecurityInput input = inputsList.get(i);
            input.setIndex(input.getIndex() - 12);
            em.merge(input);
        }
        em.flush();
        em.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#getAccessSecurityRule(int)
     */
    @Override
    public AccessSecurityRule getAccessSecurityRule(int ruleId) {
        return em.find(AccessSecurityRule.class, ruleId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#createAccessSecurityRule(se.esss.ics.rbac.pvaccess
     * .AccessSecurityRule)
     */
    @Override
    public void createAccessSecurityRule(AccessSecurityRule rule) {
        em.persist(rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#updateAccessSecurityRule(se.esss.ics.rbac.pvaccess
     * .AccessSecurityRule)
     */
    @Override
    public void updateAccessSecurityRule(AccessSecurityRule rule) {
        em.merge(rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups#removeAccessSecurityRule(se.esss.ics.rbac.pvaccess
     * .AccessSecurityRule)
     */
    @Override
    public void removeAccessSecurityRule(AccessSecurityRule rule) {
        em.remove(em.find(AccessSecurityRule.class, rule.getId()));
    }
}
