/*
 * Copyright (C) 2015 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.rbac.jsf.admin;

import java.util.Arrays;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import se.esss.ics.rbac.ejbs.interfaces.Admin;

/**
 *
 * <code>Authentication</code> is a session scoped bean that handles authentication of the admin user.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "adminAuthBean")
@SessionScoped
public class Authentication {

    @EJB
    private Admin adminAuthEJB;

    private boolean isAuthenticated;
    private char[] enteredPassword;

    /**
     * Authenticates logged in user as administrator. User is successfully authenticated as administrator if entered
     * administrator password is correct.
     */
    public void authenticate() {
        if (isCharArrayEmpty(enteredPassword)) {
            isAuthenticated = false;
        } else {
            isAuthenticated = adminAuthEJB.authenticate(enteredPassword);
        }
        enteredPassword = null;
    }

    /**
     * @return password which is entered into password field in enter password dialog on <code>/Admin</code> pages.
     */
    public char[] getEnteredPassword() {
        return enteredPassword != null ? Arrays.copyOf(enteredPassword, enteredPassword.length) : null;
    }

    /**
     * Sets password which is entered into password field in enter password dialog on <code>/Admin</code> pages.
     *
     * @param enteredPassword entered password
     */
    public void setEnteredPassword(char[] enteredPassword) {
        this.enteredPassword = enteredPassword != null ? Arrays.copyOf(enteredPassword, enteredPassword.length) : null;
    }

    /**
     * @return true if is logged in user authenticated as administrator, otherwise false.
     */
    public boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    /**
     * Invalidate authentication.
     */
    void invalidate() {
        this.isAuthenticated = false;
    }

    /**
     * Checks if given char array is empty and return true if is empty, otherwise false.
     *
     * @param array char array
     * @return true if char array is empty, otherwise false.
     */
    private static boolean isCharArrayEmpty(char[] array) {
        return array == null || array.length == 0;
    }
}
