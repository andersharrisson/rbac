/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.RBACLog;
import se.esss.ics.rbac.RBACLog.Action;
import se.esss.ics.rbac.RBACLog.Severity;
import se.esss.ics.rbac.ejbs.interfaces.RBACLogs;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RBACLogEJB</code> is a stateless bean containing utility methods for retrieving RBAC log entries. All methods
 * are defined in <code>RBACLogs</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class RBACLogEJB implements RBACLogs {

    private static final long serialVersionUID = 5404774225264576913L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACLogs#getLogs(java.util.Date, java.util.Date,
     * se.esss.ics.rbac.RBACLog.Action[], se.esss.ics.rbac.RBACLog.Severity[], java.util.List)
     */
    @Override
    public List<RBACLog> getLogs(Date startTime, Date endTime, Action[] actions, Severity[] severities,
            List<String> users) {
        return em.createNamedQuery("RBACLog.filterLogs", RBACLog.class).setParameter("startTime", startTime)
                .setParameter("endTime", endTime).setParameter("actions", Arrays.asList(actions))
                .setParameter("severities", Arrays.asList(severities)).setParameter("users", users).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACLogs#getLogs(java.util.Date, java.util.Date,
     * se.esss.ics.rbac.RBACLog.Action[], se.esss.ics.rbac.RBACLog.Severity[])
     */
    @Override
    public List<RBACLog> getLogs(Date startTime, Date endTime, Action[] actions, Severity[] severities) {
        return em.createNamedQuery("RBACLog.filterLogsWithoutUsers", RBACLog.class)
                .setParameter("startTime", startTime).setParameter("endTime", endTime)
                .setParameter("actions", Arrays.asList(actions)).setParameter("severities", Arrays.asList(severities))
                .getResultList();
    }
}
