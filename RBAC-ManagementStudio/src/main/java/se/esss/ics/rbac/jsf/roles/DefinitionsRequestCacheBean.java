/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.roles;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>DefinitionsRequestCacheBean</code> is a request bean used on the roles page. It provides access to DB content
 * by caching the results for the duration of the request. Using this bean eliminates multiple calls to the database,
 * which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "rolesRequestCacheBean")
@RequestScoped
public class DefinitionsRequestCacheBean implements Serializable {

    private static final long serialVersionUID = 4763213283228593406L;

    @EJB
    private Roles rolesEJB;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private Permissions permissionsEJB;

    private String currentWildcard = Constants.ALL;
    private int currentResourceId;

    private List<Permission> permissions;
    private List<Resource> resources;
    private List<Role> roles;
    private List<Role> rolesByWildcard;

    /**
     * Returns list which contains permissions and is shown on add permissions dialog on <code>/Roles/Definitions</code>
     * page.
     * 
     * @param resourceId resource id
     * 
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsByResource(int resourceId) {
        if (permissions == null || currentResourceId != resourceId) {
            currentResourceId = resourceId;
            permissions = permissionsEJB.getPermissionsByResource(resourceId);
        }
        return permissions;
    }

    /**
     * Retrieves and returns list of resources from database. Resources are shown in drop down list component in select
     * permissions dialog on <code>/Roles/Definitions</code> page.
     * 
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResources() {
        if (resources == null) {
            resources = resourcesEJB.getResources();
        }
        return resources;
    }

    /**
     * @param refresh if true retrieves roles from database otherwise return cached roles
     * 
     * @return list of roles retrieved from database, which corresponds to the wildcard pattern. Roles are shown in list
     *         box on <code>/Roles/Definitions</code> page.
     */
    public List<Role> getRoles(boolean refresh) {
        if (refresh || roles == null) {
            roles = rolesEJB.getRoles();
        }
        return roles;
    }

    /**
     * @param wildcard search roles by wildcard
     * @param refresh if true retrieves roles from database otherwise return cached roles
     * 
     * @return list of roles retrieved from database (which matches wildcard), which corresponds to the wildcard
     *         pattern. Roles are shown in list box on <code>/Roles/Definitions</code> page.
     */
    public List<Role> getRolesByWildcard(String wildcard, boolean refresh) {
        if (refresh || roles == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            rolesByWildcard = rolesEJB.getRolesByWildcard(wildcard);
        }
        return rolesByWildcard;
    }
}
