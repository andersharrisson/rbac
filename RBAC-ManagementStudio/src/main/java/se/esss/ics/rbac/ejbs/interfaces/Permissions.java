/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;

/**
 * <code>Permissions</code> interface defines methods for dealing with permissions.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Permissions extends Serializable {

    /**
     * Retrieves permission identified by <code>permissionId</code> from database and returns it.
     * 
     * @param permissionId unique permission identifier
     * 
     * @return specific permission identified by id.
     */
    Permission getPermission(int permissionId);

    /**
     * Retrieves permission whose name matches the given <code>name</code> and is in the given <code>resource</code>
     * from database and returns it.
     * 
     * @param name permission name
     * @param resource permission resource
     * 
     * @return permission whose properties matches to the given parameters.
     */
    Permission getPermissionByName(String name, Resource resource);

    /**
     * @return list of the permissions retrieved from database.
     */
    List<Permission> getPermissions();

    /**
     * Retrieves all permissions names which are in specific <code>resource</code> and returns it.
     * 
     * @param resource permission resource
     * 
     * @return list of the permission names which are in specific resource.
     */
    List<String> getPermissionNames(Resource resource);

    /**
     * Retrieves permissions whose names are not in <code>permissionNames</code> list from database and returns it.
     * 
     * @param permissionNames permission names
     * 
     * @return list of the permissions whose names are not in the given list.
     */
    List<Permission> getFilteredPermissionsGroupByNames(List<String> permissionNames);

    /**
     * Retrieves permissions which are in specific resource identified by <code>resourceId</code> from database and
     * returns it.
     * 
     * @param resourceId unique resource identifier
     * 
     * @return list of permissions which are in specific resource identified by id.
     */
    List<Permission> getPermissionsByResource(int resourceId);

    /**
     * Retrieves all permissions which are assigned to the user which is identified by <code>userId</code> and are in
     * given <code>resource</code> and returns it.
     * 
     * @param userId unique user identifier
     * @param resource permission resource
     * 
     * @return list of the permissions which are assigned to the specific user and are in given resource.
     */
    List<Permission> getPermissionsByResourceAndUser(Resource resource, String userId);

    /**
     * Retrieves all permissions whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the permissions whose name matches the wildcard pattern.
     */
    List<Permission> getPermissionsByWildcard(String wildcard);

    /**
     * Retrieves permissions which contains the given <code>rule</code> and returns it.
     * 
     * @param rule permission rule
     * 
     * @return list of the permissions which contains the given rule.
     */
    List<Permission> getPermissionsByRule(Rule rule);

    /**
     * Retrieves all permissions whose properties corresponds to the given parameters from database and returns it.
     * 
     * @param roles list of roles
     * @param resources list of resources
     * @param ips list of IPs
     * @param ipGroups list of IP groups
     * @param expressions list of expressions
     * 
     * @return list of the permissions whose properties corresponds to the given parameters.
     */
    List<Permission> filterPermissions(List<Role> roles, List<Resource> resources, List<String> ips,
            List<IPGroup> ipGroups, List<Expression> expressions);

    /**
     * Inserts created permission into database.
     * 
     * @param permission created permission
     */
    void createPermission(Permission permission);

    /**
     * Inserts created permissions into database.
     * 
     * @param permissions set of the created permissions
     */
    void createPermissions(Set<Permission> permissions);

    /**
     * Inserts all created permissions into database. If permission resource does not exist, first creates resource and
     * inserts it into database.
     * 
     * @param permissions list of created permissions.
     */
    void createPermissionsAndResources(List<Permission> permissions);

    /**
     * Removes permission from database.
     * 
     * @param permission permission which will be removed from database
     */
    void removePermission(Permission permission);

    /**
     * Updates permission.
     * 
     * @param permission updated permission
     */
    void updatePermission(Permission permission);

    /**
     * Updates permissions.
     * 
     * @param permissions list of the updated permissions
     */
    void updatePermissions(List<Permission> permissions);
}
