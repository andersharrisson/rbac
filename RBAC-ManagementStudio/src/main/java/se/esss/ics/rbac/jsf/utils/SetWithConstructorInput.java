/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.utils;

import java.util.HashSet;

/**
 * 
 * <code>SetWithConstructorInput</code> is an {@link HashSet} that accepts a plain object at construction, and inserts
 * it into the set. The set is configured to have the initial capacity of 1.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 * @param <T> the type of objects held by this set.
 */
public class SetWithConstructorInput<T> extends HashSet<T> {

    private static final long serialVersionUID = -4737590377598599265L;

    /**
     * Constructs a new Set and inserts the given element.
     * 
     * @param content the element
     */
    public SetWithConstructorInput(T content) {
        super(1);
        if (content != null) {
            add(content);
        }
    }
}
