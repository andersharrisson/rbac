/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>ResourceEJB</code> is a stateless bean containing utility methods for dealing with resources. Contains methods
 * for retrieving, inserting, deleting and updating resource informations. All methods are defined in
 * <code>Resources</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class ResourceEJB implements Resources {

    private static final long serialVersionUID = 1954809440250929899L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#getResource(int)
     */
    @Override
    public Resource getResource(int resourceId) {
        Resource resource = em.find(Resource.class, resourceId);
        // collections are lazy loaded
        if (resource != null) {
            resource.getPermissions().size();
            for (Permission p : resource.getPermissions()) {
                for (Role role : p.getRole()) {
                    role.getPermissions().size();
                }
            }
            resource.getManagers().size();
        }
        return resource;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#getResources()
     */
    @Override
    public List<Resource> getResources() {
        return em.createNamedQuery("Resource.selectAll", Resource.class).getResultList();
        // collections are lazy loaded
        //TODO
//        for (int i = 0; i < resources.size(); i++) {
//            resources.get(i).getPermissions().size();
//        }
//        return resources;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#getResourceNames()
     */
    @Override
    public List<String> getResourceNames() {
        return em.createNamedQuery("Resource.selectNames", String.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#getResourcesByWildcard(java.lang.String)
     */
    @Override
    public List<Resource> getResourcesByWildcard(String wildcard) {
        return em.createNamedQuery("Resource.findByWildcard", Resource.class)
                .setParameter("wildcard", wildcard).getResultList();
        //TODO
        // collections are lazy loaded
//        for (int i = 0; i < resources.size(); i++) {
//            resources.get(i).getPermissions().size();
//        }
//        return resources;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#getResourceByName(java.lang.String)
     */
    @Override
    public Resource getResourceByName(String resourceName) {
        List<Resource> resourcesList = em.createNamedQuery("Resource.findByName", Resource.class)
                .setParameter("name", resourceName)
                .getResultList();
        // collections are lazy loaded
        if (resourcesList != null && !resourcesList.isEmpty()) {
            Set<Permission> permissions = resourcesList.get(0).getPermissions();
            if (permissions != null) {
                permissions.size();
            }
            Set<String> managers = resourcesList.get(0).getManagers();
            if (managers != null) {
                managers.size();
            }
            return resourcesList.get(0);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#createResource(se.esss.ics.rbac.datamodel.Resource)
     */
    @Override
    public void createResource(Resource resource) {
        em.persist(resource);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#removeResource(se.esss.ics.rbac.datamodel.Resource)
     */
    @Override
    public void removeResource(Resource resource) {
        em.remove(em.find(Resource.class, resource.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Resources#updateResource(se.esss.ics.rbac.datamodel.Resource)
     */
    @Override
    public void updateResource(Resource resource) {
        em.merge(resource);
    }
}
