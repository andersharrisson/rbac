/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.dummy;

import java.util.Objects;

import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 * 
 * <code>DummyUserInfo</code> is an implementation of the {@link UserInfo} used in combination with
 * {@link DummyDirectoryServiceAccess}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class DummyUserInfo implements UserInfo {

    private static final long serialVersionUID = -4322642356904477469L;
    private String username;
    private String lastName;
    private String firstName;
    private String middleName;
    private String group;
    private String eMail;
    private String phoneNumber;
    private String location;

    /**
     * Default constructor.
     */
    public DummyUserInfo() {
        // EMPTY
    }

    /**
     * Constructs user with all information about it.
     * 
     * @param username username of the user
     * @param lastName last name of the user
     * @param firstName first name of the user
     * @param middleName middle name of the user
     * @param group name of the group this user belongs to
     * @param eMail e-mail of the user
     * @param phoneNumber phone number of the user
     * @param location location of the user
     */
    DummyUserInfo(String username, String lastName, String firstName, String middleName, String group, String eMail,
            String phoneNumber, String location) {
        this.username = username;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.group = group;
        this.eMail = eMail;
        this.phoneNumber = phoneNumber;
        this.location = location;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getUsername()
     */
    @Override
    public String getUsername() {
        return username;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getLastName()
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getFirstName()
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getMiddleName()
     */
    @Override
    public String getMiddleName() {
        return middleName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getGroup()
     */
    @Override
    public String getGroup() {
        return group;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getEMail()
     */
    @Override
    public String getEMail() {
        return eMail;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getPhoneNumber()
     */
    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getLocation()
     */
    @Override
    public String getLocation() {
        return location;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(UserInfo o) {
        if (this.lastName != null && o.getLastName() != null) {
            int i = this.lastName.compareTo(o.getLastName());
            if (i != 0) {
                return i;
            }
        }
        if (this.firstName != null && o.getFirstName() != null) {
            int i = this.firstName.compareTo(o.getFirstName());
            if (i != 0) {
                return i;
            }
        }
        if (this.username != null && o.getUsername() != null) {
            if (o.getUsername() == null) {
                return -1;
            } else {
                return this.username.compareTo(o.getUsername());
            }
        }
        return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DummyUserInfo other = (DummyUserInfo) obj;
        return Objects.equals(firstName,other.firstName) 
                && Objects.equals(lastName,other.lastName)
                && Objects.equals(username,other.username);
    }

}
