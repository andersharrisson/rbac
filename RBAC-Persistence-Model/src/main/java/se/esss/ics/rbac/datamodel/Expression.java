/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>Expression</code> is a generic entity that can hold any kind of expression that can be evaluated to true or
 * false. The expression is used to provide the smallest unit of {@link Rule} evaluation equation. For example:
 * <code>PV X is 1</code> or <code>IP is 10.5.2.222</code>. The result of this expression should always be true or false
 * and can be combined with other expressions defined by {@link Rule}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "expression")
@Table(name = "expression", 
    uniqueConstraints = { @UniqueConstraint(name = "UK_EXPRESSION_name", columnNames = { "name" }) }, 
    indexes = { @Index(name = "INDEX_EXPRESSION_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "Expression.selectAll", 
                query = "SELECT e FROM se.esss.ics.rbac.datamodel.Expression e ORDER BY e.name"),
        @NamedQuery(name = "Expression.findByWildcard", 
                query = "SELECT e FROM se.esss.ics.rbac.datamodel.Expression e WHERE e.name LIKE :wildcard") })
public final class Expression implements Serializable, NamedEntity {

    private static final long serialVersionUID = -4928703476586300217L;

    private static final String INVALID_NAME = "Expression name should not contain white spaces or any of the "
            + "following characters: &, =, !, <, >, |, ', \", `, (, ).";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 10, nullable = false)
    private String name;
    @Column(name = "long_name", nullable = false)
    private String longName;
    @Column(name = "description", length = 1000)
    private String description;
    @Column(name = "definition", length = 1000, nullable = false)
    private String definition;

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /**
     * @param name the name of the expression
     */
    public void setName(String name) {
        if (name != null
                && (name.indexOf(' ') > -1 || name.indexOf('&') > -1 || name.indexOf('=') > -1
                        || name.indexOf('!') > -1 || name.indexOf('<') > -1 || name.indexOf('>') > -1
                        || name.indexOf('|') > -1 || name.indexOf('\'') > -1 || name.indexOf('"') > -1
                        || name.indexOf('`') > -1 || name.indexOf('(') > -1 || name.indexOf(')') > -1 
                        || name.indexOf('@') > -1)) {
            throw new IllegalArgumentException(INVALID_NAME);

        }
        this.name = name;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @return the long (descriptive) name of this expression
     */
    public String getLongName() {
        return longName;
    }

    /**
     * @param longName the long (descriptive) name of this expression
     */
    public void setLongName(String longName) {
        this.longName = longName;
    }

    /**
     * @param description the detailed description of this expression
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the detailed description of the expression
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the definition of this expression. Definition is text, which follows certain rules. The definition should be
     * parsable and it should be possible to evaluate the definition to <code>true</code> or <code>false</code>.
     * 
     * @param definition the definition of the expression
     */
    public void setDefinition(String definition) {
        this.definition = getCorrectedDefinition(definition.trim());
    }

    /**
     * @return the definition of this expression
     */
    public String getDefinition() {
        return definition;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, longName, definition, description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Expression other = (Expression) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(longName, other.longName)
                && Objects.equals(definition, other.definition) && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(description == null ? 200 : (description.length() + 200)).append("EXPRESSION [")
                .append(tab).append("Name: ").append(name).append(tab).append("Long Name: ").append(longName)
                .append(tab).append("Description: ").append(description).append(tab).append("Definition: ")
                .append(definition).append(endTab).append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }

    private String getCorrectedDefinition(String definition) {
        String def = definition.replace("  ", " ");
        String dUpper = def.toUpperCase();
        if (dUpper.startsWith("PV")) {
            return checkPVExpression(def);
        } else if (dUpper.startsWith("IP")) {
            return checkIPExpression(def);
        }
        return def;
    }

    private static String checkPVExpression(String tmpDefinition) {
        StringBuilder def = new StringBuilder(100);
        def.append('P').append('V').append(' ');
        char[] operations1 = { '=', '<', '>' };
        String[] operations2 = { "!=", "<=", ">=" };
        for (int i = 2; i < tmpDefinition.length(); i++) {
            char character = tmpDefinition.charAt(i);
            boolean isOperator = false;
            boolean leftSpace = tmpDefinition.charAt(tmpDefinition.length() - 1) != ' ';
            for (int j = 0; j < operations2.length; j++) {
                String operator = operations2[j];
                if (i + 1 < tmpDefinition.length()) {
                    String substring = tmpDefinition.substring(i, i + 2);
                    if (operator.equals(substring)) {
                        if (leftSpace) {
                            def.append(' ');
                        }
                        def.append(operator).append(' ');
                        isOperator = true;
                        i++;
                        break;
                    }
                }
            }
            if (!isOperator) {
                for (int j = 0; j < operations1.length; j++) {
                    if (operations1[j] == character) {
                        if (leftSpace) {
                            def.append(' ');
                        }
                        def.append(character).append(' ');
                        isOperator = true;
                        break;
                    }
                }
            }
            if (!isOperator && character != ' ') {
                def.append(character);
            }
        }
        return def.toString();
    }

    private static String checkIPExpression(String tmpDefinition) {
        StringBuilder def = new StringBuilder(100);
        def.append('I').append('P');
        for (int i = 2; i < tmpDefinition.length(); i++) {
            char character = tmpDefinition.charAt(i);
            boolean leftSpace = tmpDefinition.charAt(tmpDefinition.length() - 1) != ' ';
            if (i + 1 < tmpDefinition.length()) {
                String substring = tmpDefinition.substring(i, i + 2);
                if ("!=".equals(substring)) {
                    if (leftSpace) {
                        def.append(' ');
                    }
                    def.append('!').append('=').append(' ');
                    i++;
                    continue;
                } else if ("!@".equals(substring)) {
                    if (leftSpace) {
                        def.append(' ');
                    }
                    def.append('!').append('@').append(' ');
                    i++;
                    continue;
                }
            }
            if (character == '@') {
                if (leftSpace) {
                    def.append(' ');
                }
                def.append('@').append(' ');
                continue;
            }
            if (character == '=') {
                if (leftSpace) {
                    def.append(' ');
                }
                def.append('=').append(' ');
                continue;
            }
            if (character != ' ') {
                def.append(character);
            }
        }
        return def.toString();
    }
}
