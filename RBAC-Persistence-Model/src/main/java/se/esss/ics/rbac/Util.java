/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * 
 * <code>Util</code> provides utility methods for handling the persistence model.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class Util {

    /** Default indent for toString methods */
    public static final String INDENT = "  ";
    private static final String EMPTY_ARRAY = "[]";
    private static final String LAZY_ARRAY = "[...]";

    private Util() {
    }

    /**
     * Transforms the collection of entity descriptors into a string using the provided indentation level.
     * 
     * @param collection the collection to transform to string
     * @param level the indentation level
     * @return string representation of the collection
     */
    public static String toString(Collection<? extends EntityDescriptor> collection, int level) {
        try {
            if (collection == null || collection.isEmpty()) {
                return EMPTY_ARRAY;
            }
        } catch (RuntimeException e) {
            // can happen if lazy initialisation is used and collection has not been initialised
            return LAZY_ARRAY;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        String indent = getIndent(level);
        int i = 0;
        for (EntityDescriptor e : collection) {
            if (i++ != 0) {
                sb.append('\n').append(indent);
            }
            sb.append(e.toString(level));
        }
        sb.append(" ]");
        return sb.toString();
    }

    /**
     * Transforms the collection of string into a string using the provided indentation level.
     * 
     * @param collection the collection to transform to string
     * @param level the indentation level
     * @return string representation of the collection
     */
    public static String toString(Set<String> collection, int level) {
        try {
            if (collection == null || collection.isEmpty()) {
                return EMPTY_ARRAY;
            }
        } catch (RuntimeException e) {
            // can happen if lazy initialisation is used and collection has not been initialised
            return LAZY_ARRAY;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        int i = 0;
        for (String e : collection) {
            if (i++ != 0) {
                sb.append('\n');
            }
            sb.append(e);
        }
        sb.append(" ]");
        return sb.toString();
    }

    /**
     * Returns the indent string for the given level.
     * 
     * @param level the indentation level
     * @return the indentation string
     */
    public static String getIndent(int level) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < level; i++) {
            temp.append(INDENT);
        }

        return temp.toString();
    }

    /**
     * Sorts the named entities according to the their names.
     * 
     * @param list the list to sort
     * @param <T> the NamedEntityType that is sorted 
     * @return the sorted list
     */
    public static <T extends NamedEntity> List<T> sort(List<T> list) {
        Collections.sort(list, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }

    /**
     * Sorts the named entities according to the their names.
     * 
     * @param list the list to sort
     * @param <T> the NamedEntityType that is sorted 
     * @return the sorted list
     */
    public static <T extends NamedEntity> T[] sort(T[] list) {
        Arrays.sort(list, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }
    
    /**
     * Verifies if the list contains an entry with the same id as is the id of the given element.
     * 
     * @param <T> the type of the entity to check
     * @param list the list containing the elements to check
     * @param element the element whose id we are looking for
     * @return true if the element was found or false otherwise
     * 
     */
    public static <T extends NamedEntity> boolean contains(List<T> list, T element) {
        if (element == null || list == null || list.isEmpty()) {
            return false;
        }
        int id = element.getId();
        for (T e : list) {
            if (e.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
