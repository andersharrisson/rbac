/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>Rule</code> defines additional restrictions or allowances for permissions. Some permissions can be granted only
 * when machine state has a certain value and some permissions might be granted always if the request comes from a
 * specified IP (e.g. main control room), regardless of the role that is asking for permission.
 * <p>
 * Rule combines a set of expressions and defines the relations to be used between them using the and, or, and not
 * operators.
 * </p>
 * <p>
 * Example: the following expressions are associated with this rule: <code>E1 = PV A is 1</code>,
 * <code>E2 = IP is a.b.c.d</code>. The following definition can be set on this rule: <code>(R | E2) &#36; !E1</code>,
 * where R represents the role, E1 and E2 are the expressions. The permission that is associated with this rule should
 * be granted to anyone that has a role with that permission (R) or is making the request from the computer with the IP
 * a.b.c.d. At the same time it should only be granted if the value of the PV A is different than 1.
 * </p>
 * <p>
 * The same rule can be assigned to as many permissions as one wishes.
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "rule")
@Table(name = "rule", 
    indexes = { 
        @Index(name = "INDEX_RULE_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "Rule.selectAll",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Rule r ORDER BY r.name"),
        @NamedQuery(name = "Rule.findByWildcard",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Rule r WHERE r.name LIKE :wildcard"),
        @NamedQuery(name = "Rule.findByExpression",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Rule r"
                        + " WHERE :expression IN elements(r.expression)") })
public final class Rule implements Serializable, NamedEntity {

    private static final long serialVersionUID = 1291343119795422990L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", length = 1000)
    private String description;
    @Column(name = "definition", length = 1000, nullable = false)
    private String definition;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(foreignKey = @ForeignKey(name = "FK_RULE_EXPRESSION_rule"), inverseForeignKey = @ForeignKey(
            name = "FK_RULE_EXPRESSION_expression"))
    private Set<Expression> expression;

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name for this rule
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the detailed description of this rule
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the detailed description of this rule
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the definition of this rule, which can be evaluated. The definition is an expression composed of the
     *         {@link Expression}s associated with this rule.
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * @param definition the definition of the rule
     */
    public void setDefinition(String definition) {
        // TODO parse definition and check if it matches the rules
        this.definition = definition;
    }

    /**
     * @return the expression used in this rule evaluation
     */
    public Set<Expression> getExpression() {
        return expression;
    }

    /**
     * @param expression the expression used in rule evaluation
     */
    public void setExpression(Set<Expression> expression) {
        this.expression = expression;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, definition);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Rule other = (Rule) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(definition, other.definition)
                && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String expressions = Util.toString(expression, level + 1);
        int descLength = description == null ? 4 : description.length();
        return new StringBuilder(expressions.length() + descLength + 150)
                .append("RULE [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Description: ").append(description).append(tab)
                .append("Definition: ").append(definition).append(tab)
                .append("Expressions: ").append(expressions).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
