/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * <code>ConfigurationParameter</code> is a generic entity that can hold any kind of key-value pair. Configuration
 * parameters are used for storing configuration values that may be customisable during the runtime of an application,
 * for instance file paths, timeout settings, etc. Configuration parameters are uniquely identified by their names, but
 * may otherwise hold any value.
 *
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Entity(name = "configuration_parameter")
public class ConfigurationParameter implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = -8134000354796755153L;

    @Id
    @Column(name = "name", length = 32, nullable = false, unique = true)
    private String name;

    @Column(name = "value", length = 1000)
    private String value;

    /**
     * Returns the name of this entry.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the configuration parameter
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of the configuration parameter
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value of the configuration parameter
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Returns the value of the configuration parameter as a <code>long</code>. This is equivalent to calling
     * <code>Long.parseLong(getValue())</code>.
     *
     * @return the value of the configuration parameter as a <code>long</code>
     *
     * @throws NumberFormatException if the configuration parameter value does not contain a parsable long.
     */
    public long getValueAsLong() {
        return Long.parseLong(getValue());
    }

    /**
     * Returns the value of the configuration parameter as a <code>double</code>. This is equivalent to calling
     * <code>Double.parseDouble(getValue())</code>.
     *
     * @return the value of the configuration parameter as a <code>double</code>
     *
     * @throws NullPointerException if the configuration parameter value is <code>null</code>.
     * @throws NumberFormatException if the configuration parameter value does not contain a parsable double.
     */
    public double getValueAsDouble() {
        return Double.parseDouble(getValue());
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        ConfigurationParameter other = (ConfigurationParameter) obj;
        return Objects.equals(name, other.name) && Objects.equals(value, other.value);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(150).append("CONFIG [").append(tab).append("Name: ").append(name).append(tab)
                .append("Value: ").append(value).append(endTab).append(']').toString();

    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }

}
