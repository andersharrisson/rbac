/*
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron,
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS.
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE.
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION,
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.util.Objects;

import org.csstudio.data.values.IMetaData;
import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.ITimestamp;
import org.csstudio.data.values.IValue;

/**
 * Implementation of the {@link IValue} interface.
 * 
 * @author Kay Kasemir
 */
public abstract class Value implements IValue {
    /** Time stamp of this value. */
    private final ITimestamp time;

    /** Severity code of this value. */
    private final ISeverity severity;

    /** Status text for this value's severity. */
    private final String status;

    /** Meta data (may be null). */
    private final IMetaData metaData;

    /**
     * The max count of values to be formatted into string. The value beyond this count will be omitted.
     */
    public static final int MAX_FORMAT_VALUE_COUNT = 20;

    /**
     * Constructs a new Value from pieces.
     * 
     * @param time the timestamp of the value
     * @param severity the severity of the value
     * @param status the status of the value
     * @param metaData the meta data describing the PV
     */
    public Value(final ITimestamp time, final ISeverity severity, final String status, final IMetaData metaData) {
        this.time = time;
        this.severity = severity;
        this.status = status;
        this.metaData = metaData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#getTime()
     */
    @Override
    public final ITimestamp getTime() {
        return time;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#getSeverity()
     */
    @Override
    public final ISeverity getSeverity() {
        return severity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#getStatus()
     */
    @Override
    public final String getStatus() {
        return status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#getMetaData()
     */
    @Override
    public IMetaData getMetaData() {
        return metaData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#format(org.csstudio.platform.data.IValue .Format, int)
     */
    @Override
    public abstract String format(Format how, int precision);

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IValue#format()
     */
    @Override
    public final String format() {
        return format(Format.DEFAULT, -1);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString() {
        String value = format(Format.DEFAULT, -1);
        StringBuilder buffer = new StringBuilder(100 + value.length()).append(getTime().toString())
                .append(Messages.COLUMN_SEPARATOR).append(value);
        String sevr = getSeverity().toString();
        String stat = getStatus();
        if (sevr.length() > 0 || stat.length() > 0) {
            buffer.append(Messages.COLUMN_SEPARATOR).append(sevr).append(Messages.SEVERITY_STATUS_SEPARATOR)
                    .append(stat);
        }
        return buffer.toString();
    }

    /**
     * Convert char into printable character for Format.String.
     * 
     * @param c Char, 0 for end-of-string
     * @return Printable version
     */
    protected char getDisplayChar(final char c) {
        if (c == 0) {
            return 0;
        }
        if (Character.isLetterOrDigit(c) || Character.isWhitespace(c)
                || Character.getType(c) == Character.OTHER_PUNCTUATION 
                || Character.getType(c) == Character.MATH_SYMBOL) {
            return c;
        }
        return '?';
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(metaData, severity, status, time);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Value other = (Value) obj;
        return Objects.equals(metaData, other.metaData) && Objects.equals(severity, other.severity)
                && Objects.equals(status, other.status) && Objects.equals(time, other.time);
    }
}
