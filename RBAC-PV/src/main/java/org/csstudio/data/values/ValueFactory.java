/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values;

import org.csstudio.data.values.internal.DoubleValue;
import org.csstudio.data.values.internal.EnumeratedMetaData;
import org.csstudio.data.values.internal.EnumeratedValue;
import org.csstudio.data.values.internal.LongValue;
import org.csstudio.data.values.internal.NumericMetaData;
import org.csstudio.data.values.internal.SeverityInstances;
import org.csstudio.data.values.internal.StringValue;

/**
 * Factory for IValue-based types.
 * 
 * @author Kay Kasemir
 */
public final class ValueFactory {
    /** Prohibit instance creation. */
    private ValueFactory() {
    }

    /**
     * Create an 'OK' ISeverity.
     * 
     * @return ISeverity
     */
    public static ISeverity createOKSeverity() {
        return SeverityInstances.OK;
    }

    /**
     * Create a 'minor' ISeverity.
     * 
     * @return ISeverity
     */
    public static ISeverity createMinorSeverity() {
        return SeverityInstances.MINOR;
    }

    /**
     * Create a 'major' ISeverity.
     * 
     * @return ISeverity
     */
    public static ISeverity createMajorSeverity() {
        return SeverityInstances.MAJOR;
    }

    /**
     * Create an 'invalid' ISeverity.
     * 
     * @return ISeverity
     */
    public static ISeverity createInvalidSeverity() {
        return SeverityInstances.INVALID;
    }

    /**
     * Create instance of {@link INumericMetaData}.
     * 
     * @param dispLow Low end of suggested display range.
     * @param dispHigh High end of suggested display range.
     * @param warnLow Lower warning limit.
     * @param warnHigh Upper warning limit.
     * @param alarmLow Lower alarm limit.
     * @param alarmHigh Upper alarm limit.
     * @param prec Suggested display precision.
     * @param units Engineering units string.
     * @return Instance of INumericMetaData.
     */
    public static INumericMetaData createNumericMetaData(double dispLow, double dispHigh, double warnLow,
            double warnHigh, double alarmLow, double alarmHigh, int prec, String units) {
        return new NumericMetaData(dispLow, dispHigh, warnLow, warnHigh, alarmLow, alarmHigh, prec, units);
    }

    /**
     * Create instance of {@link IEnumeratedMetaData}.
     * 
     * @param states State strings
     * @return Instance of IEnumeratedMetaData.
     */
    public static IEnumeratedMetaData createEnumeratedMetaData(String[] states) {
        return new EnumeratedMetaData(states);
    }

    /**
     * Create instance of {@link IDoubleValue}.
     * 
     * @param time Time stamp
     * @param severity Severity descriptor
     * @param status Status string.
     * @param metaData Numeric meta data.
     * @param values The actual values.
     * @return Instance of IDoubleValue.
     */
    public static IDoubleValue createDoubleValue(ITimestamp time, ISeverity severity, String status,
            INumericMetaData metaData, double[] values) {
        return new DoubleValue(time, severity, status, metaData, values);
    }

    /**
     * Create instance of {@link ILongValue}.
     * 
     * @param time Time stamp
     * @param severity Severity descriptor
     * @param status Status string.
     * @param metaData Numeric meta data.
     * @param values The actual values.
     * @return Instance of IIntegerValue.
     */
    public static ILongValue createLongValue(ITimestamp time, ISeverity severity, String status,
            INumericMetaData metaData, long[] values) {
        return new LongValue(time, severity, status, metaData, values);
    }

    /**
     * Create instance of {@link IEnumeratedValue}.
     * 
     * @param time Time stamp
     * @param severity Severity descriptor
     * @param status Status string.
     * @param metaData Enumerated meta data.
     * @param values The actual values.
     * @return Instance of IEnumValue.
     */
    public static IEnumeratedValue createEnumeratedValue(ITimestamp time, ISeverity severity, String status,
            IEnumeratedMetaData metaData, int[] values) {
        return new EnumeratedValue(time, severity, status, metaData, values);
    }

    /**
     * Create instance of {@link IStringValue}.
     * 
     * @param time Time stamp
     * @param severity Severity descriptor
     * @param status Status string.
     * @param values The actual values.
     * @return Instance of IStringValue.
     */
    public static IStringValue createStringValue(ITimestamp time, ISeverity severity, String status, String[] values) {
        return new StringValue(time, severity, status, values);
    }
}
