/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.csstudio.data.values.internal.EnumeratedValue;
import org.csstudio.data.values.internal.Messages;
import org.csstudio.data.values.internal.StringValue;

/**
 * Helper for decoding the data in a {@link IValue}, mostly for display purposes.
 * 
 * @author Kay Kasemir
 */
public final class ValueUtil {
    private ValueUtil() {
    }

    /**
     * Measures the size of the data in number of elements.
     * 
     * @param value the value, which provides the data that is measured
     * @return Array length of the value. <code>1</code> for scalars
     */
    public static int getSize(IValue value) {
        if (value instanceof IDoubleValue) {
            return ((IDoubleValue) value).getValues().length;
        } else if (value instanceof ILongValue) {
            return ((ILongValue) value).getValues().length;
        } else if (value instanceof IEnumeratedValue) {
            return ((IEnumeratedValue) value).getValues().length;
        } else {
            return 1;
        }
    }

    /**
     * Try to get a double number from the Value.
     * <p>
     * Some applications only deal with numeric data, so they want to interpret integer, enum and double values all the
     * same.
     * 
     * @param value The value to decode.
     * @return A double, or <code>Double.NaN</code> in case the value type does not decode into a number, or
     *         <code>Double.NEGATIVE_INFINITY</code> if the value's severity indicates that there happens to be no
     *         useful value.
     */
    public static double getDouble(IValue value) {
        return getDouble(value, 0);
    }

    /**
     * Try to get a double-typed array element from the Value.
     * 
     * @param value The value to decode.
     * @param index The array index, 0 ... getSize()-1.
     * @see #getSize(IValue)
     * @see #getDouble(IValue)
     * @return A double, or <code>Double.NaN</code> in case the value type does not decode into a number, or
     *         <code>Double.NEGATIVE_INFINITY</code> if the value's severity indicates that there happens to be no
     *         useful value.
     */
    public static double getDouble(IValue value, int index) {
        if (value.getSeverity().hasValue()) {
            if (value instanceof IDoubleValue) {
                return ((IDoubleValue) value).getValues()[index];
            } else if (value instanceof ILongValue) {
                return ((ILongValue) value).getValues()[index];
            } else if (value instanceof IEnumeratedValue) {
                return ((IEnumeratedValue) value).getValues()[index];
            } else {
                // Cannot decode that sample type as a number.
                return Double.NaN;
            }
        } else {
            // else: Sample carries no value other than stat/sevr
            return Double.NEGATIVE_INFINITY;
        }
    }

    /**
     * Try to get a double-typed array from the Value.
     * 
     * @param value The value to decode.
     * @see #getSize(IValue)
     * @see #getDouble(IValue)
     * @return A double array, or an empty double array in case the value type does not decode into a number, or if the
     *         value's severity indicates that there happens to be no useful value.
     */
    public static double[] getDoubleArray(IValue value) {
        if (value.getSeverity().hasValue()) {
            if (value instanceof IDoubleValue) {
                return ((IDoubleValue) value).getValues();
            } else if (value instanceof ILongValue) {
                double[] result = new double[((ILongValue) value).getValues().length];
                int i = 0;
                for (long l : ((ILongValue) value).getValues()) {
                    result[i] = l;
                    i++;
                }
                return result;
            } else if (value instanceof IEnumeratedValue) {
                double[] result = new double[((IEnumeratedValue) value).getValues().length];
                int i = 0;
                for (int l : ((IEnumeratedValue) value).getValues()) {
                    result[i] = l;
                    i++;
                }
                return result;
            }

            // else:
            // Cannot decode that sample type as a number.
            return new double[0];
        }
        // else: Sample carries no value other than stat/sevr
        return new double[0];
    }

    /**
     * Try to get an info string from the Value.
     * <p>
     * For numeric values, which is probably the vast majority of all values, this is the severity and status
     * information.
     * <p>
     * For 'enum' type values, <code>getDouble()</code> will return the numeric value, and <code>getInfo()</code>
     * returns the associated enumeration string, appended to a possible severity/status text.
     * <p>
     * For string type values, this is the string value and a possible severity/status text, while
     * <code>getDouble()</code> will return <code>NaN</code>.
     * 
     * 
     * @param value The value to decode.
     * @return The info string, never <code>null</code>.
     */
    public static String getInfo(final IValue value) {
        String info = null;
        String valTxt = null;
        final String sevr = value.getSeverity().toString();
        final String stat = value.getStatus();
        if (sevr.length() > 0 || stat.length() > 0) {
            info = sevr + Messages.SEVERITY_STATUS_SEPARATOR + stat;
        }
        if (value instanceof EnumeratedValue) {
            valTxt = ((EnumeratedValue) value).format();
        } else if (value instanceof StringValue) {
            valTxt = ((IStringValue) value).getValue();
        }
        if (valTxt != null) {
            // return info appended to value
            if (info == null) {
                return valTxt;
            }
            return valTxt + Messages.SEVERITY_STATUS_SEPARATOR + info;
        }
        if (info == null) {
            return Messages.EMPTY_STRING;
        }
        return info;
    }

    /**
     * Formats the severity and status of the value.
     * 
     * @param value the value which provides the severity and status
     * @return Non-<code>null</code> String for the value and its severity/status. Does not include the time stamp.
     */
    public static String formatValueAndSeverity(final IValue value) {
        if (value == null) {
            return Messages.EMPTY_STRING;
        }
        String v = value.format();
        if (value.getSeverity().isOK()) {
            return v;
        }
        return v + Messages.VALUE_SEVERITY_STATUS_SEPARATOR + value.getSeverity().toString()
                + Messages.SEVERITY_STATUS_SEPARATOR + value.getStatus() + Messages.SEVERITY_STATUS_END;
    }

    /**
     * Converts the given value into a string representation. For string values, returns the value. For numeric (double
     * and long) values, returns a non-localised string representation. Double values use a point as the decimal
     * separator. For other types of values, the value's {@link IValue#format()} method is called and its result
     * returned.
     * 
     * @param value the value.
     * @return a string representation of the value.
     */
    @SuppressWarnings("nls")
    public static String getString(final IValue value) {
        if (value instanceof IStringValue) {
            return ((IStringValue) value).getValue();
        } else if (value instanceof IDoubleValue) {
            IDoubleValue idv = (IDoubleValue) value;
            double dv = idv.getValue();
            if (Double.isNaN(dv)) {
                return Messages.NAN;
            } else if (Double.isInfinite(dv)) {
                return Messages.INFINITY;
            }
            int precision = ((INumericMetaData) idv.getMetaData()).getPrecision();
            DecimalFormatSymbols dcf = new DecimalFormatSymbols(Locale.US);
            dcf.setDecimalSeparator('.');
            DecimalFormat format = new DecimalFormat("0.#", dcf);
            format.setMinimumFractionDigits(precision);
            format.setMaximumFractionDigits(precision);
            return format.format(dv);
        } else if (value instanceof ILongValue) {
            ILongValue lv = (ILongValue) value;
            return Long.toString(lv.getValue());
        } else {
            return (value == null) ? Messages.EMPTY_STRING : value.format();
        }
    }
}
