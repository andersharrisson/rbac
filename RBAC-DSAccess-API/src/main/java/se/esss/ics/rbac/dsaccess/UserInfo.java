/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess;

import java.io.Serializable;

/**
 * <code>UserInfo</code> represents an object that provides information about a single user. The object provides methods
 * to return all information required by the management tools. If some pieces of data are unavailable, the
 * implementation may return null.
 * <p>
 * When different user info instances are compared the last names should be compared first. If equal, first names should
 * be compared. If equal the usernames should be compared.
 * </p>
 * 
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public interface UserInfo extends Serializable, Comparable<UserInfo> {

    /**
     * Returns the username of the user this <code>UserInfo</code> represents.
     * 
     * @return the username of the user (e.g. jdoe)
     */
    String getUsername();

    /**
     * Returns the surname of the user this <code>UserInfo</code> represents.
     * 
     * @return the last name of the user (e.g. Doe)
     */
    String getLastName();

    /**
     * Returns the first name of the user this <code>UserInfo</code> represents.
     * 
     * @return the first name of the user (e.g. John)
     */
    String getFirstName();

    /**
     * Returns the middle name of the user this <code>UserInfo</code> represents.
     * 
     * @return the middle name of the user (e.g. Jim)
     */
    String getMiddleName();

    /**
     * Returns the name of the group this user belongs to.
     * 
     * @return the name of the group that this user belongs to (e.g. MCS)
     */
    String getGroup();

    /**
     * Returns the email address of the user this <code>UserInfo</code> represents.
     * 
     * @return the e-mail address of the user (e.g. john.doe@lab.net)
     */
    String getEMail();

    /**
     * Returns the phone number of the user this <code>UserInfo</code> represents.
     * 
     * @return the phone number of the user
     */
    String getPhoneNumber();

    /**
     * Returns the location of the user this <code>UserInfo</code> represents.
     * 
     * @return the location of the user (e.g. Bldg 2, Room 235)
     */
    String getLocation();
}
