/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
/**
 * Classes in this package provide an interface to the directory service.
 * They allow authentication of a user and retrieving information about the user, 
 * which is required by the RBAC management tools.
 * Package defines two interfaces: {@link se.esss.ics.rbac.dsaccess.DirectoryServiceAccess} 
 * and {@link se.esss.ics.rbac.dsaccess.UserInfo}. Their implementation is not part of this package.
 * <p>
 * {@link se.esss.ics.rbac.dsaccess.DirectoryServiceAccess} declares methods for logging in, logging out
 * and retrieving information about users, all using directory service.
 * {@link se.esss.ics.rbac.dsaccess.UserInfo} declares methods for retrieving information about a single user.
 * </p>
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
package se.esss.ics.rbac.dsaccess;
