/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.SearchCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultAttribute;
import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.BindRequest;
import org.apache.directory.api.ldap.model.message.BindResponse;
import org.apache.directory.api.ldap.model.message.LdapResult;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.message.SearchRequest;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.dsaccess.ldap.LDAPConnectionFactory;
import se.esss.ics.rbac.dsaccess.ldap.LDAPDirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.ldap.LDAPProperties;

/**
 * 
 * <code>LDAPDirectoryServiceAccessTest</code> tests the behaviour of the {@link LDAPDirectoryServiceAccess}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class LDAPDirectoryServiceAccessTest {

    private static abstract class TestSearchCursor implements SearchCursor {
        private int noHasBeenRead = 0;

        @Override
        public boolean next() throws LdapException, CursorException {
            noHasBeenRead++;
            if (noHasBeenRead == 1) {
                return getEntry() != null;
            }
            return false;
        }

        @Override
        public boolean isEntry() {
            try {
                return getEntry() != null;
            } catch (LdapException e) {
                return false;
            }
        }
    }

    private static abstract class TestSearchCursor2 implements SearchCursor {
        private int noHasBeenRead = 0;

        @Override
        public boolean next() throws LdapException, CursorException {
            noHasBeenRead++;
            if (noHasBeenRead < 3) {
                return getEntry() != null;
            }
            return false;
        }

        @Override
        public boolean isEntry() {
            try {
                return getEntry() != null;
            } catch (LdapException e) {
                return false;
            }
        }

        @Override
        public Entry getEntry() throws LdapException {
            Entry testEntry = new DefaultEntry();
            if (noHasBeenRead == 1) {
                testEntry.add(TEST_ATTRIBUTES_JENNY);
            } else {
                testEntry.add(TEST_ATTRIBUTES);
            }
            return testEntry;
        }
    }

    private static class SearchRequestMatcher extends ArgumentMatcher<SearchRequest> {
        private String search;

        public SearchRequestMatcher(String search) {
            this.search = search;
        }

        @Override
        public boolean matches(Object argument) {
            if (!(argument instanceof SearchRequest)) {
                return false;
            }
            String requestFilter = ((SearchRequest) argument).getFilter().toString();
            return requestFilter.equals(search);

        }
    }

    private static final String FIELD_USERNAME;
    private static final String FIELD_FIRST_NAME;
    private static final String FIELD_LAST_NAME;
    private static final String FIELD_GROUP;
    private static final String FIELD_EMAIL;
    private static final String USERNAME_SEARCH;
    private static final String LASTNAME_SEARCH;
    private static final String FIRSTNAME_SEARCH;
    private static final String GROUP_FILTER;

    static {
        Properties properties = new Properties();
        try (InputStream stream = LDAPDirectoryServiceAccess.class.getClassLoader().getResourceAsStream(
                "ldap.properties")) {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FIELD_USERNAME = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_USERNAME);
        FIELD_FIRST_NAME = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_FIRST_NAME);
        FIELD_LAST_NAME = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_LAST_NAME);
        FIELD_GROUP = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_GROUP);
        FIELD_EMAIL = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_EMAIL);

        String object = properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_OBJECT) + ")";
        USERNAME_SEARCH = "(&" + properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_USERNAME) + object;
        FIRSTNAME_SEARCH = "(&" + properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_FIRST_NAME) + object;
        LASTNAME_SEARCH = "(&" + properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_LAST_NAME) + object;

        GROUP_FILTER = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_GROUP_FILTER);
    }

    private static final String TEST_USERNAME = "johndoe";
    private static final String TEST_FIRST_NAME = "John";
    private static final String TEST_LAST_NAME = "Doe";
    private static final String TEST_GROUP = "TestGroup";
    private static final String TEST_EMAIL = "john.doe@test.com";

    private static final String FAKE_USERNAME = "janedoe";
    private static final String FAKE_FIELD = "fakeField";

    private static final Attribute[] TEST_ATTRIBUTES = new Attribute[] {
            new DefaultAttribute(FIELD_USERNAME, TEST_USERNAME),
            new DefaultAttribute(FIELD_FIRST_NAME, TEST_FIRST_NAME),
            new DefaultAttribute(FIELD_LAST_NAME, TEST_LAST_NAME), new DefaultAttribute(FIELD_GROUP, TEST_GROUP),
            new DefaultAttribute(FIELD_EMAIL, TEST_EMAIL) };

    private static final Attribute[] TEST_ATTRIBUTES_JENNY = new Attribute[] {
            new DefaultAttribute(FIELD_USERNAME, "jenny"), new DefaultAttribute(FIELD_FIRST_NAME, "jenny"),
            new DefaultAttribute(FIELD_LAST_NAME, "jenny"), new DefaultAttribute(FIELD_GROUP, "group"),
            new DefaultAttribute(FIELD_EMAIL, "email") };

    private static final Attribute[] TEST_ATTRIBUTES_JIMMY = new Attribute[] {
            new DefaultAttribute(FIELD_USERNAME, "jimmy"), new DefaultAttribute(FIELD_FIRST_NAME, "jimmy"),
            new DefaultAttribute(FIELD_LAST_NAME, "jimmy"),
            new DefaultAttribute(FIELD_GROUP, "group", "my_group," + GROUP_FILTER),
            new DefaultAttribute(FIELD_EMAIL, "email") };

    private DirectoryServiceAccess dsAccess;
    private LdapNetworkConnection connection;
    private LdapNetworkConnection userConnection;

    /**
     * Setup the directory service access and create all the mockups.
     * 
     * @throws Exception on error
     */
    @Before
    public void setUp() throws Exception {
        connection = Mockito.mock(LdapNetworkConnection.class);
        Mockito.when(connection.isConnected()).thenReturn(Boolean.TRUE);
        Mockito.when(connection.search(any(SearchRequest.class))).thenAnswer(new Answer<SearchCursor>() {
            @Override
            public SearchCursor answer(InvocationOnMock invocation) throws Throwable {
                SearchRequest request = (SearchRequest) invocation.getArguments()[0];
                String requestFilter = request.getFilter().toString();

                SearchCursor c = Mockito.mock(TestSearchCursor.class);
                if (requestFilter.contains("(sAMAccountName=johndoe)")) {
                    Entry testEntry = new DefaultEntry();
                    testEntry.add(TEST_ATTRIBUTES);
                    Mockito.when(c.getEntry()).thenReturn(testEntry);
                } else if (requestFilter.contains("jenny")) {
                    c = Mockito.mock(TestSearchCursor2.class);
                    Mockito.when(c.getEntry()).thenCallRealMethod();
                } else if (requestFilter.contains("jimmy")) {
                    Entry testEntry = new DefaultEntry();
                    testEntry.add(TEST_ATTRIBUTES_JIMMY);
                    Mockito.when(c.getEntry()).thenReturn(testEntry);
                }
                Mockito.when(c.next()).thenCallRealMethod();
                Mockito.when(c.isEntry()).thenCallRealMethod();
                return c;
            }
        });
        Mockito.doNothing().when(connection).unBind();
        dsAccess = new LDAPDirectoryServiceAccess();

        LDAPConnectionFactory factory = Mockito.mock(LDAPConnectionFactory.class);
        Mockito.when(factory.getDefaultConnection()).thenReturn(connection);
        userConnection = Mockito.mock(LdapNetworkConnection.class);
        Mockito.when(userConnection.isConnected()).thenReturn(Boolean.TRUE);
        BindResponse response = Mockito.mock(BindResponse.class);
        LdapResult result = Mockito.mock(LdapResult.class);
        Mockito.when(result.getResultCode()).thenReturn(ResultCodeEnum.SUCCESS);
        Mockito.when(response.getLdapResult()).thenReturn(result);
        Mockito.when(userConnection.bind(any(BindRequest.class))).thenReturn(response);
        Mockito.when(factory.createNewConnection()).thenReturn(userConnection);

        Field field = LDAPDirectoryServiceAccess.class.getDeclaredField("connectionFactory");
        field.setAccessible(true);
        field.set(dsAccess, factory);
        field = LDAPDirectoryServiceAccess.class.getDeclaredField("searchPageSize");
        field.setAccessible(true);
        //brute force search
        field.set(dsAccess, Integer.valueOf(0));        
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#login(char[], char[])} method with invalid parameters.
     * 
     * @throws DirectoryServiceAccessException on error
     */
    @Test
    public void testLoginWithNullParameters() throws DirectoryServiceAccessException {
        UserInfo info = dsAccess.login(null, null);
        assertNull("Null user is returned if username and or password are null.", info);

        info = dsAccess.login(new char[0], new char[0]);
        assertNull("Null user is returned if username and or password are null.", info);

        info = dsAccess.login(null, "pass".toCharArray());
        assertNull("Null user is returned if username and or password are null.", info);

        info = dsAccess.login(new char[0], "pass".toCharArray());
        assertNull("Null user is returned if username and or password are null.", info);

        info = dsAccess.login("user".toCharArray(), null);
        assertNull("Null user is returned if username and or password are null.", info);

        info = dsAccess.login("user".toCharArray(), new char[0]);
        assertNull("Null user is returned if username and or password are null.", info);
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#login(char[], char[])} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testLogin() throws Exception {
        UserInfo info = dsAccess.login("someone".toCharArray(), "pass".toCharArray());
        assertNull("Login not successful, because user someon does not exist.", info);
        info = dsAccess.login("johndoe".toCharArray(), "pass".toCharArray());
        assertNotNull("Login successful", info);
        Mockito.verify(userConnection, Mockito.times(1)).bind(any(BindRequest.class));
        Mockito.verify(userConnection, Mockito.times(1)).close();
        Mockito.verify(userConnection, Mockito.times(1)).unBind();

        Mockito.when(userConnection.bind(any(BindRequest.class))).thenThrow(
                new LdapException("AcceptSecurityContext error"));
        info = dsAccess.login("jenny".toCharArray(), "pass".toCharArray());
        assertNull(info);

        Mockito.reset(userConnection);
        Mockito.when(userConnection.bind(any(BindRequest.class))).thenThrow(new LdapException("FooBar"));
        try {
            info = dsAccess.login("jenny".toCharArray(), "pass".toCharArray());
            fail("Exception should occur");
        } catch (DirectoryServiceAccessException e) {
            assertEquals("Exception expected", "org.apache.directory.api.ldap.model.exception.LdapException: FooBar",
                    e.getMessage());
        }
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#logout(char[])} method.
     */
    @Test
    public void testLogout() {
        assertTrue("Logout should always return true.", dsAccess.logout(TEST_USERNAME.toCharArray()));
        assertTrue("Logout should always return true.", dsAccess.logout(FAKE_USERNAME.toCharArray()));
        assertTrue("Logout should always return true.", dsAccess.logout(null));
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUserInfo(char[])} with invalid parameter.
     */
    @Test
    public void testGetUserInfoInvalidParameters() {
        // NULL parameters.
        try {
            dsAccess.getUserInfo(null);
            fail("Searching for null username should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username must not be null or empty.", e.getMessage());
        }

        try {
            dsAccess.getUserInfo(new char[0]);
            fail("Searching for null username should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username must not be null or empty.", e.getMessage());
        }
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUserInfo(char[])} method.
     * 
     * @throws DirectoryServiceAccessException on error
     */
    @Test
    public void testGetUserInfo() throws DirectoryServiceAccessException {
        // Fake parameters.
        UserInfo info = dsAccess.getUserInfo(FAKE_USERNAME.toCharArray());
        assertNull("Searching for nonexistent username should return null.", info);

        // Real parameters.
        info = dsAccess.getUserInfo(TEST_USERNAME.toCharArray());
        assertNotNull("Returned user info should not be null.", info);
        assertEquals(TEST_USERNAME, info.getUsername());
        assertEquals(TEST_FIRST_NAME, info.getFirstName());
        assertEquals(TEST_LAST_NAME, info.getLastName());
        assertEquals(TEST_GROUP, info.getGroup());
        assertEquals(TEST_EMAIL, info.getEMail());

        try {
            dsAccess.getUserInfo("jenny".toCharArray());
            fail("Exception expected, multiple entries found");
        } catch (DirectoryServiceAccessException e) {
            assertEquals("Exception expected", "Found more than one user with username 'jenny'.", e.getMessage());
        }

        info = dsAccess.getUserInfo("jimmy".toCharArray());
        assertNotNull("Returned user info should not be null.", info);
        assertEquals("jimmy", info.getUsername());
        assertEquals("jimmy", info.getFirstName());
        assertEquals("jimmy", info.getLastName());
        assertEquals("my_group," + GROUP_FILTER, info.getGroup());
        assertEquals("email", info.getEMail());
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUserField(char[], String)} with invalid parameters.
     */
    @Test
    public void testGetUserFieldWithInvalidParameters() {
        // NULL parameters.
        try {
            dsAccess.getUserField(null, FIELD_USERNAME);
            fail("Searching for null username should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username and field name must not be null or empty.", e.getMessage());
        }

        // NULL parameters.
        try {
            dsAccess.getUserField(new char[0], FIELD_USERNAME);
            fail("Searching for null username should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username and field name must not be null or empty.", e.getMessage());
        }

        try {
            dsAccess.getUserField(TEST_USERNAME.toCharArray(), null);
            fail("Searching for null field name should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username and field name must not be null or empty.", e.getMessage());
        }

        try {
            dsAccess.getUserField(TEST_USERNAME.toCharArray(), "");
            fail("Searching for null field name should throw an exception.");
        } catch (DirectoryServiceAccessException e) {
            assertEquals(e.getMessage(), "Username and field name must not be null or empty.", e.getMessage());
        }
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUserField(char[], String)} method.
     * 
     * @throws DirectoryServiceAccessException on error
     */
    @Test
    public void testGetUserField() throws DirectoryServiceAccessException {
        // Fake parameters.
        String value = dsAccess.getUserField(FAKE_USERNAME.toCharArray(), FIELD_USERNAME);
        assertNull("Searching for nonexistent username should produce null result.", value);

        value = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FAKE_FIELD);
        assertNull("Searching for nonexistent field should produce null result.", value);

        String username = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FIELD_USERNAME);
        assertEquals("Expected username " + TEST_USERNAME + ", but received " + username, TEST_USERNAME, username);
        String firstName = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FIELD_FIRST_NAME);
        assertEquals("Expected first name " + TEST_FIRST_NAME + ", but received " + firstName, TEST_FIRST_NAME,
                firstName);
        String lastName = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FIELD_LAST_NAME);
        assertEquals("Expected last name " + TEST_LAST_NAME + ", but received " + lastName, TEST_LAST_NAME, lastName);
        String group = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FIELD_GROUP);
        assertEquals("Expected group " + TEST_GROUP + ", but received " + group, TEST_GROUP, group);
        String eMail = dsAccess.getUserField(TEST_USERNAME.toCharArray(), FIELD_EMAIL);
        assertEquals("Expected email " + TEST_EMAIL + ", but received " + eMail, TEST_EMAIL, eMail);

        try {
            dsAccess.getUserField("jenny".toCharArray(), FIELD_USERNAME);
            fail("Exception expected, multiple entries found");
        } catch (DirectoryServiceAccessException e) {
            assertEquals("Exception expected", "Found more than one user with username 'jenny'.", e.getMessage());
        }
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUsers(String, boolean, boolean, boolean)} for general behaviour.
     * 
     * @throws DirectoryServiceAccessException on error
     */
    @Test
    public void testGetUsers() throws DirectoryServiceAccessException {
        UserInfo[] infos = dsAccess.getUsers(null, true, true, true);
        assertEquals("No user infos should be returned", 0, infos.length);

        infos = dsAccess.getUsers("", true, true, true);
        assertEquals("No user infos should be returned", 0, infos.length);

        infos = dsAccess.getUsers("johndoe", true, true, true);
        assertEquals("One user info should be returned", 1, infos.length);
        assertNotNull("Returned user info should not be null", infos[0]);
        assertEquals("Username should match johndoe", "johndoe", infos[0].getUsername());

        infos = dsAccess.getUsers("jenny", true, true, true);
        assertEquals("Two users should be returned", 2, infos.length);
        assertEquals("One username should match jenny", "johndoe", infos[0].getUsername());
        assertEquals("Second username should match johndoe", "jenny", infos[1].getUsername());
    }

    /**
     * Test {@link LDAPDirectoryServiceAccess#getUsers(String, boolean, boolean, boolean)} if the search method on the
     * LDAP connection is called appropriate number of times depending on the parameters.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetUsersSearchVariety() throws Exception {
        dsAccess.getUsers(null, true, true, true);
        // no search should happen
        Mockito.verify(connection, Mockito.never()).search(any(SearchRequest.class));

        dsAccess.getUsers("*", true, true, true);
        // one search should happen
        Mockito.verify(connection, Mockito.times(1)).search(any(SearchRequest.class));
        Mockito.verify(connection, Mockito.times(1)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(USERNAME_SEARCH, "*"))));

        dsAccess.getUsers("johndoe", true, false, false);
        // one more search should happen
        Mockito.verify(connection, Mockito.times(2)).search(any(SearchRequest.class));
        Mockito.verify(connection, Mockito.times(1)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(USERNAME_SEARCH, "johndoe"))));

        dsAccess.getUsers("johndoe", false, true, false);
        // one more search should happen
        Mockito.verify(connection, Mockito.times(3)).search(any(SearchRequest.class));
        Mockito.verify(connection, Mockito.times(1)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(FIRSTNAME_SEARCH, "johndoe"))));

        dsAccess.getUsers("johndoe", false, false, false);
        // no new search should happen
        Mockito.verify(connection, Mockito.times(3)).search(any(SearchRequest.class));

        dsAccess.getUsers("johndoe", true, true, false);
        // two new searches should happen
        Mockito.verify(connection, Mockito.times(5)).search(any(SearchRequest.class));
        Mockito.verify(connection, Mockito.times(2)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(USERNAME_SEARCH, "johndoe"))));
        Mockito.verify(connection, Mockito.times(2)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(FIRSTNAME_SEARCH, "johndoe"))));

        dsAccess.getUsers("johndoe", true, true, true);
        // three new searches should happen
        Mockito.verify(connection, Mockito.times(8)).search(any(SearchRequest.class));
        Mockito.verify(connection, Mockito.times(3)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(USERNAME_SEARCH, "johndoe"))));
        Mockito.verify(connection, Mockito.times(3)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(FIRSTNAME_SEARCH, "johndoe"))));
        Mockito.verify(connection, Mockito.times(1)).search(
                Matchers.argThat(new SearchRequestMatcher(MessageFormat.format(LASTNAME_SEARCH, "johndoe"))));
    }

    /**
     * Close the directory service access.
     */
    @After
    public void tearDown() {
        ((LDAPDirectoryServiceAccess) dsAccess).close();
    }
}
