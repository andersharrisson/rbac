/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;

/**
 *
 * <code>LDAPProperties</code> provides the properties required by the {@link LDAPDirectoryServiceAccess} to establish
 * connection and perform searches.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public final class LDAPProperties implements Serializable {

    private static final long serialVersionUID = 1131704770482281672L;

    // LDAP properties keys for default authentication.
    public static final String KEY_PRIMARY_HOST = "rbac.ldapPrimaryHost";
    public static final String KEY_PRIMARY_PORT = "rbac.ldapPrimaryPort";
    public static final String KEY_SECONDARY_HOST = "rbac.ldapSecondaryHost";
    public static final String KEY_SECONDARY_PORT = "rbac.ldapSecondaryPort";
    public static final String KEY_SECURITY_PRINCIPAL = "rbac.ldapSecurityPrincipal";
    public static final String KEY_SECURITY_CREDENTIALS = "rbac.ldapSecurityCredentials";
    public static final String KEY_SECURITY_METHOD = "rbac.ldapSecurityMethod";
    public static final String KEY_LDAP_TIMEOUT = "rbac.ldapTimeout";
    // LDAP properties keys for search parameters.
    public static final String KEY_SEARCH_NAME = "rbac.ldapSearchName";
    public static final String KEY_SEARCH_FILTER_USERNAME = "rbac.ldapSearchFilterUsername";
    public static final String KEY_SEARCH_FILTER_FIRST_NAME = "rbac.ldapSearchFilterFirstName";
    public static final String KEY_SEARCH_FILTER_LAST_NAME = "rbac.ldapSearchFilterLastName";
    public static final String KEY_SEARCH_FILTER_OBJECT = "rbac.ldapSearchFilterObject";
    public static final String KEY_PAGE_SIZE = "rbac.ldapSearchPageSize";
    // LDAP properties keys for search result attribute names.
    public static final String KEY_ATTRIBUTE_USERNAME = "rbac.ldapAttrUsername";
    public static final String KEY_ATTRIBUTE_FIRST_NAME = "rbac.ldapAttrFirstName";
    public static final String KEY_ATTRIBUTE_MIDDLE_NAME = "rbac.ldapAttrMiddleName";
    public static final String KEY_ATTRIBUTE_LAST_NAME = "rbac.ldapAttrLastName";
    public static final String KEY_ATTRIBUTE_GROUP = "rbac.ldapAttrGroup";
    public static final String KEY_ATTRIBUTE_GROUP_FILTER = "rbac.ldapAttrGroupFilter";
    public static final String KEY_ATTRIBUTE_EMAIL = "rbac.ldapAttrEMail";
    public static final String KEY_ATTRIBUTE_PHONE = "rbac.ldapAttrPhone";
    public static final String KEY_ATTRIBUTE_MOBILE = "rbac.ldapAttrMobile";
    public static final String KEY_ATTRIBUTE_LOCATION = "rbac.ldapAttrLocation";

    private static final String FILE_LDAP_PROPERTIES = "ldap.properties";

    private final Properties properties;

    private static LDAPProperties instance;

    /**
     * Returns the singleton instance of the properties.
     *
     * @return the properties
     * @throws DirectoryServiceAccessException if the properties file was not found
     */
    public static synchronized LDAPProperties getInstance() throws DirectoryServiceAccessException {
        if (instance == null) {
            instance = new LDAPProperties();
        }
        return instance;
    }

    private LDAPProperties() throws DirectoryServiceAccessException {
        properties = new Properties();
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FILE_LDAP_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException e) {
            throw new DirectoryServiceAccessException("Could not read the ldap.properties.", e);
        }
        // override with system properties
        properties.putAll(System.getProperties());
    }

    /**
     * Returns the value of the property for the given key.
     *
     * @param key the key
     * @return the value
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Returns the value of the property for the given key. If the value is null, the default value is returned.
     *
     * @param key the key
     * @param defaultValue the default value
     * @return the value
     */
    public String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }
}
