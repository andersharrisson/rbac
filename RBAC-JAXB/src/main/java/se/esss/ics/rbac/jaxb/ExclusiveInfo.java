/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <code>ExclusiveInfo</code> is a wrapper object for response data for setting exclusive access on a permission. It
 * defines XML structure of the data by using JAXB annotations.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 * 
 */
@XmlRootElement(name = "exclusiveAccess")
@XmlType(propOrder = { "resourceName", "permissionName", "expirationTime" })
public class ExclusiveInfo implements Serializable {
    private static final long serialVersionUID = 6178918049262235740L;

    private String permissionName;
    private String resourceName;
    private long expirationTime;

    /**
     * Constructs a new ExclusiveInfo.
     */
    public ExclusiveInfo() {
    }

    /**
     * Constructs a new ExclusiveInfo.
     * 
     * @param permissionName the name of the permission for which this info is for
     * @param resourceName the name of the resource that the permission belongs to
     * @param expirationTime the expiration time in UTC milliseconds
     */
    public ExclusiveInfo(String permissionName, String resourceName, long expirationTime) {
        this.permissionName = permissionName;
        this.resourceName = resourceName;
        this.expirationTime = expirationTime;
    }

    /**
     * @return the name of the resource that the permission belongs to
     */
    @XmlElement(name = "resource")
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Sets the name of the resource that the permission belongs to.
     * 
     * @param resourceName the name of the resource
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * @return the name of the permission
     */
    @XmlElement(name = "permission")
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * Sets the permission name that this exclusive access belongs to.
     * 
     * @param permissionName the permission name
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    /**
     * @return the expiration time for this exclusive access
     */
    @XmlElement(name = "expirationTime")
    public long getExpirationTime() {
        return expirationTime;
    }

    /**
     * Sets the expiration time for this exclusive access.
     * 
     * @param expirationTime the expiration time
     */
    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }
}
