/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.esss.ics.rbac.jaxb.util.SignatureAdapter;

/**
 *
 * <code>TokenInfo</code> is a wrapper object for token's persisted data and digital signature. It defines the XML
 * structure of the token using JAXB annotations, which allows for simple serialisation of the token.
 *
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 *
 */
@XmlRootElement(name = "token")
@XmlType(propOrder = { "tokenID", "userID", "firstName", "lastName", "creationTime", "expirationTime", "ip",
        "roleNames", "signature" })
public class TokenInfo implements Serializable {
    private static final long serialVersionUID = -6824607680049950392L;

    private String tokenID;
    private String userID;
    private String firstName;
    private String lastName;
    private long creationTime;
    private long expirationTime;
    private String ip;
    private Collection<String> roleNames;
    private byte[] signature;

    /**
     * Construct token info from pieces.
     *
     * @param tokenID the token id
     * @param userID the username
     * @param firstName the user's first name
     * @param lastName the user's last name
     * @param creationTime the time of creation of this token in UTC
     * @param expirationTime the time of expiration of this token in UTC
     * @param ip the ip for which the token is valid
     * @param roleNames the names of the roles represented by this token
     * @param signature the RBAC digital signature
     */
    public TokenInfo(String tokenID, String userID, String firstName, String lastName, long creationTime,
            long expirationTime, String ip, Collection<String> roleNames, byte[] signature) {
        this.tokenID = tokenID;
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationTime = creationTime;
        this.expirationTime = expirationTime;
        this.ip = ip;
        this.roleNames = roleNames;
        this.signature = signature != null ? Arrays.copyOf(signature, signature.length) : null;
    }

    /**
     * Constructs a new TokenInfo.
     */
    public TokenInfo() {
    }

    /**
     * @return the unique tokenID of this token
     */
    @XmlElement(name = "id")
    public String getTokenID() {
        return tokenID;
    }

    /**
     * @param tokenID the tokenID to set
     */
    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    /**
     * @return the userId of the user whom this token has been issued to
     */
    @XmlElement(name = "username")
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * @return the first name of the user whom this token has been issued to
     */
    @XmlElement(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the last name of the user whom this token has been issued to
     */
    @XmlElement(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the last name to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the time of creation of this token
     */
    @XmlElement(name = "creationTime")
    public long getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the time of expiration of this token
     */
    @XmlElement(name = "expirationTime")
    public long getExpirationTime() {
        return expirationTime;
    }

    /**
     * @param expirationTime the expirationTime to set
     */
    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    /**
     * @return the IP of the client for which the token was issued
     */
    @XmlElement(name = "ip")
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the IP of the client for which the token was issued
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the role names that have been used to request the token
     */
    @XmlElement(name = "role")
    @XmlElementWrapper(name = "roles")
    public Collection<String> getRoleNames() {
        return roleNames;
    }

    /**
     * @param roleNames the role names that have been used to request the token
     */
    public void setRoleNames(Collection<String> roleNames) {
        this.roleNames = roleNames;
    }

    /**
     * @return the encryption signature of the token's data
     */
    @XmlElement(name = "signature")
    public byte[] getSignature() {
        return signature != null ? Arrays.copyOf(signature, signature.length) : null;
    }

    /**
     * @param signature the encryption signature of the token's data
     */
    @XmlJavaTypeAdapter(type = byte[].class, value = SignatureAdapter.class)
    public void setSignature(byte[] signature) {
        this.signature = signature != null ? Arrays.copyOf(signature, signature.length) : null;
    }
}
