/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb.util;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * <code>Permissions</code> is a container for {@link NameValuePair}s.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Permissions {

    @XmlElement(name = "permission")
    private List<NameValuePair> permissions;

    /**
     * Construct a new empty Permissions container.
     */
    public Permissions() {
    }

    /**
     * Construct a new Permissions container that contains the provided list of {@link NameValuePair}.
     * 
     * @param permissions the list of permissions
     */
    public Permissions(List<NameValuePair> permissions) {
        this.permissions = permissions;
    }

    /**
     * Set the {@link NameValuePair}s for this container.
     * 
     * @param permissions the list of pairs
     */
    public void setPermissions(List<NameValuePair> permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the list of name value pairs
     */
    public List<NameValuePair> getPermissions() {
        return permissions;
    }
}
