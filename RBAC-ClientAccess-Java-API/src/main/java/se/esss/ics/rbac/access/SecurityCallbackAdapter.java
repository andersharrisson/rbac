/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.security.cert.X509Certificate;
import java.util.Map;

import se.esss.ics.rbac.access.localservice.LocalAuthServiceDetails;

/**
 * 
 * <code>SecurityCallbackAdapter</code> is an adapter that implements all methods from the {@link SecurityCallback}
 * except {@link #getCredentials()}. Use this adapter when you only want to implement some of the methods from the
 * interface.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public abstract class SecurityCallbackAdapter implements SecurityCallback {

    /** Property which defines if the default action is to accept certificate or not */
    public static final String DEFAULT_ACCEPT_CERTIFICATE = "rbac.default.accept.certificate";
    
    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#getCredentials()
     */
    @Override
    public Credentials getCredentials() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#authenticationCompleted(se.esss.ics.rbac.access .Token)
     */
    @Override
    public void authenticationCompleted(Token token) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#authenticationFailed(java.lang.String, java.lang.String,
     * java.lang.Throwable)
     */
    @Override
    public void authenticationFailed(String reason, String username, Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#logoutCompleted(se.esss.ics.rbac.access.Token)
     */
    @Override
    public void logoutCompleted(Token token) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#logoutFailed(java.lang.String, se.esss.ics.rbac.access.Token,
     * java.lang.Throwable)
     */
    @Override
    public void logoutFailed(String reason, Token token, Throwable error) {
    }

    /*
     * @see se.esss.ics.rbac.access.SecurityCallback#authorisationCompleted(se.esss.ics.rbac.access.Token,
     * java.util.Map)
     */
    @Override
    public void authorisationCompleted(Token token, Map<String, Boolean> permissions) {
    }

    /*
     * @see se.esss.ics.rbac.access.SecurityCallback#authorisationFailed(java.lang.String, java.lang.String[],
     * se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void authorisationFailed(String reason, String[] permissions, Token token, Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#rolesLoaded(java.lang.String, java.lang.String[])
     */
    @Override
    public void rolesLoadingCompleted(String username, String[] roles) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#rolesLoadingFailed(java.lang.String, java.lang.String,
     * java.lang.Throwable)
     */
    @Override
    public void rolesLoadingFailed(String reason, String username, Throwable error) {
    }

    /*
     * @see
     * se.esss.ics.rbac.access.SecurityCallback#requestExclusiveAccessCompleted(se.esss.ics.rbac.access.Token,
     * se.esss.ics.rbac.access.ExclusiveAccess)
     */
    @Override
    public void requestExclusiveAccessCompleted(Token token, ExclusiveAccess exclusiveAccess) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#requestExclusiveAccessFailed(java.lang.String, java.lang.String,
     * java.lang.String, se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void requestExclusiveAccessFailed(String reason, String permission, String resource, Token token,
            Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#exclusiveAccessReleased(se.esss.ics.rbac.access .Token,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void releaseExclusiveAccessCompleted(Token token, String permission, String resource) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#releaseExclusiveAccessFailed(java.lang.String, java.lang.String,
     * java.lang.String, se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void releaseExclusiveAccessFailed(String reason, String permission, String resource, Token token,
            Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#tokenRenewalFailed(java.lang.String,
     * se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void tokenRenewalFailed(String reason, Token token, Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#tokenRenewed(se.esss.ics.rbac.access.Token)
     */
    @Override
    public void tokenRenewalCompleted(Token token) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#autoLogoutConfirm(se.esss.ics.rbac.access. Token, int)
     */
    @Override
    public boolean autoLogoutConfirm(Token token, int timeoutInSeconds) {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#acceptCertificate(java.lang.String,
     * java.security.cert.X509Certificate)
     */
    @Override
    public boolean acceptCertificate(String host, X509Certificate certificate) {
        String s = System.getProperty(DEFAULT_ACCEPT_CERTIFICATE);
        return Boolean.parseBoolean(s);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#tokenValidityCheckCompleted(se.esss.ics.rbac.access.Token,
     * boolean)
     */
    @Override
    public void tokenValidationCompleted(Token token, boolean valid) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#tokenValidationFailed(java.lang.String,
     * se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void tokenValidationFailed(String reason, Token token, Throwable error) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#getConnectionDetails()
     */
    @Override
    public LocalAuthServiceDetails getLocalAuthServiceDetails() {
        return new LocalAuthServiceDetails();
    }
}
