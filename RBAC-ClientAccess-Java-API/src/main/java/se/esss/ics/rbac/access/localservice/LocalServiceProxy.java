/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.localservice;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.bind.JAXB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.ConnectionExecutionTask;
import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * <code>LocalServcieProxy</code> interacts with the local authentication service and enables {@link SecurityFacade} to
 * acquire or store RBAC tokens locally. Apart from basic methods used for token manipulation it also provides an
 * utility method {@link LocalServiceProxy#useLocalService(LocalAuthServiceDetails)} that can be used to determine,
 * whether the local authentication service should be used or not.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class LocalServiceProxy {

    private class LongPollingThread extends Thread {

        private static final String EMPTY_STRING = "";

        private boolean running = true;
        private final LocalAuthServiceDetails serviceDetails;
        private HttpURLConnection connection;
        private Token currentToken;

        LongPollingThread(LocalAuthServiceDetails serviceDetails) {
            super("RBAC Long Polling");
            this.serviceDetails = serviceDetails;
        }

        @Override
        public void interrupt() {
            running = false;
            super.interrupt();
            synchronized (this) {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        @Override
        public void run() {
            while (useLocalService(serviceDetails) && running) {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(FacadeUtilities.HEADER_TOKEN_PART, getTokenPart(currentToken));
                String url = createUrlString(NOTIFY_PATH, serviceDetails.getLocalServiceIP(),
                        serviceDetails.getLocalServicePort());
                try {
                    execute(url, FacadeUtilities.GET, null, parameters,
                            new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                                @Override
                                public Boolean execute(HttpURLConnection connection) throws IOException {
                                    synchronized (LongPollingThread.this) {
                                        LongPollingThread.this.connection = connection;
                                    }
                                    int code = connection.getResponseCode();
                                    if (code == FacadeUtilities.CREATED) {
                                        Token token = new Token(JAXB.unmarshal(connection.getInputStream(),
                                                TokenInfo.class));
                                        currentToken = token;
                                        fireEvent(token, true);
                                        return Boolean.TRUE;
                                    } else if (code == FacadeUtilities.OK || code == FacadeUtilities.FORBIDDEN) {
                                        Token token = null;
                                        if (code == FacadeUtilities.OK) {
                                            token = new Token(JAXB.unmarshal(connection.getInputStream(),
                                                    TokenInfo.class));
                                        } else {
                                            token = currentToken;
                                        }
                                        fireEvent(token, false);
                                        currentToken = null;
                                        return Boolean.TRUE;
                                    } else if (connection.getResponseCode() == FacadeUtilities.BAD_REQUEST) {
                                        running = false;
                                        return Boolean.FALSE;
                                    }
                                    return Boolean.FALSE;
                                }
                            });
                } catch (SecurityFacadeException e) {
                    running = false;
                    LOGGER.error(NOTIFY_ERROR, e);
                }
            }
        }

        private String getTokenPart(Token token) {
            if (token != null) {
                String tokenId = new String(token.getTokenID());
                return tokenId.substring(0, tokenId.indexOf('-') - 1);
            }
            return EMPTY_STRING;
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalServiceProxy.class);
    private static final String NOTIFY_ERROR = "Exception occurred while listening for notifications from local "
            + "authentication service.";
    private static final String NOT_REACHABLE = "Cannot reach SSO authentication service on ";
    private static final String TOKEN_PATH = "/sso/token/";
    private static final String NOTIFY_PATH = "/sso/notify";

    private final transient List<LocalServiceListener> listeners;
    private Thread longPollingThread;
    private static Map<String, Date> connectionFailedMap = Collections.synchronizedMap(new HashMap<String, Date>(100));

    /**
     * Constructs local service proxy and initialise list of listeners.
     */
    public LocalServiceProxy() {
        listeners = new CopyOnWriteArrayList<>();
    }

    /**
     * Returns a token from the local authentication service, which corresponds to the provided data or
     * <code>null</code>, if such token does not exist. If there is any problem with communication with local service a
     * {@link SecurityFacadeException} is thrown.
     *
     * @param serviceDetails object which holds informations about local authentication service and client request (IP
     *        address, port number).
     * 
     * @return RBAC token from the local service or <code>null</code>, if a token for the provided parameters could not
     *         be found
     * 
     * @throws SecurityFacadeException if there is a problem with communication with the local service
     */
    public Token getLocalToken(LocalAuthServiceDetails serviceDetails) throws SecurityFacadeException {
        if (serviceDetails == null) {
            return null;
        }
        Map<String, String> parameters = new LinkedHashMap<>(2);
        if (serviceDetails.getRequestIP() != null && serviceDetails.getRequestPort() != 0) {
            parameters.put(FacadeUtilities.HEADER_IP, serviceDetails.getRequestIP());
            parameters.put(FacadeUtilities.HEADER_PORT, String.valueOf(serviceDetails.getRequestPort()));
        }
        String url = createUrlString(TOKEN_PATH, serviceDetails.getLocalServiceIP(),
                serviceDetails.getLocalServicePort());
        TokenInfo tokenInfo = execute(url, FacadeUtilities.GET, null, parameters,
                new ConnectionExecutionTask<TokenInfo, SecurityFacadeException>() {

                    @Override
                    public TokenInfo execute(HttpURLConnection connection) throws IOException {
                        if (connection.getResponseCode() == FacadeUtilities.FOUND) {
                            return JAXB.unmarshal(connection.getInputStream(), TokenInfo.class);
                        }
                        return null;
                    }
                });
        return tokenInfo != null ? new Token(tokenInfo) : null;
    }

    /**
     * Passes the provided token to the local service to be stored. Token can later be acquired back from the service by
     * calling {@link #getLocalToken(LocalAuthServiceDetails)}. If there is any problem with communication with local
     * service a {@link SecurityFacadeException} is thrown.
     * 
     * @param token to be stored by the local authentication service.
     * @param serviceDetails object which holds informations about local authentication service and client request (IP
     *        address, port number).
     * 
     * @throws SecurityFacadeException if there is a problem with communication with the local service.
     */
    public void setLocalToken(Token token, LocalAuthServiceDetails serviceDetails) throws SecurityFacadeException {
        if (token != null && serviceDetails != null) {
            Map<String, String> parameters = new LinkedHashMap<>(3);
            parameters.put("Content-Type", "application/xml");
            if (serviceDetails.getRequestIP() != null && serviceDetails.getRequestPort() != 0) {
                parameters.put(FacadeUtilities.HEADER_IP, serviceDetails.getRequestIP());
                parameters.put(FacadeUtilities.HEADER_PORT, String.valueOf(serviceDetails.getRequestPort()));
            }
            String url = createUrlString(TOKEN_PATH, serviceDetails.getLocalServiceIP(),
                    serviceDetails.getLocalServicePort());
            execute(url, FacadeUtilities.POST, fromToken(token), parameters,
                    new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {

                        @Override
                        public Boolean execute(HttpURLConnection connection) throws IOException {
                            if (connection.getResponseCode() == FacadeUtilities.CREATED) {
                                return Boolean.TRUE;
                            }
                            return Boolean.FALSE;
                        }
                    });
        }
    }

    /**
     * Deletes the provided token from the local store by the local service. If there is any problem with communication
     * with local service a {@link SecurityFacadeException} is thrown.
     * 
     * @param token to be removed from the local store by the local service.
     * @param serviceDetails object which holds informations about local authentication service and client request (IP
     *        address, port number).
     * 
     * @throws SecurityFacadeException if there is a problem with communication with the local service
     */
    public void deleteLocalToken(Token token, LocalAuthServiceDetails serviceDetails) throws SecurityFacadeException {
        if (token != null && serviceDetails != null) {
            Map<String, String> parameters = new LinkedHashMap<>(2);
            if (serviceDetails.getRequestIP() != null && serviceDetails.getRequestPort() != 0) {
                parameters.put(FacadeUtilities.HEADER_IP, serviceDetails.getRequestIP());
                parameters.put(FacadeUtilities.HEADER_PORT, String.valueOf(serviceDetails.getRequestPort()));
            }
            StringBuilder url = new StringBuilder(50);
            url.append(createUrlString(TOKEN_PATH, serviceDetails.getLocalServiceIP(),
                    serviceDetails.getLocalServicePort()));
            url.append(token.getTokenID());
            execute(url.toString(), FacadeUtilities.DELETE, null, parameters,
                    new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {

                        @Override
                        public Boolean execute(HttpURLConnection connection) throws IOException {
                            if (connection.getResponseCode() == FacadeUtilities.OK) {
                                return Boolean.TRUE;
                            }
                            return Boolean.FALSE;
                        }
                    });
        }
    }

    /**
     * Destroy this service and deallocate any resources it holds.
     */
    public synchronized void destroy() {
        if (longPollingThread != null) {
            longPollingThread.interrupt();
        }
        listeners.clear();
    }

    /**
     * Registers listener, which waits for events from local authentication service.
     * 
     * @param serviceDetails the service details to register to
     */
    public synchronized void registerNotifyListener(LocalAuthServiceDetails serviceDetails) {
        if (longPollingThread == null) {
            longPollingThread = new LongPollingThread(serviceDetails);
            longPollingThread.start();
        }
    }

    /**
     * Determines whether the local authentication service should be used or not. Local service should be used only, if
     * the system property {@link RBACProperties#KEY_USE_LOCAL_SERVICE} is set to <code>true</code>.
     * 
     * @param serviceDetails the service details that provide access point details use in connection
     * 
     * @return <code>true</code> if the local authentication service should be used.
     */
    public static boolean useLocalService(LocalAuthServiceDetails serviceDetails) {
        if (RBACProperties.getInstance().useLocalService() && serviceDetails != null) {
            String urlString = new StringBuilder(100).append("http://").append(serviceDetails.getLocalServiceIP())
                    .append(':').append(serviceDetails.getLocalServicePort()).toString();
            Date d = connectionFailedMap.get(urlString);
            if (d != null) {
                if (System.currentTimeMillis() - d.getTime() < 120000) {
                    return false;
                }
                connectionFailedMap.remove(urlString);
            }
            try {
                URL localServiceURL = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) localServiceURL.openConnection();
                connection.setConnectTimeout(2000);
                connection.setRequestMethod("GET");
                connection.connect();
                connection.getResponseCode();
                return true;
            } catch (IOException e) {
                connectionFailedMap.put(urlString, new Date());
                LOGGER.info(NOT_REACHABLE + serviceDetails.getLocalServiceIP());
            }
        }
        return false;
    }

    /**
     * Adds local authentication service listener.
     * 
     * @param listener local authentication service listener
     */
    public void addLocalServiceListener(LocalServiceListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes local authentication service listener.
     * 
     * @param listener local authentication service listener
     */
    public void removeLocalServiceListener(LocalServiceListener listener) {
        listeners.remove(listener);
    }

    /**
     * Notifies all local authentication service listeners about logged in or logged out event.
     * 
     * @param token token
     * @param isLoggedIn true if logged in, otherwise false
     */
    private void fireEvent(Token token, boolean isLoggedIn) {
        for (LocalServiceListener listener : listeners) {
            listener.localServiceEvent(token, isLoggedIn);
        }
    }

    /**
     * Constructs a TokenInfo containing data from the parameter Token. Returned TokenInfo does not have the signature
     * field set.
     * 
     * @param token to be wrapped into a TokenInfo.
     * 
     * @return TokenInfo containing data from the parameter Token.
     */
    private static TokenInfo fromToken(Token token) {
        TokenInfo tokenInfo = new TokenInfo();
        if (token.getCreationDate() != null) {
            tokenInfo.setCreationTime(token.getCreationDate().getTime());
        }
        if (token.getExpirationDate() != null) {
            tokenInfo.setExpirationTime(token.getExpirationDate().getTime());
        }
        tokenInfo.setFirstName(token.getFirstName());
        tokenInfo.setIp(token.getIP());
        tokenInfo.setLastName(token.getLastName());
        tokenInfo.setRoleNames(Arrays.asList(token.getRoles()));
        tokenInfo.setSignature(token.getRBACSignature());
        tokenInfo.setTokenID(String.valueOf(token.getTokenID()));
        tokenInfo.setUserID(token.getUsername());
        return tokenInfo;
    }

    /**
     * Builds URL string from provided values.
     * 
     * @param restMethod name of the rest method
     * @param host host address
     * @param port address port
     * @return created URL as string.
     */
    private static String createUrlString(String restMethod, String host, int port) {
        StringBuilder sb = new StringBuilder(100);
        if (host != null && !host.isEmpty() && port > 0) {
            sb.append("http://").append(host).append(':').append(port).append(restMethod);
        }
        return sb.toString();
    }

    /**
     * Creates a connection to the given URL using the given method and parameters and executes the task using the
     * created connection. If there was an error during execution the error will be wrapped into a
     * {@link SecurityFacadeException}.
     * 
     * @param url the URL to connect to
     * @param method the HTTP request method
     * @param postObject the object which is send over HTTP if method is POST
     * @param parameters the request parameters
     * @param executor the task
     * 
     * @return the task execution result
     * 
     * @throws SecurityFacadeException if there was an error during execution
     */
    private static <T> T execute(String urlString, String method, Object postObject, Map<String, String> parameters,
            ConnectionExecutionTask<T, SecurityFacadeException> task) throws SecurityFacadeException {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            if (method != null) {
                connection.setRequestMethod(method);
            }
            if (parameters != null) {
                for (Map.Entry<String, String> property : parameters.entrySet()) {
                    if (property.getValue() == null) {
                        continue;
                    }
                    connection.setRequestProperty(property.getKey(), property.getValue());
                }
            }
            if (postObject != null && FacadeUtilities.POST.equals(method) && postObject instanceof TokenInfo) {
                connection.setDoOutput(true);
                TokenInfo tokenInfo = (TokenInfo) postObject;
                JAXB.marshal(tokenInfo, connection.getOutputStream());
            }
            return task.execute(connection);
        } catch (IOException e) {
            LOGGER.error("Service connection error (type: "
                    + (e.getCause() == null ? e.getClass().getSimpleName() : e.getCause().getClass().getSimpleName())
                    + "): " + e.getMessage());
            return FacadeUtilities.throwConnectionException(connection, e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
