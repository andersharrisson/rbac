/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.Test;

import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;

/**
 *
 * <code>SecurityFacadeAuthenticationTest</code> tests log in and log out methods of the security facade.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class SecurityFacadeAuthenticationTest extends AbstractSecurityFacadeBase {

    private static final String EXCEPTION_SHOULD_OCCUR     = "Exception should occur";
    private static final String LOGGED_IN_WAS_NEVER_ONCE   = "Logged in was never once";
    private static final String LOGGED_OUT_WAS_CALLED_ONCE = "Logged out was called once";
    private static final String LOGOUT_SUCCESSFUL          = "Logout successful";
    private static final String MESSAGE_SHOULD_MATCH       = "Message should match";
    private static final String TOKENS_SHOULD_BE_THE_SAME  = "Tokens should be the same";

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setUpForAuthentication();
    }

    /**
     * Test authentication when the connection returns proper response.
     *
     * @throws SecurityFacadeException on error
     */
    @Test
    public void testAuthenticateSuccessfully() throws SecurityFacadeException {
        Token token = facade.authenticate();
        assertNotNull("Token should not be null if authentication is successful", token);
        assertEquals("Username should match", info.getUserID(), token.getUsername());
        assertEquals("First name should match", info.getFirstName(), token.getFirstName());
        assertEquals("Last name should match", info.getLastName(), token.getLastName());
        assertEquals("IP should match", info.getIp(), token.getIP());
        assertEquals("Token ID should match", info.getTokenID(), new String(token.getTokenID()));
        assertEquals("Creation date should match", info.getCreationTime(), token.getCreationDate().getTime());
        assertEquals("Expiration date should match", info.getExpirationTime(), token.getExpirationDate().getTime());
        assertArrayEquals("Role names should match", info.getRoleNames().toArray(new String[2]), token.getRoles());
        assertArrayEquals("Signature should match", info.getSignature(), token.getRBACSignature());

        verify(loginConnection, times(1)).setRequestProperty(FacadeUtilities.HEADER_AUTHORISATION,
                "BASIC am9obmRvZTpwYXNz");
        verify(loginConnection, times(1)).setRequestProperty(FacadeUtilities.HEADER_IP, "192.168.1.2");
        verify(loginConnection, times(1)).setRequestProperty(FacadeUtilities.HEADER_ROLES, "Role");

        Token storedToken = facade.getToken(ip);
        assertSame(TOKENS_SHOULD_BE_THE_SAME, token, storedToken);

        assertEquals("Logged in called only once", 1, loggedIn);
        assertEquals("Logged out should not have been called", 0, loggedOut);
        assertSame(TOKENS_SHOULD_BE_THE_SAME, token, loggedInToken);
    }

    /**
     * Test authentication when connection returns invalid response.
     *
     * @throws IOException on error
     * @throws SecurityFacadeException on error
     */
    @Test
    public void testAuthenticateFailure() throws IOException, SecurityFacadeException {
        when(loginConnection.getInputStream()).thenReturn(new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 }));
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Service response error: Error", e.getMessage());
        }

        loginConnection.getErrorStream().reset();
        when(loginConnection.getResponseCode()).thenReturn(400);
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Service responded unexpectedly: Error", e.getMessage());
        }

        loginConnection.getErrorStream().reset();
        when(loginConnection.getInputStream()).thenReturn(null);
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Service responded unexpectedly: Error", e.getMessage());
        }

        assertEquals("Logged in should not have been called", 0, loggedIn);
        assertEquals("Logged out should not have been called", 0, loggedOut);
        assertNull("Token should be null", facade.getToken(ip));
    }

    /**
     * Test logout, when user is the response from connection is OK.
     *
     * @throws Exception on error
     */
    @Test
    public void testLogoutSuccessful() throws Exception {
        Token token = facade.authenticate();
        loggedIn = 0;
        loggedInToken = null;
        when(logoutConnection.getResponseCode()).thenReturn(FacadeUtilities.DELETED);
        boolean logout = facade.logout(ip);
        assertTrue(LOGOUT_SUCCESSFUL, logout);
        assertEquals(LOGGED_OUT_WAS_CALLED_ONCE, 1, loggedOut);
        assertEquals(LOGGED_IN_WAS_NEVER_ONCE, 0, loggedIn);
        assertSame(TOKENS_SHOULD_BE_THE_SAME, token, loggedOutToken);

        setUpForAuthentication();
        token = facade.authenticate();
        loggedIn = 0;
        loggedInToken = null;
        loggedOut = 0;
        loggedOutToken = null;
        when(logoutConnection.getResponseCode()).thenReturn(FacadeUtilities.OK);
        logout = facade.logout(ip);
        assertFalse(LOGOUT_SUCCESSFUL, logout);
        assertEquals(LOGGED_OUT_WAS_CALLED_ONCE, 1, loggedOut);
        assertEquals(LOGGED_IN_WAS_NEVER_ONCE, 0, loggedIn);
        assertSame(TOKENS_SHOULD_BE_THE_SAME, token, loggedOutToken);

        token = facade.getToken(ip);
        assertNull("Facade token should be null", token);

        logout = facade.logout(ip);
        assertFalse("No user logged in, logout should fail", logout);
    }

    /**
     * Test logout, when user is the response from connection is DELETE.
     *
     * @throws Exception on error
     */
    @Test
    public void testLogoutSuccessful2() throws Exception {
        Token token = facade.authenticate();
        loggedIn = 0;
        loggedInToken = null;
        boolean logout = facade.logout(ip);
        assertTrue(LOGOUT_SUCCESSFUL, logout);
        assertEquals(LOGGED_OUT_WAS_CALLED_ONCE, 1, loggedOut);
        assertEquals(LOGGED_IN_WAS_NEVER_ONCE, 0, loggedIn);
        assertSame(TOKENS_SHOULD_BE_THE_SAME, token, loggedOutToken);
        token = facade.getToken(ip);
        assertNull("Facade token should be null", token);

        logout = facade.logout(ip);
        assertFalse("No user logged in, logout should fail", logout);
    }

    /**
     * Test logout when connection responds incorrectly.
     *
     * @throws Exception on error
     */
    @Test
    public void testLogoutFailure() throws Exception {
        Token token = facade.authenticate();
        loggedIn = 0;
        loggedInToken = null;
        when(logoutConnection.getResponseCode()).thenReturn(400);
        ByteArrayInputStream stream = new ByteArrayInputStream("FooBar".getBytes(CHARSET));
        when(logoutConnection.getErrorStream()).thenReturn(stream);
        try {
            facade.logout(ip);
            fail("Logout should fail, due to wrong response code");
        } catch (SecurityFacadeException e) {
            assertTrue("Exception expected", e.getMessage().contains("FooBar"));
        }
        assertEquals("Logged out was never called", 0, loggedOut);
        assertEquals(LOGGED_IN_WAS_NEVER_ONCE, 0, loggedIn);
        Token token2 = facade.getToken(ip);
        assertSame("Facade token should still be the same", token, token2);

        stream.reset();
        when(logoutConnection.getResponseCode()).thenThrow(new IOException("Test"));
        when(logoutConnection.getErrorStream()).thenReturn(stream);
        try {
            facade.logout(ip);
            fail("Logout should fail, due to wrong response code");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Service response error: FooBar", e.getMessage());
            assertEquals("IOException is the cause", IOException.class, e.getCause().getClass());
            assertEquals("IOMessage is correct", "Test", e.getCause().getMessage());
        }
    }

}
