/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;

/**
 * 
 * <code>CredentialsTest</code> tests all {@link Credentials} constructors and methods.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class CredentialsTest {

    /**
     * Test constructor with username and password.
     */
    @Test
    public void testUsernamePassword() {
        Credentials c = new Credentials(new String("username"), new char[] { 'p', 'a', 's', 's' });
        assertEquals("Username should match", new String("username"), c.getUsername());
        assertArrayEquals("Password should match", new char[] { 'p', 'a', 's', 's' }, c.getPassword());
        assertNull("There should be no preferred role", c.getPreferredRole());
        assertNull("There should be no IP", c.getIP());
    }

    /**
     * Test constructor with username, password, and preferred role.
     */
    @Test
    public void testUsernamePasswordRole() {
        Credentials c = new Credentials(new String("username"), new char[] { 'p', 'a', 's', 's' }, new String("role"));
        assertEquals("Username should match", new String("username"), c.getUsername());
        assertArrayEquals("Password should match", new char[] { 'p', 'a', 's', 's' }, c.getPassword());
        assertEquals("Preferred role should match", "role", c.getPreferredRole());
        assertNull("There should be no IP", c.getIP());
    }

    /**
     * Test constructor with username, password, preferred role, and IP.
     */
    @Test
    public void testUsernamePasswordRoleIP() {
        Credentials c = new Credentials(new String("username"), new char[] { 'p', 'a', 's', 's' }, new String("role"),
                new String("192.168.1.2"));
        assertEquals("Username should match", new String("username"), c.getUsername());
        assertArrayEquals("Password should match", new char[] { 'p', 'a', 's', 's' }, c.getPassword());
        assertEquals("Preferred role should match", "role", c.getPreferredRole());
        assertEquals("IP should match", new String("192.168.1.2"), c.getIP());
    }

    /**
     * Test constructor with null parameters.
     */
    @Test
    public void testNull() {
        Credentials c = new Credentials(null, null, null, null);
        assertNull("Username should be null", c.getUsername());
        assertArrayEquals("Password should be empty", new char[0], c.getPassword());
        assertNull("Preferred role should be null", c.getPreferredRole());
        assertNull("IP should be null", c.getIP());
    }

    /**
     * Test if the Credentials instance is immutable.
     */
    @Test
    public void testImmutable() {
        char[] password = new char[] { 'p', 'a', 's', 's' };
        Credentials c = new Credentials(new String("username"), password, new String("role"), 
                new String("192.168.1.2"));

        password[0] = 'b';
        assertArrayEquals("Credentials password should not change", new char[] { 'p', 'a', 's', 's' }, c.getPassword());

        c.getPassword()[0] = 'b';
        assertArrayEquals("Credentials password should not change", new char[] { 'p', 'a', 's', 's' }, c.getPassword());

    }
}
