/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Date;

import javax.xml.bind.JAXB;

import org.junit.Test;

import se.esss.ics.rbac.access.ExclusiveAccess;
import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.jaxb.ExclusiveInfo;

/**
 *
 * <code>SecurityFacadeExclusiveAccessTest</code> tests method related to exclusive access.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class SecurityFacadeExclusiveAccessTest extends AbstractSecurityFacadeBase {

    private static final String EXCEPTION_EXPECTED                   = "Exception expected";
    private static final String EXCEPTION_SHOULD_OCCUR               = "Exception should occur";
    private static final String MESSAGE_SHOULD_MATCH                 = "Message should match";
    private static final String PARAMETER_PERMISSION_MUST_NOT_BE_NULL_OR_EMPTY =
            "Parameter 'permission' must not be null or empty.";
    private static final String PARAMETER_RESOURCE_MUST_NOT_BE_NULL_OR_EMPTY =
            "Parameter 'resource' must not be null or empty.";
    private static final String RESOURCE                             = "resource";
    private static final String SERVICE_RESPONDED_UNEXPECTEDLY_ERROR = "Service responded unexpectedly: Error";

    private HttpURLConnection releaseConnection;
    private HttpURLConnection requestConnection;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setUpForAuthentication();
        facade.authenticate();

        requestConnection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/p1/exclusive"),
                        eq("POST"))).thenReturn(requestConnection);
        when(requestConnection.getResponseCode()).thenReturn(201);

        ExclusiveInfo ei = new ExclusiveInfo("p1", RESOURCE, 10);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(ei, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(requestConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(requestConnection.getErrorStream()).thenReturn(stream2);

        releaseConnection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/p1/exclusive"),
                        eq("DELETE"))).thenReturn(releaseConnection);
        when(releaseConnection.getResponseCode()).thenReturn(204);

        InputStream stream3 = new ByteArrayInputStream("Deleted".getBytes(CHARSET));
        when(releaseConnection.getInputStream()).thenReturn(stream3);

        InputStream stream4 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(releaseConnection.getErrorStream()).thenReturn(stream4);
    }

    /**
     * Test {@link SecurityFacade#requestExclusiveAccess(String, String, int)} with wrong parameters or state.
     *
     * @throws Exception on error
     */
    @Test
    public void testRequestExclusiveAccessFailure() throws Exception {
        try {
            facade.requestExclusiveAccess(ip, null, "p1", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_RESOURCE_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.requestExclusiveAccess(ip, "", "p1", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_RESOURCE_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.requestExclusiveAccess(ip, RESOURCE, null, 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_PERMISSION_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.requestExclusiveAccess(ip, RESOURCE, "", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_PERMISSION_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }

        facade.logout(ip);
        try {
            facade.requestExclusiveAccess(ip, RESOURCE, "p1", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(EXCEPTION_EXPECTED, "RBAC authentication token is missing.", e.getMessage());
        }
    }

    /**
     * Test if exclusive access is properly requested, if the default duration is requested.
     *
     * @throws Exception on error
     */
    @Test
    public void testRequestExclusiveAccessDefaultDuration() throws Exception {
        ExclusiveAccess access = facade.requestExclusiveAccess(ip, RESOURCE, "p1", 0);
        assertNotNull("Exclusive access granted", access);
        assertEquals("Resource name should match", RESOURCE, access.getResource());
        assertEquals("Permission name should match", "p1", access.getPermission());
        assertEquals("Expiration date should match", new Date(10), access.getExpirationDate());
        verify(requestConnection, times(0)).setRequestProperty(FacadeUtilities.HEADER_EXCLUSIVE_DURATION, "600000");
    }

    /**
     * Test {@link SecurityFacade#requestExclusiveAccess(String, String, int)}.
     *
     * @throws Exception on error
     */
    @Test
    public void testRequestExclusiveAccess() throws Exception {
        ExclusiveAccess access = facade.requestExclusiveAccess(ip, RESOURCE, "p1", 10);
        assertNotNull("Exclusive access granted", access);

        verify(requestConnection, times(1)).setRequestProperty(FacadeUtilities.HEADER_EXCLUSIVE_DURATION, "600000");

        when(requestConnection.getResponseCode()).thenReturn(400);
        try {
            facade.requestExclusiveAccess(ip, RESOURCE, "p1", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }

        requestConnection.getErrorStream().reset();
        when(requestConnection.getInputStream()).thenReturn(null);
        try {
            facade.requestExclusiveAccess(ip, RESOURCE, "p1", 10);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

    /**
     * Test {@link SecurityFacade#releaseExclusiveAccess(String, String)} with incorrect parameters or without token.
     *
     * @throws Exception on error
     */
    @Test
    public void testReleaseExclusiveAccessFailure() throws Exception {
        try {
            facade.releaseExclusiveAccess(ip, null, "p1");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_RESOURCE_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.releaseExclusiveAccess(ip, "", "p1");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_RESOURCE_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.releaseExclusiveAccess(ip, RESOURCE, null);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_PERMISSION_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        try {
            facade.releaseExclusiveAccess(ip, RESOURCE, "");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(EXCEPTION_EXPECTED, PARAMETER_PERMISSION_MUST_NOT_BE_NULL_OR_EMPTY, e.getMessage());
        }
        facade.logout(ip);
        try {
            facade.releaseExclusiveAccess(ip, RESOURCE, "p1");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(EXCEPTION_EXPECTED, "RBAC authentication token is missing.", e.getMessage());
        }
    }

    /**
     * Test {@link SecurityFacade#requestExclusiveAccess(String, String, int)}.
     *
     * @throws Exception on error
     */
    @Test
    public void testReleaseExclusiveAccess() throws Exception {
        boolean access = facade.releaseExclusiveAccess(ip, RESOURCE, "p1");
        assertTrue("Exclusive access released", access);

        when(releaseConnection.getResponseCode()).thenReturn(200);
        access = facade.releaseExclusiveAccess(ip, RESOURCE, "p1");
        assertFalse("Exclusive access release not released, because it does not exist", access);

        when(releaseConnection.getResponseCode()).thenReturn(400);
        try {
            facade.releaseExclusiveAccess(ip, RESOURCE, "p1");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
        releaseConnection.getErrorStream().reset();
        releaseConnection.getInputStream().reset();
        when(releaseConnection.getInputStream()).thenReturn(null);
        try {
            facade.releaseExclusiveAccess(ip, RESOURCE, "p1");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

}
