/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXB;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ExclusiveAccess;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>SecurityFacadeAsyncCallsTest</code> tests that asynchronous calls notify the appropriate callback methods when
 * necessary.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SecurityFacadeAsyncCallsTest extends AbstractSecurityFacadeBase {

    private static final String ERROR    = "Error";
    private static final String JOHNDOE  = "johndoe";
    private static final String RESOURCE = "resource";
    private static final String ROLE1    = "Role1";
    private static final String ROLE2    = "Role2";
    private static final String SERVICE_RESPONDED_UNEXPECTEDLY_ERROR = "Service responded unexpectedly: Error";

    private static final long TIMEOUT = 2000;

    private HttpURLConnection authConnection;
    private HttpURLConnection renewTokenConnection;
    private HttpURLConnection getRolesConnection;
    private HttpURLConnection requestExcusiveAccessConnection;
    private HttpURLConnection releaseExcusiveAccessConnection;
    private HttpURLConnection validateTokenConnection;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setUpForAuthentication();
    }

    private void setUpForPermissionCheck() throws Exception {
        HashMap<String, Boolean> permissions = new LinkedHashMap<>();
        permissions.put("p1", Boolean.TRUE);
        permissions.put("p2", Boolean.FALSE);

        StringBuilder sb = new StringBuilder();
        for (String s : permissions.keySet().toArray(new String[permissions.size()])) {
            sb.append(s).append(',');
        }
        authConnection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/" 
                            + URLEncoder.encode(sb.substring(0, sb.length() - 1), "UTF-8")), eq("GET"))).thenReturn(
                                        authConnection);
        when(authConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PermissionsInfo pInfo = new PermissionsInfo(RESOURCE, permissions);
        JAXB.marshal(pInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(authConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(authConnection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForRenewToken() throws Exception {
        setUpForAuthentication();
        renewTokenConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/renew"), eq("POST")))
                .thenReturn(renewTokenConnection);
        when(renewTokenConnection.getResponseCode()).thenReturn(200);

        TokenInfo info = new TokenInfo("tokenID", JOHNDOE, "John", "Doe", System.currentTimeMillis(),
                System.currentTimeMillis() + 30000, "192.168.1.2", Arrays.asList(ROLE1, ROLE2), new byte[] { 1, 2,
                        3, 4 });

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(info, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(renewTokenConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(renewTokenConnection.getErrorStream()).thenReturn(stream2);

    }

    private void setUpForGetRoles() throws Exception {
        getRolesConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/johndoe/role"), eq("GET"))).thenReturn(
                getRolesConnection);
        when(getRolesConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(new RolesInfo(JOHNDOE, Arrays.asList(ROLE1, ROLE2)), out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(getRolesConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(getRolesConnection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForTokenValidation() throws Exception {
        setUpForAuthentication();
        validateTokenConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/isvalid"), eq("GET")))
                .thenReturn(validateTokenConnection);
        when(validateTokenConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(new byte[0]);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(validateTokenConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(validateTokenConnection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForExclusiveAccess() throws Exception {
        setUpForAuthentication();
        facade.authenticate();

        requestExcusiveAccessConnection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/p1/exclusive"),
                        eq("POST"))).thenReturn(requestExcusiveAccessConnection);
        when(requestExcusiveAccessConnection.getResponseCode()).thenReturn(201);

        ExclusiveInfo ei = new ExclusiveInfo("p1", RESOURCE, 10);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(ei, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(requestExcusiveAccessConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(requestExcusiveAccessConnection.getErrorStream()).thenReturn(stream2);

        releaseExcusiveAccessConnection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/p1/exclusive"),
                        eq("DELETE"))).thenReturn(releaseExcusiveAccessConnection);
        when(releaseExcusiveAccessConnection.getResponseCode()).thenReturn(204);

        InputStream stream3 = new ByteArrayInputStream("Deleted".getBytes(CHARSET));
        when(releaseExcusiveAccessConnection.getInputStream()).thenReturn(stream3);

        InputStream stream4 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(releaseExcusiveAccessConnection.getErrorStream()).thenReturn(stream4);
    }

    /**
     * Test is authentication methods are called during asynchronous authentication.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAuthenticationCalls() throws Exception {
        SecurityCallback callback = mock(SecurityCallback.class);
        when(callback.getCredentials()).thenReturn(facade.getDefaultSecurityCallback().getCredentials());
        doNotify(callback).authenticationCompleted(any(Token.class));
        synchronized (callback) {
            facade.authenticate(callback);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).authenticationCompleted(loggedInToken);
        verify(callback, never()).authenticationFailed(any(String.class), any(String.class), any(Throwable.class));

        callback = mock(SecurityCallback.class);
        synchronized (callback) {
            facade.authenticate(callback);
            callback.wait(TIMEOUT);
        }
        verify(callback, never()).authenticationCompleted(any(Token.class));
        verify(callback, never()).authenticationFailed(any(String.class), any(String.class), any(Throwable.class));

        callback = mock(SecurityCallback.class);
        when(callback.getCredentials()).thenReturn(new Credentials("janedoe", "foobar".toCharArray()));
        doNotify(callback).authenticationFailed(any(String.class), any(String.class), any(Throwable.class));
        when(loginConnection.getResponseCode()).thenReturn(400);
        synchronized (callback) {
            facade.authenticate(callback);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).authenticationFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq("janedoe"),
                any(SecurityFacadeException.class));
        verify(callback, never()).authenticationCompleted(any(Token.class));
    }

    /**
     * Tests is authorisation callback methods are called during authorisation.
     * 
     * @throws Exception on error
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testAuthorizationCalls() throws Exception {
        Token token = facade.authenticate();
        setUpForPermissionCheck();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).authorisationCompleted(any(Token.class), anyMap());
        synchronized (callback) {
            facade.hasPermissions(callback, ip, RESOURCE, "p1", "p2");
            callback.wait(TIMEOUT);
        }
        Map<String, Boolean> map = new HashMap<>();
        map.put("p1", Boolean.TRUE);
        map.put("p2", Boolean.FALSE);
        verify(callback, only()).authorisationCompleted(eq(token), eq(map));

        callback = mock(SecurityCallback.class);
        doNotify(callback).authorisationFailed(any(String.class), any(String[].class), any(Token.class),
                any(Throwable.class));
        setUpForPermissionCheck();
        when(authConnection.getResponseCode()).thenReturn(400);
        synchronized (callback) {
            facade.hasPermissions(callback, ip, RESOURCE, "p1", "p2");
            callback.wait(TIMEOUT);
        }
        verify(callback, only()).authorisationFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR),
                eq(new String[] { "p1", "p2" }), eq(token), any(SecurityFacadeException.class));
    }

    /**
     * Test is logout callback methods are called during logout.
     * 
     * @throws Exception on error
     */
    @Test
    public void testLogoutCalls() throws Exception {
        Token token = facade.authenticate();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).logoutCompleted(any(Token.class));
        synchronized (callback) {
            facade.logout(callback, ip);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).logoutCompleted(token);
        verify(callback, times(1)).getLocalAuthServiceDetails();

        setUpForAuthentication();
        token = facade.authenticate();
        callback = mock(SecurityCallback.class);
        doNotify(callback).logoutFailed(any(String.class), any(Token.class), any(Throwable.class));
        when(logoutConnection.getResponseCode()).thenReturn(400);
        logoutConnection.getErrorStream().reset();
        synchronized (callback) {
            facade.logout(callback, ip);
            callback.wait(TIMEOUT);
        }

        verify(callback, times(1)).logoutFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq(token),
                any(SecurityFacadeException.class));

        setUpForAuthentication();
        facade.authenticate();
        facade.logout(ip);
        callback = mock(SecurityCallback.class);
        doNotify(callback).logoutCompleted(any(Token.class));
        synchronized (callback) {
            facade.logout(callback, ip);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).logoutFailed(eq("Logout failed. There is no token."), any(Token.class),
                any(Throwable.class));
    }

    /**
     * Test is renew token callback methods are called during token renewal.
     * 
     * @throws Exception on error
     */
    @Test
    public void testTokenRenewCalls() throws Exception {
        Token token = facade.authenticate();
        SecurityCallback callback = mock(SecurityCallback.class);
        final Token[] newTokens = new Token[1];
        doAnswer(new Answer<SecurityCallback>() {
            @Override
            public SecurityCallback answer(InvocationOnMock invocation) throws Throwable {
                newTokens[0] = (Token) invocation.getArguments()[0];
                synchronized (invocation.getMock()) {
                    invocation.getMock().notifyAll();
                }
                return null;
            }
        }).when(callback).tokenRenewalCompleted(any(Token.class));
        setUpForRenewToken();
        synchronized (callback) {
            facade.renewToken(callback, ip);
            callback.wait(TIMEOUT);
        }

        verify(callback, times(1)).tokenRenewalCompleted(any(Token.class));
        verify(callback, times(1)).getLocalAuthServiceDetails();
        assertArrayEquals("Token IDS should be the same", token.getTokenID(), newTokens[0].getTokenID());
        assertTrue("Token expiration date should be extended",
                token.getExpirationDate().before(newTokens[0].getExpirationDate()));

        setUpForAuthentication();
        token = facade.authenticate();
        callback = mock(SecurityCallback.class);
        doNotify(callback).tokenRenewalFailed(any(String.class), any(Token.class), any(Throwable.class));
        setUpForRenewToken();
        when(renewTokenConnection.getResponseCode()).thenReturn(400);
        renewTokenConnection.getErrorStream().reset();
        synchronized (callback) {
            facade.renewToken(callback, ip);
            callback.wait(TIMEOUT);
        }
        verify(callback, only()).tokenRenewalFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq(token),
                any(SecurityFacadeException.class));
    }

    /**
     * Test if request exclusive access callback methods are called during request exclusive access.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRequestExclusiveAccessCalls() throws Exception {
        Token token = facade.authenticate();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).requestExclusiveAccessCompleted(any(Token.class), any(ExclusiveAccess.class));
        setUpForExclusiveAccess();
        synchronized (callback) {
            facade.requestExclusiveAccess(callback, ip, RESOURCE, "p1", 10);
            callback.wait(TIMEOUT);
        }
        ExclusiveAccess access = new ExclusiveAccess(RESOURCE, "p1", 10);
        verify(callback, only()).requestExclusiveAccessCompleted(eq(token), eq(access));

        setUpForAuthentication();
        token = facade.authenticate();
        callback = mock(SecurityCallback.class);
        doNotify(callback).requestExclusiveAccessFailed(any(String.class), any(String.class), any(String.class),
                any(Token.class), any(Throwable.class));
        setUpForExclusiveAccess();
        when(requestExcusiveAccessConnection.getResponseCode()).thenReturn(400);
        releaseExcusiveAccessConnection.getErrorStream().reset();
        synchronized (callback) {
            facade.requestExclusiveAccess(callback, ip, RESOURCE, "p1", 10);
            callback.wait(TIMEOUT);
        }
        verify(callback, only()).requestExclusiveAccessFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq("p1"),
                eq(RESOURCE), eq(token), any(SecurityFacadeException.class));
    }

    /**
     * Test if release exclusive access callback methods are called during request exclusive access.
     * 
     * @throws Exception on error
     */
    @Test
    public void testReleaseExclusiveAccessCalls() throws Exception {
        Token token = facade.authenticate();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).releaseExclusiveAccessCompleted(any(Token.class), any(String.class), any(String.class));
        setUpForExclusiveAccess();

        synchronized (callback) {
            facade.releaseExclusiveAccess(callback, ip, RESOURCE, "p1");
            callback.wait(TIMEOUT);
        }
        verify(callback, only()).releaseExclusiveAccessCompleted(token, "p1", RESOURCE);

        setUpForAuthentication();
        token = facade.authenticate();
        callback = mock(SecurityCallback.class);
        doNotify(callback).releaseExclusiveAccessFailed(any(String.class), any(String.class), any(String.class),
                any(Token.class), any(Throwable.class));
        setUpForExclusiveAccess();
        when(releaseExcusiveAccessConnection.getResponseCode()).thenReturn(400);
        releaseExcusiveAccessConnection.getErrorStream().reset();
        synchronized (callback) {
            facade.releaseExclusiveAccess(callback, ip, RESOURCE, "p1");
            callback.wait(TIMEOUT);
        }

        verify(callback, only()).releaseExclusiveAccessFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq("p1"),
                eq(RESOURCE), eq(token), any(SecurityFacadeException.class));
    }

    /**
     * Test is roles loading callback methods are called during roles retrieval.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetRolesCalls() throws Exception {
        setUpForGetRoles();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).rolesLoadingCompleted(any(String.class), any(String[].class));
        synchronized (callback) {
            facade.getRolesForUser(callback, ip, JOHNDOE);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).rolesLoadingCompleted(JOHNDOE, new String[] { ROLE1, ROLE2 });
        verify(callback, times(0)).rolesLoadingFailed(any(String.class), any(String.class), any(Throwable.class));

        setUpForGetRoles();
        callback = mock(SecurityCallback.class);
        doNotify(callback).rolesLoadingFailed(any(String.class), any(String.class), any(Throwable.class));
        when(getRolesConnection.getResponseCode()).thenReturn(400);
        getRolesConnection.getErrorStream().reset();

        synchronized (callback) {
            facade.getRolesForUser(callback, ip, JOHNDOE);
            callback.wait(TIMEOUT);
        }

        verify(callback, times(0)).rolesLoadingCompleted(any(String.class), any(String[].class));
        verify(callback, times(1)).rolesLoadingFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq(JOHNDOE),
                any(SecurityFacadeException.class));
    }

    /**
     * Test is roles loading callback methods are called during roles retrieval.
     * 
     * @throws Exception on error
     */
    @Test
    public void testValidateTokenCalls() throws Exception {
        setUpForTokenValidation();
        Token token = facade.authenticate();
        SecurityCallback callback = mock(SecurityCallback.class);
        doNotify(callback).tokenValidationCompleted(any(Token.class), anyBoolean());
        synchronized (callback) {
            facade.isTokenValid(callback, ip);
            callback.wait(TIMEOUT);
        }
        verify(callback, times(1)).tokenValidationCompleted(token, true);
        verify(callback, times(0)).tokenValidationFailed(any(String.class), any(Token.class), any(Throwable.class));

        setUpForTokenValidation();
        callback = mock(SecurityCallback.class);
        doNotify(callback).tokenValidationFailed(any(String.class), any(Token.class), any(Throwable.class));
        when(validateTokenConnection.getResponseCode()).thenReturn(400);
        validateTokenConnection.getErrorStream().reset();

        synchronized (callback) {
            facade.isTokenValid(callback, ip);
            callback.wait(TIMEOUT);
        }

        verify(callback, times(0)).tokenValidationCompleted(any(Token.class), anyBoolean());
        verify(callback, times(1)).tokenValidationFailed(eq(SERVICE_RESPONDED_UNEXPECTEDLY_ERROR), eq(token),
                any(SecurityFacadeException.class));
        facade.logout(ip);
    }

    private static SecurityCallback doNotify(SecurityCallback mock) {
        return doAnswer(new Answer<SecurityCallback>() {
            @Override
            public SecurityCallback answer(InvocationOnMock invocation) throws Throwable {
                synchronized (invocation.getMock()) {
                    invocation.getMock().notifyAll();
                }
                return null;
            }
        }).when(mock);
    }
}
