/*
 * Copyright (C) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.rbac.sso;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 *
 * <code>NullPrintStream</code> is a print stream that does not print anything at all. All print requests are
 * immediately ignored.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class NullPrintStream extends PrintStream {

    /**
     * Constructs a new null print stream.
     */
    public NullPrintStream() {
        super(new OutputStream() {

            @Override
            public void write(int b) throws IOException {
                // do nothing
            }
        });
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#flush()
     */
    @Override
    public void flush() {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#checkError()
     */
    @Override
    public boolean checkError() {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#write(int)
     */
    @Override
    public void write(int b) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#write(byte[], int, int)
     */
    @Override
    public void write(byte[] buf, int off, int len) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(boolean)
     */
    @Override
    public void print(boolean b) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(char)
     */
    @Override
    public void print(char c) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(int)
     */
    @Override
    public void print(int i) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(long)
     */
    @Override
    public void print(long l) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(float)
     */
    @Override
    public void print(float f) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(double)
     */
    @Override
    public void print(double d) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(char[])
     */
    @Override
    public void print(char[] s) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(java.lang.String)
     */
    @Override
    public void print(String s) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#print(java.lang.Object)
     */
    @Override
    public void print(Object obj) {
        // nothing
    }

    /* Methods that do terminate lines */

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println()
     */
    @Override
    public void println() {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(boolean)
     */
    @Override
    public void println(boolean x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(char)
     */
    @Override
    public void println(char x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(int)
     */
    @Override
    public void println(int x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(long)
     */
    @Override
    public void println(long x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(float)
     */
    @Override
    public void println(float x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(double)
     */
    @Override
    public void println(double x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(char[])
     */
    @Override
    public void println(char[] x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(java.lang.String)
     */
    @Override
    public void println(String x) {
        // nothing
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.PrintStream#println(java.lang.Object)
     */
    @Override
    public void println(Object x) {
        // nothing
    }
}
