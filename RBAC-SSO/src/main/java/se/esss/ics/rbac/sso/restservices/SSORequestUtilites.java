/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso.restservices;

import org.glassfish.grizzly.http.server.Request;

/**
 * <code>RequestUtilities</code> is a utility class for extracting specific information from a
 * the HTTP request.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class SSORequestUtilites {

    /** The name of the IP address header */
    public static final String HEADER_NAME_IP = "RBACAddress";
    /** The name of the port header */
    public static final String HEADER_NAME_PORT = "RBACAddressPort";
    /** The name of the current token part header */
    public static final String HEADER_TOKEN_PART = "RBACTokenPart";
    /** Empty string */
    private static final String EMPTY_STRING = "";
    
    /**
     * Default constructor.
     */
    private SSORequestUtilites() {
    }

    /**
     * Retrieves user's IP info from the provided request. User's IP should be stored in a header named
     * {@value #HEADER_NAME_IP}. If this header does not exist, remote address of the request is used instead.
     * 
     * @param request
     *            containing user's IP information
     * 
     * @return user's IP address.
     */
    public static String getIP(Request request) {
        String ipHeader = request.getHeader(HEADER_NAME_IP);
        if (ipHeader != null) {
            return ipHeader;
        }
        return request.getRemoteAddr();
    }

    /**
     * Retrieves user's address port info from the provided request. User's address port should be stored in a header
     * named {@value #HEADER_NAME_PORT}. If this header does not exist, remote address port of the request is used
     * instead.
     * 
     * @param request
     *            containing user's address port information
     * 
     * @return user's address port.
     */
    public static int getPort(Request request) {
        String portHeader = request.getHeader(HEADER_NAME_PORT);
        if (portHeader != null) {
            return Integer.parseInt(portHeader);
        }
        return request.getRemotePort();
    }
    
    /**
     * Retrieves part of the token's id from the provided request. Part of the Token's id should be stored in a header
     * named {@value #HEADER_TOKEN_PART}. If this header does not exist, empty string is used instead.
     * 
     * @param request
     *            containing part of the token's id
     * 
     * @return part of the token's id.
     */
    public static String getCurrentTokenPart(Request request) {
        String tokenPartHeader = request.getHeader(HEADER_TOKEN_PART);
        if (tokenPartHeader != null) {
            return tokenPartHeader;
        }
        return EMPTY_STRING;
    }
}
