package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase3: Single Sign On</code> tests if the single sign on works when using two separate
 * instances of the security facade. 
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class TestCase3 extends Base {
            
    /**
     * Tests if all registered security facade instances receive notifications when one of the 
     * facades authenticates or logs out.
     * 
     */
    @Test
    public void rbac160() throws Exception{
        //create two instance of seurity facade
        final ISecurityFacade facade1 = SecurityFacade.getDefaultInstance();
        facade1.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        facade1.addSecurityFacadeListener(new SecurityFacadeListener() {
            @Override
            public void loggedOut(Token token) {
                synchronized(facade1) {
                    facade1.notifyAll();
                }
            }
            
            @Override
            public void loggedIn(Token token) {                
            }
        });
        facade1.initLocalServiceUsage();
        final ISecurityFacade facade2 = SecurityFacade.newInstance();
        facade2.initLocalServiceUsage();
        
        //register a listener on the second one to be notified when the token changed
        final Token[] mtoken = new Token[1];
        final Token[] ltoken = new Token[1];
        facade2.addSecurityFacadeListener(new SecurityFacadeListener() {
            
            @Override
            public void loggedOut(Token token) {
                ltoken[0] = token;
                synchronized (facade2) {
                    facade2.notifyAll();
                }
            }
            
            @Override
            public void loggedIn(Token token) {
                mtoken[0] = token;
                synchronized (facade2) {
                    facade2.notifyAll();
                }
                
            }
        });
        
        synchronized(facade2) {
            facade1.authenticate();
            facade2.wait();
        }
        
        assertEquals("Tokens from both facades should be identical", facade1.getLocalToken(), facade2.getLocalToken());
        
        Thread.sleep(1000);
        synchronized (facade2) {
            facade1.logout();
            facade2.wait();
        }
        
        assertEquals("The same token should be used during logout as it was used during login",mtoken[0],ltoken[0]);
        assertNull("No token should be available anymore", facade2.getLocalToken());
        
        Thread.sleep(1000);
        synchronized (facade2) {
            facade1.authenticate();
            facade2.wait();
        }   
                
        Thread.sleep(1000);
        synchronized (facade1) {
            facade2.logout();
            facade1.wait();
        }
                
        assertNull("No token should be available in facade 1", facade1.getLocalToken());
        
    }

}
