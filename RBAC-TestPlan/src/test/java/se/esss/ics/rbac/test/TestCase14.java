/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertNotNull;

import javax.swing.JOptionPane;

import org.junit.Test;

import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;

/**
 * 
 * <code>TestCase14: Java Access API</code> verifies that version number of the RBAC services can be obtained.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase14 {

    /**
     * Test if the version number can be read from the service.
     */
    @Test
    public void rbac550() throws SecurityFacadeException {
        String version = SecurityFacade.getDefaultInstance().getRBACVersion();
        assertNotNull("Version should not be null", version);
        JOptionPane.showMessageDialog(null, "The version number of RBAC is " + version + ".");
    }
}
