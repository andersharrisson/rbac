/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import se.esss.ics.rbac.access.AccessDeniedException;
import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase9: Token Validation</code> tests if the token becomes invalid if the user logged out.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase9 extends Base {

    /** The location where the RBAC public key is located */
    private static final String PUBLIC_KEY_LOCATION = "c:/public.key";

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        System.setProperty(RBACProperties.KEY_VERIFY_SIGNATURE, "true");
        System.setProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION, PUBLIC_KEY_LOCATION);
    }

    /**
     * Test if the token becomes invalid after the user has been logged out.
     */
    @Test
    public void rbac190Logout() throws SecurityFacadeException {
        System.setProperty(RBACProperties.KEY_VERIFY_SIGNATURE, "false");
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        Token token = facade.authenticate();
        assertNotNull("Login successful", token);
        boolean logout = facade.logout();
        assertTrue("Logout successful", logout);

        Token newToken = facade.getToken();
        assertNull("No token is currently available", newToken);

        facade.setToken(token.getTokenID());

        try {
            newToken = facade.getToken();
            fail("Exception should occur");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected",
                    "Service responded unexpectedly: Token with ID '" + new String(token.getTokenID())
                            + "' could not be found.", e.getMessage().trim());
        }
    }

    /**
     * Test that token signature validation is successful if proper public key is used.
     */
    @Test
    public void rbac190SignatureValidation() throws Exception {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        Token token = facade.authenticate();
        assertNotNull("Authentication should be successful", token);
        assertEquals("Received token should belong to '" + username + "'.", username, token.getUsername());
    }

    /**
     * Test that token signature validation fails if an invalid public key is used.
     */
    @Test
    public void rbac190SignatureValidationInvalidPublicKey() throws Exception {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(512, new SecureRandom());
        KeyPair keyPair = keyGenerator.genKeyPair();
        File file = new File("public.key");
        file.delete();
        try (FileOutputStream fos = new FileOutputStream(file)) {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyPair.getPublic().getEncoded());
            fos.write(spec.getEncoded());
        }
        System.setProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION, file.getAbsolutePath());

        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        try {
            facade.authenticate();
            fail("Exception should occur, signature is invalid");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Token signature verification failed.", e.getMessage());
        }

        file.delete();
    }

    /**
     * Tests if the token becomes invalid after it expires. !!!!WARNING!!!! TEST MIGHT TAKE SEVERAL HOURS TO EXECUTE,
     * DEPENDING ON THE EXPIRATION TIME SETTINGS (DEFAULT = 8 HOURS)
     */
    @Test
    @Ignore
    public void rbac190Expiration() throws Exception {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        Token token = facade.authenticate();
        synchronized (this) {
            while (token.getExpirationDate().after(new Date())) {
                wait(1000);
            }
        }

        // check for both messages: either token has expired, but is still present (message1)
        // or it has already been purged from the database (message2)
        String message1 = "Service responded unexpectedly: Token owned by '" + username + "' has already expired.";
        String message2 = "Service responded unexpectedly: Token with ID '" + new String(token.getTokenID())
                + "' could not be found.";
        try {
            facade.hasPermission("RBACManagementStudio", "ManageRole");
            fail("Exception should occur, because token is no longer valid.");
        } catch (AccessDeniedException e) {
            System.out.println(e.getMessage());
            assertTrue("Exception expected", message1.equals(e.getMessage()) || message2.equals(e.getMessage()));
        }

        SecurityFacade newFacade = new SecurityFacade();
        newFacade.setToken(token.getTokenID());
        try {
            newFacade.getToken();
            fail("Exception should occur, because token does not exist anymore");
        } catch (AccessDeniedException e) {
            System.out.println(e.getMessage());
            assertTrue("Exception expected", message1.equals(e.getMessage()) || message2.equals(e.getMessage()));
        }
    }

}
